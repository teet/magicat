<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Response\GameResponse;

class ProgressController extends Controller
{
    /**
     * Возвращает прогресс прохождения пользователя.
     *
     * @param integer $userId   идентификатор пользователя
     * @return GameResponse
     */
    public function getAction($userId)
    {
        $playerObject = PlayerObjects::getObject($userId);
        return new GameResponse($playerObject->toArray());
    }
}
