<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Response\GameResponse;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\SocialNetworks\Api\SocialNetwork;

/**
 * Класс для обработки запросов, связанные с обращениями к api социальных сетей.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class SocialApiController extends Controller
{
    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * @param int $userId идентификатор пользователя
     * @param int $level  новый уровень, который достиг пользователь
     *
     * @return GameResponse
     * @throws GameException
     */
    public function setLevelAction($userId, $level)
    {
        $level = intval($level);

        if ($level <= 0) {
            throw new GameException(
                sprintf('Parameter level[%s] is wrong(must > 0).', $level),
                GameException::WRONG_PARAM
            );
        }

        $sn  = SocialNetwork::create();

        try {
            $sn->setUserLevel($userId, $level);
        } catch(GameException $e) {
            throw new GameException(
                sprintf('setUserLevel error. %s', $e->getMessage()),
                GameException::SOCIAL_API_ERROR
            );
        }

        return new GameResponse(array('status' => 1));
    }
}
