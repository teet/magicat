<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Response\GameResponse;

class FriendsController extends Controller
{
    public function progressAction($userIds)
    {
        $friendsList = array();

        if ($userIds != '') {
            $ids = explode(',', $userIds);
            if (count($ids) > 0) {
                $ids = array_unique($ids);
                foreach ($ids as $id) {
                    $playerObject = new PlayerObjects($id);
                    if ($playerObject->isExists()) {
                        $friendsList[] = array(
                            'k' => $playerObject->getUserId(),
                            'p' => $playerObject->getLastPlayStamp(),
                            's' => $playerObject->getStrawberry(),
                            'm' => $playerObject->getMissionsInfo()
                        );
                    }
                    unset($playerObject);
                }
            }
        }

        return new GameResponse($friendsList);
    }
}
