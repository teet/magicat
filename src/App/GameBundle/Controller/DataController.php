<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Models\WeeklyChallenges;
use App\GameBundle\Core\Models\Location\PlayerObjectsLocation;
use App\GameBundle\Core\Models\Location\UserDataLocation;
use App\GameBundle\Core\Models\Location\WeeklyChallengesLocation;
use App\GameBundle\Core\Models\Gifts;
use App\GameBundle\Core\Response\GameResponse;
use App\GameBundle\Core\Exception\GameException;

class DataController extends Controller
{
    /**
     * Обновление данных после авторизации.
     *
     * @param integer $userId       идентификатор пользователя
     * @param string $data          данные для обновления
     * @return GameResponse
     */
    public function updateAutorizeUserDataAction($userId, $data)
    {
        $data     = json_decode($data, true);
        $userData = UserData::getObject($userId);
        $location = new UserDataLocation($userData);

        $consecutiveVisitsCount = null;
        $spinnerTimeStamp       = null;
        $tutorialStates         = null;
        $sendGiftPopupStamp     = null;
        $sentGifts              = null;
        $gameQuestId            = null;

        if (isset($data['cv'])) {
            $consecutiveVisitsCount = $location->checkConsecutiveVisitsCount($data['cv']);
        }
        if (isset($data['st'])) {
            $spinnerTimeStamp = $location->checkSpinnerTimeStamp($data['st']);
        }
        if (isset($data['ts'])) {
            $tutorialStates = $location->updateTutorialStates($data['ts']);
        }
        if (isset($data['gp'])) {
            $sendGiftPopupStamp = $location->updateSendGiftPopupStamp($data['gp']);
        }
        if (isset($data['sg'])) {
            $sentGifts = $location->updateSentGifts($data['sg']);
        }
        if (isset($data['gq'])) {
            $gameQuestId = $location->updateGameQuest($data['gq']);
        }

        if ($consecutiveVisitsCount !== null) {
            $userData->setConsecutiveVisitsCount($consecutiveVisitsCount);
        }
        if ($spinnerTimeStamp !== null) {
            $userData->setSpinnerTimeStamp($spinnerTimeStamp);
        }
        if ($tutorialStates !== null) {
            $userData->setTutorialStates($tutorialStates);
        }
        if ($sendGiftPopupStamp !== null) {
            $userData->setSendGiftPopupStamp($sendGiftPopupStamp);
        }
        if ($sentGifts !== null) {
            $userData->setSentGifts($sentGifts);
        }
        if ($gameQuestId !== null) {
            $userData->setGameQuestId($gameQuestId);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Сохранение пройденных шагов туториала.
     *
     * @param integer $userId       идентификатор пользователя
     * @param string $data          данные туториала
     * @return GameResponse
     */
    public function updateTutorialStateAction($userId, $data)
    {
        $data = json_decode($data, true);

        if (isset($data['ts'])) {
            $userData       = UserData::getObject($userId);
            $location       = new UserDataLocation($userData);
            $tutorialStates = $location->updateTutorialStates($data['ts']);
            $userData->setTutorialStates($tutorialStates);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновление прогресса прохождения.
     * Обновление клубник, времени последней активности, сейва и панд.
     *
     * @param integer $userId  идентификатор пользователя
     * @param string $data  данные для обновления
     * @return GameResponse
     */
    public function updatePlayerProgressAction($userId, $data)
    {
        $playerObjects = PlayerObjects::getObject($userId);
        $data          = json_decode($data, true);
        $location      = new PlayerObjectsLocation($playerObjects);

        $location->updateProgress($data);

        return new GameResponse(array('status' => 1));
    }

    /**
     * Изменяет флаг прохождения миссии.
     *
     * @param type $userId  идентификатор пользователя
     * @param string $data  данные для обновления
     * @return GameResponse
     */
    public function updateIsPlayingMissionAction($userId, $data)
    {
        $data     = json_decode($data, true);
        $userData = UserData::getObject($userId);
        $location = new UserDataLocation($userData);

        $isPlayingMission = null;

        if (isset($data['pm'])) {
            $isPlayingMission = $location->checkIsPlayingMission($data['pm']);
        }

        if ($isPlayingMission !== null) {
            $userData->setIsPlayingMission($isPlayingMission);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет количество жизней.
     * Обновляет время последнего пополнения жизни.
     *
     * @param type $userId  идентификатор пользователя
     * @param string $data  данные для обновления
     * @return GameResponse
     */
    public function updateUserLivesAction($userId, $data)
    {
        $data     = json_decode($data, true);
        $userData = UserData::getObject($userId);
        $location = new UserDataLocation($userData);

        $lives             = null;
        $lastLivesAddStamp = null;

        if (isset($data['lv'])) {
            $lives = $location->checkLives($data['lv']);
        }

        if (isset($data['lt'])) {
            $lastLivesAddStamp = $location->checkLastLivesAddStamp($data['lt']);
        }

        if ($lives !== null) {
            $userData->setLives($lives);
        }

        if ($lastLivesAddStamp !== null) {
            $userData->setLastLivesAddStamp($lastLivesAddStamp);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет список разблокированных бустеров.
     *
     * @param integer $userId   идентификатор пользователя
     * @param string $data      строка разблокированных бустеров
     * @return GameResponse
     */
    public function updateUnlockedBoostsAction($userId, $data)
    {
        $data     = json_decode($data, true);
        $userData = UserData::getObject($userId);
        $location = new UserDataLocation($userData);

        $unlockedBoostNames = null;

        if (isset($data['ub'])) {
            $unlockedBoostNames = $location->checkUnlockedBoostNames($data['ub']);
        }

        if ($unlockedBoostNames !== null) {
            $userData->setUnlockedBoostNames($unlockedBoostNames);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет время последней активности.
     *
     * @param integer $userId   идентификатор пользователя
     * @param string $data      данные о времени последней активности
     * @return GameResponse
     */
    public function updateLastPlayStampAction($userId, $data)
    {
        $data          = json_decode($data, true);
        $playerObjects = PlayerObjects::getObject($userId);
        $location      = new PlayerObjectsLocation($playerObjects);

        $lastPlayStamp = null;
        $strawberry    = null;

        if (isset($data['p'])) {
            $lastPlayStamp = $location->checkLastPlayStamp($data['p']);
        }

        if (isset($data['s'])) {
            $strawberry = $location->checkStrawberry($data['s']);
        }

        if ($lastPlayStamp !== null) {
            $playerObjects->setLastPlayStamp($lastPlayStamp);
        }

        if ($strawberry !== null) {
            $playerObjects->setStrawberry($strawberry);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет время и этап получения монет в еженедельном рейтинге.
     *
     * @param integer $userId идентификатор пользователя
     * @param string  $data   json-объект данных для обновления
     *
     * @return GameResponse
     */
    public function updateWeeklyChallengesAction($userId, $data)
    {
        $data = json_decode($data, true);

        // Параметры не могут прийти по-одиночке
        if (isset($data['m']) && isset($data['t'])) {
            $wc       = WeeklyChallenges::getObject($userId);
            $location = new WeeklyChallengesLocation($wc);

            $milestone         = $location->checkAndGetMilestone($data['m']);
            $milestoneHitStamp = $location->checkAndGetMilestoneHitStamp($data['t']);

            $wc->setMilestone($milestone)
               ->setMilestoneHitStamp($milestoneHitStamp);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет купленные супер бустеры.
     *
     * @param integer $userId идентификатор пользователя
     * @param string  $data   json-объект данных для обновления
     * @throw GameException
     *
     * @return GameResponse
     */
    public function updateInGameBoostsAction($userId, $data)
    {
        $data = json_decode($data, true);

        if (!isset($data['ib'])) {
            throw new GameException('Wrong of input data "ib"(inGameBoosts)', GameException::MISSING_PARAM);
        }

        $userData     = UserData::getObject($userId);
        $location     = new UserDataLocation($userData);
        $inGameBoosts = $location->updateInGameBoosts($data['ib']);
        $userData->setInGameBoosts($inGameBoosts);

        return new GameResponse(array('status' => 1));
    }

    /**
     * Сохраняет время и название предмета последней покупки.
     *
     * @param integer $userId идентификатор пользователя
     * @param string  $data   json-объект данных для обновления
     *
     * @return GameResponse
     */
    public function updatePlayerTransactionAction($userId, $data)
    {
        $data = json_decode($data, true);

        if (!isset($data['bi'])) {
            throw new GameException('Missing parameter "bi"(boughtItemTransaction)', GameException::MISSING_PARAM);
        }

        $userData = UserData::getObject($userId);
        $location = new UserDataLocation($userData);
        $boughtItemTransaction = $location->updateBoughtItemTransaction($data['bi']);
        $userData->setBoughtItemTransaction($boughtItemTransaction);

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет данные об отправке подарков друзьям.
     *
     * @param integer $userId идентификатор пользователя
     * @param string  $data   json-объект данных для обновления
     *
     * @return GameResponse
     */
    public function updateSentGiftsInfoAction($userId, $data)
    {
        $data = json_decode($data, true);

        if (!isset($data['sg'])) {
            throw new GameException('Missing param "sg"(sentGifts)', GameException::MISSING_PARAM);
        }

        $userData  = UserData::getObject($userId);
        $location  = new UserDataLocation($userData);
        $sentGifts = $location->updateSentGifts($data['sg']);

        // Эта проверка могла быть в $location->updateSentGifts, но метод вызывается в другом месте, поэтому вынесена.
        if (sizeof($sentGifts) <= 0) {
            throw new GameException('Size of array "sentGifts" must be more then zero', GameException::WRONG_PARAM);
        }

        // Добавление подарков всем пользователям, которым они предназначены.
        $uniqueIndex = 0;

        foreach ($sentGifts as $gift) {
            // уникальный ключ подарка, по которому его можно найти и удалить
            $key = md5($userId . $gift['i'] . $gift['g'] . (++$uniqueIndex + microtime(true)));

            $receivedGift = array(
                array(
                    'k' => $key,
                    'i' => $userId,
                    'g' => $gift['g'],
                    't' => $gift['t']
                )
            );

            $giftsData = new Gifts($gift['i']);

            if ($giftsData->isExists()) {
                $receivedGift = array_merge($giftsData->getGifts(), $receivedGift);
            }

            // Если пользователя, для кого предназначен подарок, нет в БД, то все равно дарим его.
            // Иначе, добавляем в текущий список.
            $giftsData->setGifts($receivedGift);
        }

        // обновление списка отправленных подарков.
        $sentGifts = array_merge($userData->getSentGifts(), $sentGifts);
        $userData->setSentGifts($sentGifts);

        return new GameResponse(array('status' => 1));
    }

    /**
     * Сохраняет время последнего использования 5 бесплатных дополнительных ходов.
     *
     * @param integer $userId   id пользователя
     * @param string  $data     json-строка с параметрами
     * @return GameResponse
     */
    public function updateUseFreeExtraMovesTimeStampAction($userId, $data)
    {
        $data = json_decode($data, true);

        if (isset($data['em'])) {
            $userData = UserData::getObject($userId);
            $location = new UserDataLocation($userData);
            $value    = $location->checkUseFreeExtraMovesTimeStamp($data['em']);
            $userData->setUseFreeExtraMovesTimeStamp($value);
        }

        return new GameResponse(array('status' => 1));
    }

    /**
     * Обновляет битовую маску этапов прохождения социального квеста.
     *
     * @param integer $userId   id пользователя
     * @param string  $data     json-строка с параметрами
     * @return GameResponse
     */
    public function updateSocialQuestAction($userId, $data)
    {
        $data = json_decode($data, true);

        if (!isset($data['sq'])) {
            throw new GameException('Missing param "sq"(socialQuestMask)', GameException::MISSING_PARAM);
        }

        $questMask = intval($data['sq']);

        if ($questMask < 0) {
            throw new GameException('Wrong value of param "sq"(socialQuestMask)', GameException::WRONG_PARAM);
        }

        $userData         = UserData::getObject($userId);
        $currentQuestMask = $userData->getSocialQuestMask();

        if (($currentQuestMask & 1) == 1) {
            throw new GameException("Social quest allready completed", GameException::WRONG_ACTION);
        }

        // Оставляем второй бит(или же бит для флага приглашенности друга в игру, остальное перезатираем).
        $currentQuestMask = (($currentQuestMask & UserData::SQ_MASK_INVITE_FRIEND) | $questMask);
        $userData->setSocialQuestMask($currentQuestMask);

        return new GameResponse(array('status' => 1, 'sq' => $currentQuestMask));
    }

    /**
     * Обновляет идентификатор игрового квеста.
     *
     * Может прийти событие о начале квеста или завершении.
     *
     * Можно добавить проверку на то, что начатый id квеста должен совпадать с переданным $questId.
     * Или наоборот, чтобы начать квест, надо, чтобы предыдущий был завершен.
     * Но, с этими проверками могут возникнуть проблемы с зависаниями квестов(если будут зависать запросы).
     *
     * @param integer $userId  id пользователя
     * @param string  $questId идентификатор квеста
     * @param int     $state   флаг, определяющий начало или завершение квеста(1-начал, 2-завершил)
     * @return GameResponse
     */
    public function updateGameQuestAction($userId, $questId, $state)
    {
        $state = intval($state);

        if ($state != 1 && $state != 2) {
            throw new GameException('Wrong value of param state', GameException::WRONG_PARAM);
        }

        $userData = UserData::getObject($userId);
        $location = new UserDataLocation($userData);
        $questId  = $location->updateGameQuest($questId); // проверяет существование такого квеста

        if ($state == 2) {
            // Очищаем значение идентификатора текущего квеста.
            $questId = '';
        }

        $userData->setGameQuestId($questId);

        return new GameResponse(array('status' => 1));
    }
}
