<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Models\Coins;

use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\Item\StageCatalog;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Response\GameResponse;

class GameEditorController extends Controller
{
    /**
     * Проверка на доступ к игровому редактору.
     *
     * @throws GameException
     */
    private function _checkAccess()
    {
        if ( ! Config::get('testMode')) {
            throw new GameException('GameEditor access error.', GameException::INTERNAL_ERROR);
        }
    }

    /**
     * Возвращает главную форму игрового редактора.
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->_checkAccess();

        $userId = $request->query->get('userId');
        if (null === $userId) {
            return new Response('Ошибка: введите id пользователя!');
        }

        $userData  = UserData::getObject($userId);
        $playerObj = PlayerObjects::getObject($userId);
        $coinsObj  = Coins::getObject($userId);

        $raw = $playerObj->getMissionsInfo();
        $progress = array();
        foreach ($raw as $value) {
            $mission = explode('_', $value['m']);
            $progress[$mission[0]-1][] = $value;
        }

        $stages = array();
        foreach (StageCatalog::getCatalog($userId) as $stage) {
            $stages[] = $stage;
        }

        $boosts = explode(',', $userData->getUnlockedBoostNames());
        unset($boosts[0]);

        $params = array(
            'USER_ID'      => $userId,
            'PROGRESS'     => json_encode($progress),
            'STAGES'       => json_encode($stages),
            'LIVES'        => $userData->getLives(),
            'COINS'        => $coinsObj->getCoins(),
            'JELLY'        => $playerObj->getStrawberry(),
            'BOOSTS'       => $boosts,
            'SUPER_BOOSTS' => $userData->getInGameBoosts()
        );

        return $this->render('AppGameBundle:GameEditor:index.html.twig', $params);
    }

    /**
     * Редактирование обычных бустеров.
     *
     * @param integer $userId   id пользователя
     * @param string $data      данные
     * @return JsonResponse
     */
    public function setUnlockedBoostsAction($userId, $data)
    {
        $this->_checkAccess();

        $data = json_decode($data, true);
        if (isset($data['ub'])) {
            UserData::getObject($userId)->setUnlockedBoostNames($data['ub']);
        }

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Редактирование супер бустеров.
     *
     * @param integer $userId   id пользователя
     * @param string $data      данные
     * @return JsonResponse
     */
    public function setInGameBoostsAction($userId, $data)
    {
        $this->_checkAccess();

        $data = json_decode($data, true);
        if (isset($data['ib'])) {
            UserData::getObject($userId)->setInGameBoosts($data['ib']);
        }

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Сброс всех туториалов.
     *
     * @param integer $userId   id пользователя
     * @return JsonResponse
     */
    public function resetTutorialsAction($userId)
    {
        $this->_checkAccess();

        UserData::getObject($userId)->setTutorialStates(array());

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Полное обнуление пользователя.
     *
     * @param integer $userId   id пользователя
     * @return JsonResponse
     */
    public function resetUserAction($userId)
    {
        $this->_checkAccess();

        $store = RedisStore::getInstance();
        $store->delete(sprintf('u:%s', $userId));
        $store->delete(sprintf('p:%s', $userId));
        $store->delete(sprintf('c:%s', $userId));
        $store->delete(sprintf('w:%s', $userId));
        $store->delete(sprintf('g:%s', $userId));
        $store->delete(sprintf('sd:%s', $userId));
        $store->delete(sprintf('s:s:%s', $userId));

        $partnerKeys = $store->keys(sprintf('pr:[0-9]*:%s', $userId));
        foreach ($partnerKeys as $key) {
            $store->delete($key);
        }

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Сохраняет прогресс прохождения пользователя.
     *
     * @param integer $userId   id пользователя
     * @param string $data      данные
     * @return JsonResponse
     */
    public function saveProgressAction($userId, $data)
    {
        $this->_checkAccess();

        $data = json_decode($data);
        $missionInfo = array();
        foreach ($data as $perIsland) {
            foreach ($perIsland as $perLevel) {
                $missionInfo[] = $perLevel;
            }
        }

        PlayerObjects::getObject($userId)->setMissionsInfo($missionInfo);

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Сброс информации о покупках.
     *
     * @param integer $userId   id пользователя
     * @return JsonResponse
     */
    public function resetPlayerTransactionAction($userId)
    {
        $this->_checkAccess();

        UserData::getObject($userId)->setBoughtItemTransaction(array());

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Сброс информации об отправленных подарках друзьям.
     *
     * @param integer $userId   id пользователя
     * @return JsonResponse
     */
    public function resetSentGiftsAction($userId)
    {
        $this->_checkAccess();

        UserData::getObject($userId)->setSentGifts(array());

        return new JsonResponse(array('status' => 1));
    }

    /**
     * Установка этапов социального квеста.
     *
     * @param integer $userId   id пользователя
     * @param string $data      данные
     * @return JsonResponse
     */
    public function setSocialQuestAction($userId, $data)
    {
        $this->_checkAccess();

        UserData::getObject($userId)->setSocialQuestMask($data);

        return new JsonResponse(array('status' => 1));
    }
}
