<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Response\GameResponse;
use App\GameBundle\Core\Item\ItemCatalog;
use App\GameBundle\Core\Item\QuestCatalog;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Models\WeeklyChallenges;
use App\GameBundle\Core\Models\Gifts;

class AuthorizeController extends Controller
{
    /**
     * Возвращает информацию о пользователе.
     *
     * @param int $userId   идентификатор пользователя
     * @return GameResponse
     */
    public function indexAction($userId)
    {
        $userData = UserData::getObject($userId);
        return new GameResponse($userData->toArray());
    }

    /**
     * Возвращает конфигурацию цен.
     *
     * @param integer $userId   идентификатор пользователя
     * @return GameResponse
     */
    public function payVaultAction($userId)
    {
        $items = ItemCatalog::getCatalog($userId);
        return new GameResponse($items);
    }

    /**
     * Возвращает данные о еженедельном клубничном рейтинге.
     *
     * @param integer $userId идентификатор пользователя
     * @return GameResponse
     */
    public function weeklyChallengesAction($userId)
    {
        $wc = WeeklyChallenges::getObject($userId);
        return new GameResponse($wc->toArray());
    }

    /**
     * Возвращает данные о подарках пользователя от друзей.
     *
     * @param integer $userId идентификатор пользователя
     * @return GameResponse
     */
    public function giftsAction($userId)
    {
        $gifts = Gifts::getObject($userId);
        return new GameResponse($gifts->toArray());
    }

    /**
     * Возвращает параметры активных квестов.
     *
     * Вместе с активными квестами, присылаются данные текущего квеста пользователя
     * (на случай, если квест был занесен в список неактивных).
     *
     * @param integer $userId   идентификатор пользователя
     * @return GameResponse
     */
    public function gameQuestsAction($userId)
    {
        $userData = UserData::getObject($userId);
        $questId  = $userData->getGameQuestId();
        $quests   = QuestCatalog::getCurrentQuests($questId);

        return new GameResponse($quests);
    }
}
