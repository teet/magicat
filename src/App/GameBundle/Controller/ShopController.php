<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Models\Coins;
use App\GameBundle\Core\Models\Location\CoinsLocation;
use App\GameBundle\Core\Response\GameResponse;

class ShopController extends Controller
{
    /**
     * Покупка бустеров за золото.
     *
     * @param integer $userId идентификатор пользователя
     * @param string  $boosts строка покупаемых бустеров
     *
     * @return GameResponse     остаток золота на счете
     */
    public function buyBoostAction($userId, $boosts)
    {
        $coinsObject = Coins::getObject($userId);
        $location    = new CoinsLocation($coinsObject);
        $remainder   = $location->buyBoosts($boosts);

        $coinsObject->setCoins($remainder);

        return new GameResponse(array('coins' => $remainder));
    }
}
