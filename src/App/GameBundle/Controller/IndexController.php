<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * Обрабатывает запрос на открытие главной страницы по пустому маршруту "/".
     * 
     * @return Response
     */
    public function indexAction()
    {
        return new Response();
    }
}
