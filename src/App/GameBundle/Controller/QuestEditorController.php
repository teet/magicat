<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Item\QuestCatalog;

/**
 * Контроллер для обработки запросов, связанные с редактированием параметров игровых квестов.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class QuestEditorController extends Controller
{
    /**
     * Выдает страницу редактирования квестов.
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppGameBundle:QuestEditor:index.html.twig');
    }

    /**
     * Возвращает список активных и неактивных квестов.
     *
     * @return JsonResponse Возвращает json объект с данными
     */
    public function getQuestsAction()
    {
        return new JsonResponse(QuestCatalog::getCatalog());
    }

    /**
     * Сохраняет список измененных квестов со страницы редактирования.
     *
     * Создает резервную копию текущего файла с данными квестов и перезаписывает его.
     *
     * @param string $data json-объект с данными активных и неактивных квестов
     * @return JsonResponse Возвращает {status: 1}, если успешно, иначе код ошибки
     */
    public function setQuestsAction($data)
    {
        // Проверка данных
        $exp = '/\{'
             . '"lastId":[1-9]{1}\d*,'
             . '"active":\{("i\d+"\:\[\d+\,\d+\,\[(\[[0-2]\,\d+\]\,?)+\]\]\,?)*\},'
             . '"inactive":\{("i\d+"\:\[\d+\,\d+\,\[(\[[0-2]\,\d+\]\,?)+\]\]\,?)*\}'
             . '\}/';

        if (!preg_match($exp, $data)) {
            return new JsonResponse(array('err' => 1));
        }

        $path = dirname(__FILE__) . '/../Resources/catalog/quest/';
        $file = $path . strtolower(Config::get('socialNetwork')) . '.json';

        // Резервная копия
        $destPath = $path . strtolower(Config::get('socialNetwork')) . '_bak/';

        if (!file_exists($destPath)) {
            if (!mkdir($destPath, 0775)) {
                return new JsonResponse(array('err' => 2));
            }
        }

        if (!copy($file, $destPath . date('d.m.y_[H.i.s]'))) {
            return new JsonResponse(array('err' => 3));
        }

        // Сохранение и билд
        file_put_contents($file, $data);
        QuestCatalog::build($file);

        return new JsonResponse(array('status' => 1));
    }
}
