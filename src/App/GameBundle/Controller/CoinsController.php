<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Models\Coins;
use App\GameBundle\Core\Response\GameResponse;
use App\GameBundle\Core\Exception\GameException;

class CoinsController extends Controller
{
    /**
     * Возвращает текущее количество монет на счету пользователя.
     *
     * @param integer $userId   идентификатор пользователя
     * @return GameResponse
     */
    public function getAction($userId)
    {
        $coinsObject = Coins::getObject($userId);

        return new GameResponse(array('coins' => $coinsObject->getCoins()));
    }

    /**
     * Устанавливает пользователю монеты.
     *
     * @param integer $userId   идентификатор пользователя
     * @param integer $coins    устанавливаемое значение
     * @return GameResponse
     * @throws GameException
     */
    public function setAction($userId, $coins)
    {
        $coins = intval($coins);

        if ($coins < 0) {
            throw new GameException(
                sprintf('The negative value of param "coins": %s.', $coins),
                GameException::WRONG_PARAM
            );
        }

        $coinsObject = Coins::getObject($userId);
        $coinsObject->setCoins($coins);

        return new GameResponse(array('status' => 1));
    }
}
