<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Response\GameResponse;
use App\GameBundle\Core\Config\Config;

/**
 * Класс для обработки запросов, связанные c виджетами соц. сетей.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class ShareWidgetController extends Controller
{
    /**
     * Выдает страницу, к которой привязана кнопка "Класс" для ОД.
     *
     * @see http://apiok.ru/wiki/pages/viewpage.action?pageId=42476656
     * В методе: OK.CONNECT.insertShareWidget(id,did,st),
     * в качестве второго параметра должна указываться ссылка, на этот контроллер.
     *
     * @param int $appId идентификатор приложения(чтобы был свой счетчик для каждого приложения: qa,dev,prod).
     *                   значение должно совпадать с Config::get('appId'), поэтому можно использовать его.
     *
     * @return Response
     */
    public function getSharePageAction($appId)
    {
        $parameters = array(
            'APP_ID'  => Config::get('appId'),
            'HOST'    => Config::get('statServer'),
            'VERSION' => Config::get('version')
        );

        return $this->render('AppGameBundle:ShareWidget:od.html.twig', $parameters);
    }
}
