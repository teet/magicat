<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\SocialNetworks\Loader\IframeLoader;
use App\GameBundle\Core\Authorize\Location\AuthorizeLocation;
use App\GameBundle\Core\Authorize\AuthorizeSignature;
use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Models\Coins;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Progress\Location\ProgressLocation;
use App\GameBundle\Core\SplitManager\SplitManager;
use App\GameBundle\Core\Item\ItemCatalog;

class IframeController extends Controller
{
    /**
     * Выполняет необходимые действия при загрузке главной страницы ифрейма и загружает ее.
     *
     * В зависимости, от социальной сети, могут выполняться различные действия:
     * регистрация пользователя, если его нет в БД, проверка офферов, и т.д.
     *
     * @param Request $request объект с полученными от клиента данными
     * @throws \App\GameBundle\Core\Exception\GameException
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $loader = IframeLoader::create($request);
        $userId = $loader->getUserId();

        // Технические работы
        $developerList = Config::get('developerList');

        if (Config::get('gameState') == 1 && !isset($developerList[$userId])) {
            return $this->render('AppGameBundle:Iframe:technical-works.html.twig');
        }

        // Регистрация пользователя, если не зарегистрирован
        $location = new AuthorizeLocation($userId);
        $isReg    = $location->register();

        // Получение номера сплита пользователя.
        $splitId = SplitManager::getUserSplitId($userId, false);

        if ($isReg && $splitId != SplitManager::DEFAULT_SPLIT_ID) {
            // Устанавливаем первоначальный баланс золотых моент пользователя
            $item = ItemCatalog::getItem('startCoins', $userId);
            $obj  = Coins::getObject($userId);
            $obj->setCoins($item['goldCount']);

            // устанавливаем, что ивентовые акции будут доступны только на след. сутки
            $nextDayTs = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
            $obj       = UserData::getObject($userId);
            $obj->setOfferDiscount(array(1, $nextDayTs));

            // Увелививаем счетчик пользователей в сплите
            SplitManager::incrUserCountInSplit($splitId);
        }

        // Номер последнего пройденного острова и уровня
        $playerObjects    = PlayerObjects::getObject($userId);
        $progressLocation = new ProgressLocation($playerObjects);
        $currentLevelId   = $progressLocation->getCurrentLevelId();

        $session    = AuthorizeSignature::generateAndSaveSession($userId);
        $nodeServer = Config::get('nodeServer');

        $parameters = array(
            'HTTP_SERVER_UNIXTIME' => time(),
            'HOST'                 => Config::get('statServer'),
            'SESSION'              => $session,
            'STATISTICS_SERVER'    => Config::get('statisticsServer'),
            'NODE_SERVER_HOST'     => $nodeServer['host'],
            'NODE_SERVER_PORT'     => $nodeServer['port'],
            'IS_REGISTER'          => ($isReg === false ? 0 : 1),
            'APP_ID'               => Config::get('appId'),
            'GROUP_ID'             => Config::get('groupId'),
            'REPORT_ERROR_LINK'    => Config::get('reportErrorLink'),
            'TEST_MODE'            => Config::get('testMode'),
            'VERSION'              => Config::get('version'),
            'SOCIAL_NETWORK'       => Config::get('socialNetwork'),
            'CURRENT_LEVEL_ID'     => $currentLevelId,
            'SPLIT_ID'             => $splitId
        );

        $parameters = array_merge($parameters, $loader->process());

        return $this->render('AppGameBundle:Iframe:' . Config::get('socialNetwork') . '.html.twig', $parameters);
    }
}
