<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\GameBundle\Core\Models\Gifts;
use App\GameBundle\Core\Response\GameResponse;
use App\GameBundle\Core\Exception\GameException;

/**
 * Контроллер для обработки запросов, связанные с подарками.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class GiftsController extends Controller
{
    /**
     * Удаляет использованные(принятые) подарки из списка.
     *
     * @param integer $userId идентификатор пользователя
     * @param string  $gifts  список уникальных ключей подарков через запятую
     *
     * @return GameResponse
     * @throws GameException
     */
    public function removeAction($userId, $gifts)
    {
        if ($gifts == "") {
            throw new GameException(
                'Parameter "gifts" is empty in GiftsController::removeAction.',
                GameException::WRONG_PARAM
            );
        }

        $tmpKeys = explode(',', $gifts);
        $keys    = array();

        // Проверка и преобразование массива в ассоциативный для быстрого поиска элемента по ключу.
        foreach ($tmpKeys as $key) {
            $key = trim($key);

            if ($key == "") {
                throw new GameException(
                    sprintf('Some key is empty in "gifts"[%s].', $gifts),
                    GameException::WRONG_PARAM
                );
            }

            $keys['i' . $key] = 1;
        }

        if (sizeof($keys) <= 0) {
            throw new GameException(
                sprintf('Parameter "gifts"[%s] is empty. ', $gifts),
                GameException::WRONG_PARAM
            );
        }

        $giftsObject  = Gifts::getObject($userId);
        $currentGifts = $giftsObject->getGifts();
        $newGifts     = array();

        foreach ($currentGifts as $key => $gift) {
            if (!isset($keys['i' . $gift['k']])) {
                $newGifts[] = $gift;
            }
        }

        $giftsObject->setGifts($newGifts);

        return new GameResponse(array('status' => 1));
    }
}
