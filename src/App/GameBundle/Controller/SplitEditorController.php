<?php

namespace App\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Tools\Json;
use App\GameBundle\Core\Item\SplitCatalog;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Response\GameResponse;

class SplitEditorController extends Controller
{
    /**
     * Возвращает главную страницу редактора.
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppGameBundle:SplitEditor:index.html.twig');
    }

    /**
     * Возвращает список сплитов.
     *
     * @return Response Возвращает json объект с данными
     */
    public function getSplitsAction()
    {
        $data = file_get_contents($this->_getSplitsPath());
        return new Response($data, Response::HTTP_OK, array('Content-Type' => 'application/json'));
    }

    /**
     * Добавляет новую группу сплитов.
     *
     * @return Response
     */
    public function addGroupAction()
    {
        $data = $this->_getSplits();
        $data['lastGroupId']++;

        $data['items']['g' . $data['lastGroupId']] = array(
            'name'            => 'Group_' . $data['lastGroupId'],
            'startTime'       => 0,
            'stopTime'        => 0,
            'removeTime'      => 0,
            'selectedSplitId' => 0,
            'splits' => array()
        );

        $this->_save(Json::encode($data));

        return new JsonResponse(array(
            'id'   => $data['lastGroupId'],
            'name' => 'Group_' . $data['lastGroupId']
        ));
    }

    /**
     * Добавляет новую группу сплитов.
     *
     * @return Response
     */
    public function removeGroupAction($groupId)
    {
        $data = $this->_getSplits();

        if (!isset($data['items'][$groupId])) {
            throw new GameException('Group id[' . $groupId . '] not found', 1);
        }

        $info = $data['items'][$groupId];

        if ($info['removeTime'] > 0) {
            throw new GameException('Group[' . $groupId . '] allready removed', 2);
        }

        if ($info['startTime'] > 0 && $info['stopTime'] <= 0) {
            throw new GameException('Group[' . $groupId . '] first must be stopped', 3);
        }

        if ($info['startTime'] <= 0) {
            foreach ($info['splits'] as $split) {
                $this->_removeSplit($split['id']);
            }
            unset($data['items'][$groupId]);
        } else {
            $data['items'][$groupId]['removeTime'] = time();
        }

        $this->_save(Json::encode($data));

        return new JsonResponse();
    }

    public function startGroupAction($groupId)
    {
        return new JsonResponse();
    }

    public function stopGroupAction($groupId)
    {
        return new JsonResponse();
    }

    public function addSplitAction($groupId)
    {
        $data = $this->_getSplits();

        if (!isset($data['items'][$groupId])) {
            throw new GameException('Group id[' . $groupId . '] not found', 1);
        }

        if ($data['items'][$groupId]['startTime'] > 0) {
            throw new GameException('Group[' . $groupId . '] allready started', 2);
        }

        $result = array();

        if (count($data['items'][$groupId]['splits']) <= 0) {
            $data['lastSplitId']++;
            $data['items'][$groupId]['splits']['s' . $data['lastSplitId']] = array(
                'id'       => $data['lastSplitId'],
                'name'     => 'Split_' . $data['lastSplitId'],
                'percent'  => 0,
                'copacity' => 0
            );

            $data['lastSplitId']++;
            $data['items'][$groupId]['splits']['s' . $data['lastSplitId']] = array(
                'id'       => $data['lastSplitId'],
                'name'     => 'Split_' . $data['lastSplitId'],
                'percent'  => 0,
                'copacity' => 0
            );

            $result = array(
                array(
                    'id'   => 's' . ($data['lastSplitId'] - 1),
                    'name' => 'Split_' . ($data['lastSplitId'] - 1),
                ),
                array(
                    'id'   => 's' . $data['lastSplitId'],
                    'name' => 'Split_' . $data['lastSplitId'],
                )
            );
        } else {
            $data['lastSplitId']++;
            $data['items'][$groupId]['splits']['s' . $data['lastSplitId']] = array(
                'id'       => $data['lastSplitId'],
                'name'     => 'Split_' . $data['lastSplitId'],
                'percent'  => 0,
                'copacity' => 0
            );

            $result = array(
                array(
                    'id'   => 's' . $data['lastSplitId'],
                    'name' => 'Split_' . $data['lastSplitId'],
                )
            );
        }

        /*
        $this->_save(Json::encode($data));

        if (count($info['splits']) <= 0) {
            // добавление
            $result[] = array()
        }

        foreach ($info['splits'] as $split) {
                $this->_removeSplit($split['id']);
            }
            unset($data['items'][$groupId]);
        } else {
            $data['items'][$groupId]['removeTime'] = time();
        }*/

        $this->_save(Json::encode($data));

        return new JsonResponse($result);
    }

    public function removeSplitAction()
    {
        return new JsonResponse();
    }

    public function editSplitAction()
    {
        return new JsonResponse();
    }

    /**
     * Возвращает путь к файлу с данными сплитов.
     *
     * @return string
     */
    private function _getSplitsPath()
    {
        $path = dirname(__FILE__) . '/../Resources/catalog/split/'
              . strtolower(Config::get('socialNetwork')) . '.json';

        return $path;
    }

    private function _getSplits()
    {
        $data = file_get_contents($this->_getSplitsPath());
        return Json::decode($data);
    }

    private function _save($data)
    {
        $path = dirname(__FILE__) . '/../Resources/catalog/split/';
        $file = $path . strtolower(Config::get('socialNetwork')) . '.json';

        // Резервная копия
        $destPath = $path . strtolower(Config::get('socialNetwork')) . '_bak/';

        if (!file_exists($destPath)) {
            if (!mkdir($destPath, 0775)) {
                throw new GameException('Create directory ["' . $destPath . '"] error', 1);
            }
        }

        if (!copy($file, $destPath . date('d.m.y_[H.i.s]'))) {
            throw new GameException('Splits save error. Can not copy ["' . $file . '"] to ["' . $destPath . '"]', 2);
        }

        // Сохранение и билд
        file_put_contents($file, $data);
        SplitCatalog::build($file);
    }

    private function _removeSplit($splitId)
    {
        //
    }
}
