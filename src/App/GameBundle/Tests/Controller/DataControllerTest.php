<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Item\QuestCatalog;

class DataControllerTest extends WebTestCase
{
    public function testUpdateAutorizeUserData()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // id какого-нибудь существующего квеста
        $quests  = QuestCatalog::getCurrentQuests('');
        $questId = '';

        foreach ($quests as $key => $val) {
            $questId = $key;
            break;
        }

        // test1
        $helper->setParam('data', '{"cv":0,"st":1334567891}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{"ts":{"a1":true},'
                          . '"gp":1334567891,"sg":['
                          . '{"t":1395313583,"i":"2","g":"energy"},'
                          . '{"t":1395313583,"i":"2","g":"gold"},'
                          . '{"t":1395313583,"i":"2","g":"bonusForExtraMoves"},'
                          . '{"t":1395313583,"i":"2","g":"ask"}'
                          . ']}');

        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3
        $helper->setParam('data', '{"gp":1,"sg":[]}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test5
        if ($questId != '') {
            $helper->setParam('data', '{"gq":"' . $questId . '"}');
            $client->request('POST', '/u/ud', $helper->getParameters());
            $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());
        }

        // test6: wrong sg:t(sentStamp must be > 0)
        $helper->setParam('data', '{"ts":{"a1":true},"sg":[{"t":0,"i":"2","g":"energy"}]}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test7: wrong sg:t(sentStamp must be > 0)
        $helper->setParam('data', '{"ts":{"a1":true},"sg":[{"t":-123,"i":"2","g":"energy"}]}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test8: negative
        $helper->setParam('data', '{"sg":[{"wrongParam123":1}]}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test9: negative
        $helper->setParam('data', '{"sg":[{"t":1395313583,"i":"2","g":"_wrong_"}]}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test10: negative
        $helper->setParam('data', '{"gp":-1}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test11: negative
        $helper->setParam('data', '{"gp":0}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test12: negative
        $helper->setParam('data', '{"st":-1}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test13: negative
        $helper->setParam('data', '{"st":0}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test14: negative
        $helper->setParam('data', '{"gq":""}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test15: negative
        $helper->setParam('data', '{"gq":"_inone_"}');
        $client->request('POST', '/u/ud', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateTutorialState()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"ts":{"a1":false,"b4":true}}');
        $client->request('POST', '/u/ts', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/ts', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3: negative
        $helper->setParam('data', '{"ts":{"_wrongTutorialId123_":true}}');
        $client->request('POST', '/u/ts', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdatePlayerProgress()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        //TODO добавить тесты для missionsInfo и collectibleList
        // Отключение функционала коллекционных зверей с v5.9.
        //$helper->setParam('data', '{"s":3,"p":' . TIME . ',"m":[],"c":[]}');
        $helper->setParam('data', '{"s":3,"p":' . TIME . ',"m":[]}');

        $client->request('POST', '/u/pp', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2: negative
        $helper->setParam('data', '{"s":-1}');
        $client->request('POST', '/u/pp', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test3: negative: "p"(lastPlayStamp) must be > 0
        $helper->setParam('data', '{"p":0}');
        $client->request('POST', '/u/pp', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test3: negative: "p"(lastPlayStamp) must be > 0
        $helper->setParam('data', '{"p":-1}');
        $client->request('POST', '/u/pp', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateIsPlayingMission()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"pm":true}');
        $client->request('POST', '/u/pm', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{"pm":false}');
        $client->request('POST', '/u/pm', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/pm', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4: negative
        $helper->setParam('data', '{"pm":"_wrong_"}');
        $client->request('POST', '/u/pm', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateUserLives()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"lv":4,"lt":' . TIME . '}');
        $client->request('POST', '/u/ul', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2: "lt"(lastLivesAddStamp) может отсутствовать
        $helper->setParam('data', '{"lv":5}');
        $client->request('POST', '/u/ul', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3: "lv"(lives) может отсутствовать
        $helper->setParam('data', '{"lt":' . TIME . '}');
        $client->request('POST', '/u/ul', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4: negative
        $helper->setParam('data', '{"lv":-5,"lt":' . TIME . '}');
        $client->request('POST', '/u/ul', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative: "lt"(lastLivesAddStamp) must be > 0
        $helper->setParam('data', '{"lv":0,"lt":0}');
        $client->request('POST', '/u/ul', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test6: negative: "lt"(lastLivesAddStamp) must be > 0
        $helper->setParam('data', '{"lv":0,"lt":-' . TIME . '}');
        $client->request('POST', '/u/ul', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateUnlockedBoosts()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"ub":"bomb"}');
        $client->request('POST', '/u/ub', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{"ub":",bomb,,colorBomb"}');
        $client->request('POST', '/u/ub', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/ub', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4: negative
        $helper->setParam('data', '{"ub":"_emptyBoost_,_boost123_"}');
        $client->request('POST', '/u/ub', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative
        $helper->setParam('data', '{"ub":""}');
        $client->request('POST', '/u/ub', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateLastPlayStamp()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"p":' . TIME . '}');
        $client->request('POST', '/u/lp', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{"s":0}');
        $client->request('POST', '/u/lp', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3
        $helper->setParam('data', '{"p":' . TIME . ',"s":12}');
        $client->request('POST', '/u/lp', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4: negative: s(strawberry) must be >= 0
        $helper->setParam('data', '{"p":' . TIME . ',"s":-1}');
        $client->request('POST', '/u/lp', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative
        $helper->setParam('data', '{"p":0}');
        $client->request('POST', '/u/lp', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test6: negative
        $helper->setParam('data', '{"p":-1334567890}');
        $client->request('POST', '/u/lp', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateWeeklyChallenges()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"m":2,"t":1334567890}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2: обновления не будет но и ошибки тоже
        $helper->setParam('data', '{"m":1}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3: обновления не будет но и ошибки тоже
        $helper->setParam('data', '{"t":1}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4: обновления не будет но и ошибки тоже
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test5: negative: "t"(milestoneHitStamp) must be > 0
        $helper->setParam('data', '{"m":0,"t":0}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test6: negative: "t"(milestoneHitStamp) must be > 0
        $helper->setParam('data', '{"m":6,"t":-1}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test7: negative: "m"(milestone) must be from 0 to 5
        $helper->setParam('data', '{"m":6,"t":1}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test8: negative: "m"(milestone) must be from 0 to 5
        $helper->setParam('data', '{"m":-1,"t":1334567890}');
        $client->request('POST', '/u/wc', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateInGameBoosts()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"ib":{"ingameRadialBomb":5,"ingameChineseFirework":101}}');
        $client->request('POST', '/u/ib', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3
        $helper->setParam('data', '{"ib":{"ingameRadialBomb":0,"ingameChineseFirework":0}}');
        $client->request('POST', '/u/ib', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test4: negative
        $helper->setParam('data', '{"ib":{"_wrongBoost1_":1,"ingameRadialBomb":2}}');
        $client->request('POST', '/u/ib', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative
        $helper->setParam('data', '{"ib":{"ingameRadialBomb":-1,"ingameChineseFirework":2}}');
        $client->request('POST', '/u/ib', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test6: negative
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/ib', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::MISSING_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdatePlayerTransaction()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper(1);

        // обнуление данных о последней транзакции.
        $userData = UserData::getObject(1);
        $userData->setBoughtItemTransaction(array());

        // test1
        $helper->setParam('data', '{"bi":{"i":"bomb","t":13}}');
        $client->request('POST', '/u/pt', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2: покупка того же продукта, что и в последний раз.
        $helper->setParam('data', '{"bi":{"t":1324542455}}');
        $client->request('POST', '/u/pt', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3: negative: параметр "t"(transactionTimeStamp) должен присутствовать
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/pt', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::MISSING_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test4: negative: покупка несуществующего продукта
        $helper->setParam('data', '{"bi":{"i":"_1wrongItem1_","t":1}}');
        $client->request('POST', '/u/pt', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative: отрицательное значение времени
        $helper->setParam('data', '{"bi":{"i":"bomb","t":-1}}');
        $client->request('POST', '/u/pt', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative: нулевое значение времени
        $helper->setParam('data', '{"bi":{"i":"bomb","t":0}}');
        $client->request('POST', '/u/pt', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateSentGiftsInfo()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"sg":['
                          . '{"g":"energy","t":1325478545,"i":"2"},'
                          . '{"g":"gold","t":"1325478545","i":"2"},'
                          . '{"g":"bonusForExtraMoves","t":"1325478545","i":"2"},'
                          . '{"g":"ask","t":"1325478545","i":"2"}'
                          . ']}');

        $client->request('POST', '/u/sg', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2: negative
        $helper->setParam('data', '{"sg":[{"g":"_wrong_","t":1325478545,"i":"2"}]}');
        $client->request('POST', '/u/sg', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test3: negative
        $helper->setParam('data', '{"sg":[{"g":"energy","t":0,"i":"2"}]}');
        $client->request('POST', '/u/sg', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test4: negative
        $helper->setParam('data', '{"sg":[{"g":"energy","t":-1,"i":"2"}]}');
        $client->request('POST', '/u/sg', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative
        $helper->setParam('data', '{"sg":[{"g":"energy","t":1234567890,"i":"-2"}]}');
        $client->request('POST', '/u/sg', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test6: negative
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/sg', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::MISSING_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateUseFreeExtraMovesTimeStamp()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('data', '{"em":' . TIME . '}');
        $client->request('POST', '/u/em', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/em', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3: negative
        $helper->setParam('data', '{"em":0}');
        $client->request('POST', '/u/em', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test4: negative
        $helper->setParam('data', '{"em":-1334567890}');
        $client->request('POST', '/u/em', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateSocialQuest()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        $userData = UserData::getObject(1);
        $userData->setSocialQuestMask(UserData::SQ_MASK_INVITE_FRIEND);

        // note: каждый следующий тест может зависеть от предыдущего!

        // test1
        $helper->setParam('data', '{"sq":0}');
        $client->request('POST', '/u/sq', $helper->getParameters());
        $this->assertRegExp('/\{"status":1,"sq":' . UserData::SQ_MASK_INVITE_FRIEND . '\}/',
                            $client->getResponse()->getContent());

        // test2
        $helper->setParam('data', '{"sq":6}');
        $client->request('POST', '/u/sq', $helper->getParameters());
        $this->assertRegExp('/\{"status":1,"sq":6\}/', $client->getResponse()->getContent());

        // test3: quest completed
        $helper->setParam('data', '{"sq":1}');
        $client->request('POST', '/u/sq', $helper->getParameters());
        $this->assertRegExp('/\{"status":1,"sq":3\}/', $client->getResponse()->getContent());

        // test4: negative: sq must be exists
        $helper->setParam('data', '{}');
        $client->request('POST', '/u/sq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::MISSING_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative: sq must be >= 0
        $helper->setParam('data', '{"sq":-1}');
        $client->request('POST', '/u/sq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test6: negative: quest allready completed
        $helper->setParam('data', '{"sq":1}');
        $client->request('POST', '/u/sq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_ACTION . '\}/',
            $client->getResponse()->getContent()
        );
    }

    public function testUpdateGameQuest()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // id какого-нибудь существующего квеста
        $quests  = QuestCatalog::getCurrentQuests('');
        $questId = '';

        foreach ($quests as $key => $val) {
            $questId = $key;
            break;
        }

        if ($questId != '') {
            // test1
            $helper->setParam('questId', $questId)->setParam('state', 1);
            $client->request('POST', '/u/gq', $helper->getParameters());
            $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

            // test2
            $helper->setParam('questId', $questId)->setParam('state', 2);
            $client->request('POST', '/u/gq', $helper->getParameters());
            $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());
        }

        // test3: negative: state must be equal to 1 or 2
        $helper->setParam('questId', 'i1')->setParam('state', 0);
        $client->request('POST', '/u/gq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test4: negative: state must be equal to 1 or 2
        $helper->setParam('questId', 'i1')->setParam('state', 3);
        $client->request('POST', '/u/gq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative: empty quest id
        $helper->setParam('questId', '')->setParam('state', 1);
        $client->request('POST', '/u/gq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test5: negative: invalid quest id
        $helper->setParam('questId', '_inone_')->setParam('state', 1);
        $client->request('POST', '/u/gq', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }
}
