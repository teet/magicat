<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;

class ProgressControllerTest extends WebTestCase
{
    public function testGet()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper(1);

        $client->request('POST', '/p/g', $helper->getParameters());

        $exp = '/\{'
             . '"s":((\d+)|("\d+")),'
             . '"m":\[.*\],'
             // HACK: Отключение функционала коллекционных зверей с v5.9.
             //. '"c":\[.*\],'
             . '"i":((\d+)|("\d+")),'
             . '"p":((\d+)|("\d+"))'
             . '\}/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }
}
