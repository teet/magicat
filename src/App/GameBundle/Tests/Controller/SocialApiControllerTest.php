<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;
use App\GameBundle\Core\Exception\GameException;

class SocialApiControllerTest extends WebTestCase
{
    public function testSetLevel()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // 1 тест на проверку синтаксических ошибок, т.к. запрос к апи может не сработать.
        $helper->setParam('level', '1');
        $client->request('POST', '/sa/sl', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // тесты на ошибку.
        $helper->setParam('level', 0);
        $client->request('POST', '/sa/sl', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        $helper->setParam('level', -1);
        $client->request('POST', '/sa/sl', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        $helper->setParam('level', 'e1');
        $client->request('POST', '/sa/sl', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }
}
