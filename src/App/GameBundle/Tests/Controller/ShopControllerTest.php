<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;
use App\GameBundle\Core\Exception\GameException;

class ShopControllerTest extends WebTestCase
{
    public function testBuyBoost()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1: успешно, недостаточно монет или нет разблокированных бустеров для покупки
        $helper->setParam('boosts', 'bomb');
        $client->request('POST', '/s/b', $helper->getParameters());

        $exp = '/((\{"coins":(([\d]+)|("[\d]+"))\})|(\{"errorCode":('
             . GameException::NO_COINS . '|' . GameException::WRONG_ACTION . ')\}))/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());

        // test2: negative: список бустеров для покупки пуст
        $helper->setParam('boosts', '');
        $client->request('POST', '/s/b', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test3: negative: нет разблокированных бустеров | список не содержит бустеров
        $helper->setParam('boosts', ',,');
        $client->request('POST', '/s/b', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":(' . GameException::WRONG_ACTION . '|' . GameException::GAME_ERROR . ')\}/',
            $client->getResponse()->getContent()
        );

        // test4: negative: нет разблокированных бустеров | список содержит неправильные имена бустеров
        $helper->setParam('boosts', '_wrongBoost_,orEmptyBoost,12');
        $client->request('POST', '/s/b', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":(' . GameException::WRONG_ACTION . '|' . GameException::GAME_ERROR . ')\}/',
            $client->getResponse()->getContent()
        );
    }
}
