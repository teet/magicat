<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\SocialNetworks\Loader\IframeLoader;

class IframeControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $loader = IframeLoader::create(new Request());

        // test1
        $crawler = $client->request('GET', '/i?' . $loader->getViewerIdKey() . '=1');
        $this->assertTrue($crawler->filter("title:contains('AppGameBundle')")->count() > 0);

        // test2: negative
        $client->request('GET', '/i');
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::MISSING_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }
}
