<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;
use App\GameBundle\Core\Models\Location\UserDataLocation;

class AuthorizeControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper(1);

        // Список типов туториала
        $tutorialStates = array_keys(UserDataLocation::getTutorialStatesList());
        $tutorialIdList = implode('|', $tutorialStates);

        $client->request('POST', '/a/i', $helper->getParameters());

        $exp = '/\{'
             . '"k":"1",'                    // идентификатор пользователя, от кого был запрос.
             . '"lv":"?(0|([1-9]+[\d]*))"?,'
             . '"lt":((\d+)|("\d+")),'
             . '"cv":((\d+)|("\d+")),'
             . '"sg":\[(\{.+\}\,?)*\],' // порядок ключей пожет несовпадать, поэтому проверяем по общему шаблону
             . '"ib":((\[\])|(\{("[\w]+":[\d]+\,?)*\})),'
             . '"ub":"(\,?[\w]*)*",'
             . '"st":((\d+)|("\d+")),'
             . '"gp":((\d+)|("\d+")),'
             . '"pd":((0)|("0")),'
             . '"ps":((0)|("0")),'
             . '"pu":(true|false),'
             . '"ts":((\[\])|(\{("(' . $tutorialIdList . ')":(true|false|[\d]+)\,?)*\})),'
             . '"bi":((\[\])|(\{("(i|t)":"?\w+"?\,?){0,2}\})),'
             . '"pm":(true|false),'
             . '"em":((\d+)|("\d+")),'
             . '"sq":((\d+)|("\d+")),'
             . '"gq":(("")|("i\d+")),'
             . '"od":\[\d+,\d+\]'
             . '\}/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }

    public function testPayVault()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        $client->request('POST', '/a/p', $helper->getParameters());
        $this->assertRegExp('/\{((\[.*\])|(.*))\}/', $client->getResponse()->getContent());
    }

    public function testWeeklyChallenges()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        $client->request('POST', '/a/w', $helper->getParameters());

        $exp = '/\{'
             . '"m":(([0-5]{1})|("[0-5]{1}"))\,'
             . '"t":((\d+)|("\d+"))'
             . '\}/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }

    public function testGifts()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // Список типов подарков
        $giftTypes = implode('|', array_keys(UserDataLocation::getGiftTypes()));

        $client->request('POST', '/a/g', $helper->getParameters());

        $exp = '/\[(\{'
             . '"k":"[a-z\d]{32}",'
             . '"i":((\d+)|("\d+")),'
             . '"g":"(' . $giftTypes . ')",'
             . '"t":((\d+)|("\d+"))\}\,?'
             . ')*\]/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }

    public function testGameQuests()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        $client->request('POST', '/a/q', $helper->getParameters());
        $exp = '/((\[\])|(\{'
             . '("i\d+"\:\[\d+\,\d+\,\[(\[[0-2]\,\d+\]\,?)+\]\]\,?)+'
             . '\}))/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }

}
