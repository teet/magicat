<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;
use App\GameBundle\Core\Exception\GameException;

class GiftsControllerTest extends WebTestCase
{
    public function testRemove()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('gifts', 'giftfquniquemd5key');
        $client->request('POST', '/g/r', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test2
        $helper->setParam('gifts', ' giftfquniquemd5key, key2');
        $client->request('POST', '/g/r', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        // test3: negative: пустой список ключей
        $helper->setParam('gifts', '');
        $client->request('POST', '/g/r', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );

        // test4: negative: (в списке присутствует пустой ключ)
        $helper->setParam('gifts', 'key1,  ,key3 ');
        $client->request('POST', '/g/r', $helper->getParameters());
        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }
}
