<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;
use App\GameBundle\Core\Exception\GameException;

class CoinsControllerTest extends WebTestCase
{
    public function testGet()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        $client->request('POST', '/c/g', $helper->getParameters());
        $exp = '/\{"coins":(([\d]+)|("[\d]+"))\}/';
        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }

    public function testSet()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper();

        // test1
        $helper->setParam('coins', 12);
        $client->request('POST', '/c/s', $helper->getParameters());
        $this->assertRegExp('/\{"status":1\}/', $client->getResponse()->getContent());

        //test2
        $helper->setParam('coins', -1);
        $client->request('POST', '/c/s', $helper->getParameters());

        $this->assertRegExp(
            '/\{"errorCode":' . GameException::WRONG_PARAM . '\}/',
            $client->getResponse()->getContent()
        );
    }
}
