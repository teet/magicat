<?php

namespace App\GameBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\GameBundle\Tests\Common\TestCaseHelper;

class FriendsControllerTest extends WebTestCase
{
    public function testProgress()
    {
        $client = static::createClient();
        $helper = new TestCaseHelper(1, array(2, 3));

        // test1: one friend
        $helper->setParam('userIds', '2');
        $client->request('POST', '/f/p', $helper->getParameters());

        $exp = '/\['
             . '\{"k":"2","p":((\d+)|("\d+")),"s":((\d+)|("\d+")),"m":\[.*\]\}'
             . '\]/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());

        // test2: many friends
        $helper->setParam('userIds', '2,3');
        $client->request('POST', '/f/p', $helper->getParameters());

        $exp = '/\['
             . '(\{"k":"(2|3)","p":((\d+)|("\d+")),"s":((\d+)|("\d+")),"m":\[.*\]\}\,*)+'
             . '\]/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());

        // test3: no friends
        $helper->setParam('userIds', '');
        $client->request('POST', '/f/p', $helper->getParameters());
        $this->assertRegExp('/\[\]/', $client->getResponse()->getContent());

        // test4: для всех случаев
        $helper->setParam('userIds', '2,3,-4');
        $client->request('POST', '/f/p', $helper->getParameters());

        $exp = '/\['
             . '(\{"k":"[\d]+","p":((\d+)|("\d+")),"s":((\d+)|("\d+")),"m":\[.*\]\}\,*)*'
             . '\]/';

        $this->assertRegExp($exp, $client->getResponse()->getContent());
    }
}
