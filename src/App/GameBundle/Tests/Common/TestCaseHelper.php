<?php

namespace App\GameBundle\Tests\Common;

use Redis;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\SocialNetworks\Authorize\AuthorizeSocial;
use App\GameBundle\Core\Authorize\Location\AuthorizeLocation;

/**
 * @file
 * Содержит класс со вспомогательными методами для юнит-тестов.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Вспомогательный класс для предоставления общего функционала юнит-тестам.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class TestCaseHelper
{
    /**
     * Ассоциативный массив post-параметров запроса.
     *
     * @var array
     */
    protected $_parameters;

    /**
     * Экземпляр класса для работы с хранилищем redis.
     *
     * @var RedisStore
     */
    protected $_redis;

    /**
     * Конструктор.
     *
     * Инициализирует начальные параметры, создает тестовых пользователей в БД, если их нет.
     *
     * @param $userId      идентификатор пользователя, от которого поступяют запросы
     * @param $friendsList идентификаторы друзей пользователя
     *
     * @throw \Exception
     */
    public function __construct($userId = 1, $friendsList = array())
    {
        if (Config::get('testMode') !== 1) {
            throw new \Exception("Application must be in testMode. See parameter \"testMode\" in Config class");
        }

        $this->_redis = RedisStore::getInstance();
        $this->_initParameters((string)$userId);

        if (is_array($friendsList) && sizeof($friendsList) > 0) {
            for ($i = 0; $i < sizeof($friendsList); $i++) {
                $this->_registerUser((string)$friendsList[$i]);
            }
        }
    }

    /**
     * Создает аккаунт пользователя, если его нет в БД.
     *
     * @param $userId идентификатор пользователя
     *
     * @return TestHelper
     */
    private function _registerUser($userId)
    {
        $location = new AuthorizeLocation($userId);
        $location->register();

        return $this;
    }

    /**
     * Устанавливает post-параметр запроса.
     *
     * @param $key имя параметра
     * @param $val значение
     *
     * @return TestHelper
     */
    public function setParam($key, $val)
    {
        $this->_parameters[$key] = $val;
        return $this;
    }

    /**
     * Возвращает данные post-параметров запроса.
     *
     * @return array Ассоциативный массив post-параметров запроса
     */
    public function getParameters()
    {
        return $this->_parameters;
    }

    /**
     * Инициализирует данные клиента, от которого будут идти запросы в систему.
     *
     * @param $userId идентификатор пользователя в соц. сети, от которого будут идти запросы
     */
    private function _initParameters($userId)
    {
        $this->_parameters = array(
            'userId'  => $userId,
            'authKey' => AuthorizeSocial::create()->getAuthHash($userId)
        );

        $this->_registerUser($userId);
    }

    /**
     * Устанавливает post-параметры по умолчанию.
     *
     * @return TestHelper
     */
    public function resetParameters()
    {
        $this->_parameters = array(
            'userId'  => $this->_parameters['userId'],
            'authKey' => $this->_parameters['authKey']
        );

        return $this;
    }

    /**
     * Проверяет сигнатуру ответа от сервера.
     *
     * @return bool Возвращает true, если сигнатура правильная, иначе false
     */
    public function checkSignature($response)
    {
        return true;
    }
}
