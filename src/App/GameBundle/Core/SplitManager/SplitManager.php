<?php

namespace App\GameBundle\Core\SplitManager;

use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Models\SplitData;
use App\GameBundle\Core\Item\SplitCatalog;

/**
 * @file
 * Содержит класс для работы со сплитами.
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Менеджер по работе с системой разбивки настроек для групп пользователей (Сплит-система).
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class SplitManager
{
    const DEFAULT_SPLIT_ID     = '0';
    const ACTIVE_SPLITS_KEY    = 'splits';

    const PERCENT_PREFIX       = 'p';
    const MAX_COUNT_PREFIX     = 'm';
    const CURRENT_COUNT_PREFIX = 'c';

    /**
     * Возвращает номер сплита из списка, исходя из процентного соотношения между ними.
     *
     * @param array $splits Список сплитов: array(id => 1, p => 100, m => 1000, c => 0)
     * @return string Номер выбранного сплита
     */
    private static function _selectSplit($splits)
    {
        $k = 100; // 100%

        foreach ($splits as $key => $info) {
            $percent = $info[self::PERCENT_PREFIX];
            $r       = rand(1, $k);

            if ($r <= $percent) {
                return $info['id'];
            }

            $k -= $percent;
        }

        return self::DEFAULT_SPLIT_ID;
    }

    /**
     * Возвращает список активный сплитов, с учетом кол-ва пользователей в кажом из них.
     *
     * @param array $splits Список сплитов из хранилища redis: array(p1 => 100, m1 => 0, c1 => 12)
     * @return array Возвращает список сплитов: array(id => 1, p => 100, m => 1000, c => 0)
     */
    private static function _getActiveSplits($splits)
    {
        if (!is_array($splits) || count($splits) <= 0) {
            return array();
        }

        $splitInfo = array();

        foreach ($splits as $key => $val) {
            $id = substr($key, 1);

            if (!isset($splitInfo['i' . $id])) {
                $splitInfo['i' . $id] = array(
                    'id' => $id
                );
            }

            $splitInfo['i' . $id][$key[0]] = $val;
        }

        $percents     = 0;
        $activeSplits = array();

        foreach ($splitInfo as $key => $info) {
            if ($info[self::MAX_COUNT_PREFIX] > 0 &&
                $info[self::CURRENT_COUNT_PREFIX] >= $info[self::MAX_COUNT_PREFIX]
            ) {
                $percents += $info[self::PERCENT_PREFIX];
            } else {
                $activeSplits[] = $info;
            }
        }

        $n = count($activeSplits);

        if ($n == 1) {
            $activeSplits[0][self::PERCENT_PREFIX] = 100;
        } elseif ($percents > 0 && $n > 0) {
            $percents = round((float)$percents / $n);

            foreach ($activeSplits as $key => $info) {
                $activeSplits[$key][self::PERCENT_PREFIX] += $percents;
            }
        }

        return $activeSplits;
    }

    /**
     * Возвращает номер сплита для нового пользователя.
     *
     * @return string
     */
    public static function getNewSplitId()
    {
        // данные сплитов.
        $redis  = RedisStore::getInstance();
        $splits = $redis->hFetchAll(self::ACTIVE_SPLITS_KEY);

        if (!is_array($splits) || count($splits) <= 0) {
            return self::DEFAULT_SPLIT_ID;
        }

        // данные активных сплитов, куда еще можно вместить пользователей.
        $splits = self::_getActiveSplits($splits);

        if (count($splits) <= 0) {
            return self::DEFAULT_SPLIT_ID;
        }

        // получение номера сплита, иходя из процентного соотношения сплитов.
        $splitId = self::_selectSplit($splits);

        return $splitId;
    }

    /**
     * Увеличивает счетчик кол-ва пользователей, которые попали в определенный сплит.
     *
     * @param int $splitId        Идентификатор сплита
     *
     * @return void
     */
    public static function incrUserCountInSplit($splitId)
    {
        $redis = RedisStore::getInstance();
        $redis->hIncrBy(self::ACTIVE_SPLITS_KEY, self::CURRENT_COUNT_PREFIX . $splitId, 1);
    }

    /**
     * Возвращает номер сплита существующего пользователя.
     *
     * Дополнительно проверяет сплит на существование.
     *
     * @param string $userId        Идентификатор пользователя в соц. сети
     * @param bool   $findInRemoved Флаг, при котором сплит ищется в списке удаленных
     *
     * @return string
     */
    public static function getUserSplitId($userId, $findInRemoved = true)
    {
        $splitData = new SplitData($userId);
        $splitId   = $splitData->getSplitId();

        if (!is_null($splitId) && SplitCatalog::isExists($splitId, $findInRemoved)) {
            return $splitId;
        }

        return self::DEFAULT_SPLIT_ID;
    }
}
