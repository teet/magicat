<?php
namespace App\GameBundle\Core\Config;

use App\GameBundle\Core\Tools\ArrayToText;

class Config
{
    public static function get($name)
    {
        return self::$_container[$name];
    }

    /**
     * Генерирует содержимое параметров из json файла.
     *
     * @param $path путь к json-файлу с параметрами.
     * @return void
     */
    public static function build($path)
    {
        $data = file_get_contents($path);
        $data = json_decode($data, true);

        $content = preg_replace(
            '/private static \$_container = array\(.*?\);/s',
            sprintf("private static \$_container = array(\n%s    );", ArrayToText::parse($data)),
            file_get_contents(__FILE__)
        );

        file_put_contents(__FILE__, $content);
    }

    private static $_container = array(
        'version'          => '6.6.3.2',
        'socialNetwork'    => 'VK',
        'appId'            => 4172737,
        'groupId'          => 70043976,
        'reportErrorLink'  => '#',
        'serverSecretKey'  => 'KSbNwal5LIiEJW9MpbcG',
        'signatureKey'     => 'fb8bff1bd337c4f9289eb9661dcf71dd',
        'testMode'         => 1,
        'gameState'        => 0,
        'redisServer'      => array(
            'host' => '127.0.0.1',
            'port' => 6379,
            'db'   => 1,
            'pass' => '',
        ),
        'redisStatistic'   => array(
            'host' => '127.0.0.1',
            'port' => 6379,
            'db'   => 5,
            'pass' => '',
        ),
        'nodeServer'       => array(
            'host'    => '//qa.vk.jgit.ru',
            'port'    => 8083,
            'sslKey'  => '',
            'sslCert' => '',
        ),
        'statServer'       => 'qa.vk.jgit.ru',
        'statisticsServer' => 'watch.vk.jgit.ru',
        'logDir'           => '../../logs',
        'partnerSecret'    => array(
            7776 => 'NLIBqbGh7sIniuOKS0',
        ),
        'developerList'    => array(
            388964    => 1,
            10282559  => 1,
            16343276  => 1,
            22830864  => 1,
            82542921  => 1,
            180755863 => 1,
            245620072 => 1,
            144416249 => 1,
            87322300  => 1,
        ),
    );
}
