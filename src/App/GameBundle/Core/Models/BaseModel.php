<?php

namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\Exception\GameException;

/**
 * @file
 * Содержит базовую абстрактную модель.
 * Все модели должны быть унаследованны от базовой.
 *
 * @author Amir Kumalov <amir@-emagic.org>
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Базовая абстрактная модель.
 *
 * Каждая унаследованная модель содержит время последнего обращения к объекту.
 * Данные:
 * - _t; // lastAccessTimestamp
 *
 * @author Amir Kumalov <amir@-emagic.org>
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
abstract class BaseModel
{
    /**
     * Хранит идентификатор пользователя.
     *
     * @var integer
     */
    protected $_userId;

    /**
     * Хранит ссылку на объект кэша.
     *
     * @var RedisStore
     */
    protected $_cache;

    /**
     * Хранит ключ кэша модели.
     *
     * @var string
     */
    protected $_key;

    /**
     * Конструктор.
     *
     * @param integer $userId   идентификатор пользователя
     */
    public function __construct($userId)
    {
        $this->_userId = $userId;
        $this->_cache  = RedisStore::getInstance();
        $this->_key    = sprintf($this->getKeyPrefix(), $userId);
    }

    /**
     * Создает объект модели.
     *
     * @param integer $userId   идентификатор пользователя
     * @return BaseModel
     * @throws GameException
     */
    public static function getObject($userId)
    {
        $className = get_called_class();
        if ($className === false) {
            throw new GameException($className . ' not found.', GameException::GAME_ERROR);
        }

        $object = new $className($userId);
        if (!$object->isExists()) {
            throw new GameException($className . ' not exists.', GameException::USER_NOT_FOUND);
        }

        return $object;
    }

    /**
     * Возвращает идентификатор пользователя модели.
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     * Проверяет существование объекта.
     *
     * @return boolean  true  - объект существует
     *                  false - объект отсутствует
     */
    public function isExists()
    {
        if (!$this->_cache->exists($this->_key)) {
            $path = $this->getDumpPath();
            if (!file_exists($path)) {
                return false;
            }

            $json = json_decode(file_get_contents($path), true);
            if (!$this->restore($json)) {
                return false;
            }
            unlink($path);
        }

        $this->_cache->hSave($this->_key, '_t', TIME);
        return true;
    }

    /**
     * Запуск транзакции redis.
     *
     * @return void
     */
    public function beginTransaction()
    {
        $this->_cache->multi();
    }

    /**
     * Завершение транзакции redis.
     *
     * @return void
     */
    public function commit()
    {
        $this->_cache->exec();
    }

    /**
     * Возвращает путь до файла с дампом
     *
     * @return string Строка содержащая путь к файлу дампа
     */
    protected function getDumpPath()
    {
        $path = './../dump/' . substr(md5($this->_userId), 0, 5) .'/' . $this->_userId . '/';
        $path[12] = '/';
        return $path;
    }

    /**
     * Возвращает созданный объект модели c начальными данными.
     *
     * @return BaseModel
     */
    public abstract function init();

    /**
     * Возвращает префикс ключа для хранения модели в redis.
     *
     * В префиксе должен присутствовать флаг форматирования "%s".
     * На его место подставляется значение, по которому можно
     * определить кому принадлежит ключ (sprintf(getKeyPrefix(), $uniqueKey);).
     *
     * Например, может подставляться идентификатор пользователя: "key:123".
     *
     * @return string Возвращает префикс ключа в виде форматированной строки с флагом "%s"
     */
    public abstract function getKeyPrefix();

    /**
     * Восствнавливает данные игрока из файла-дампа
     *
     * @param $json Параметр содержащий весь дамп игрока в виде json-объекта
     * @return bool Возвращает true в случае удачного восстановления
     */
    public abstract function restore($json);
}
