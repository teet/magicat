<?php

namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Models\BaseModel;

/**
 * @file
 * Содержит модель для хранения данных о количестве монет пользователя.
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Модель для хранения количества монет пользователя.
 *
 * Хранит объект(хеш) в редисе по ключу "c:{userId}".
 * Данные объекта:
 * - int  c; // кол-во монет
 * - int _t; // время последнего обращения к объекту
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class Coins extends BaseModel
{
    /**
     * Устанавливает количество монет пользователя.
     *
     * @param int $coins количество монет
     * @return Coins
     */
    public function setCoins($coins)
    {
        $this->_cache->hSave($this->_key, 'c', $coins);
        return $this;
    }

    /**
     * Возвращает текущее количество монет пользователя.
     *
     * @return int
     */
    public function getCoins()
    {
        return $this->_cache->hFetch($this->_key, 'c');
    }

    /**
     * Возвращает созданный объект модели c начальными данными.
     *
     * @return Coins
     */
    public function init()
    {
        $this->_cache->hSaveAll(
            $this->_key, array(
                'c'  => 0,
                '_t' => TIME
            )
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDumpPath()
    {
        return parent::getDumpPath() . 'c.json';
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyPrefix()
    {
        return 'c:%s';
    }

    /**
     * {@inheritDoc}
     */
    public function restore($json)
    {
        if (isset($json['c'])) {
            $this->_cache->hSaveAll($this->_key, $json['c']);
            return true;
        }
        return false;
    }
}
