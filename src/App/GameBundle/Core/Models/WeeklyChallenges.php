<?php

namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Models\BaseModel;

/**
 * @file
 * Содержит модель данных по еженедельному клубничному рейтингу.
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Модель для хранения данных по еженедельному клубничному рейтингу.
 *
 * Хранит объект(хеш) в редисе по ключу "w:{userId}".
 * Данные объекта:
 * - int  m; // этап(milestone) от 0 до 5
 * - int  t; // время в миллисекундах
 * - int _t; // время последнего обращения к объекту
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class WeeklyChallenges extends BaseModel
{
    /**
     * Возвращает номер последнего пройденного этапа.
     *
     * @return int
     */
    public function getMilestone()
    {
        return $this->_cache->hFetch($this->_key, 'm');
    }

    /**
     * Устанавливает номер последнего пройденного этапа.
     *
     * @param int $milestone номер последнего пройденного этапа
     *
     * @return WeeklyChallenges
     */
    public function setMilestone($milestone)
    {
        $this->_cache->hSave($this->_key, 'm', $milestone);
        return $this;
    }

    /**
     * Возвращает время получения монет за последний пройденный этап.
     *
     * @return int
     */
    public function getMilestoneHitStamp()
    {
        return $this->_cache->hFetch($this->_key, 't');
    }

    /**
     * Устанавливает время получения монет за последний пройденный этап.
     *
     * @param int $milestoneHitStamp время получения монет за последний этап
     *
     * @return WeeklyChallenges
     */
    public function setMilestoneHitStamp($milestoneHitStamp)
    {
        $this->_cache->hSave($this->_key, 't', $milestoneHitStamp);

        return $this;
    }

    /**
     * Возвращает объект модели в виде массива.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'm' => $this->getMilestone(),
            't' => $this->getMilestoneHitStamp()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->_cache->hSaveAll(
            $this->_key, array(
                'm'  => 0,
                't'  => 0,
                '_t' => TIME
            )
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDumpPath()
    {
        return parent::getDumpPath() . 'w.json';
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyPrefix()
    {
        return 'w:%s';
    }

    /**
     * {@inheritDoc}
     */
    public function restore($json)
    {
        if (isset($json['w'])) {
            $this->_cache->hSaveAll($this->_key, $json['w']);
            return true;
        }
        return false;
    }
}
