<?php

namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Models\BaseModel;
use App\GameBundle\Core\SplitManager\SplitManager;

/**
 * @file
 * Содержит модель для хранения номера сплита(отдельная ветка настроек).
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Модель для хранения номера сплита пользователя.
 *
 * Хранит объект(хеш) в редисе по ключу "sd:{userId}".
 * Данные объекта:
 * - int  s; // номер сплита
 * - int _t; // время последнего обращения к объекту
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class SplitData extends BaseModel
{
    /**
     * Возвращает id сплита.
     *
     * @return int
     */
    public function getSplitId()
    {
        return $this->_cache->hFetch($this->_key, 's');
    }

    /**
     * Возвращает созданный объект модели c начальными данными.
     *
     * @return SplitData
     */
    public function init()
    {
        $splitId = SplitManager::getNewSplitId();

        if ($splitId != SplitManager::DEFAULT_SPLIT_ID) {
            $this->_cache->hSaveAll(
                $this->_key, array(
                    's'  => $splitId,
                    '_t' => TIME
                )
            );
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDumpPath()
    {
        return parent::getDumpPath() . 'sd.json';
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyPrefix()
    {
        return 'sd:%s';
    }

    /**
     * {@inheritDoc}
     */
    public function restore($json)
    {
        if (isset($json['sd'])) {
            $this->_cache->hSaveAll($this->_key, $json['sd']);
            return true;
        }
        return false;
    }
}
