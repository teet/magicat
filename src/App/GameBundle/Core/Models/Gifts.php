<?php

namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Models\BaseModel;

/**
 * @file
 * Содержит модель данных по текущим подаркам пользователя.
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Модель для хранения подарков от друзей.
 *
 * Хранит объект(хеш) в редисе по ключу "g:{userId}".
 * Данные одного объекта:
 * - array g: // подарки
 *      - string k; // уникальный ключ, по которому можно определить запись в бд(от одного друга может быть много подарков).
 *      - string i; // идентификатор пользователя, от кого был подарок.
 *      - string g; // тип подарка, @see Models/Location/UserDataLocation: $_giftTypes.
 *      - int    t; // время подарка (timestamp).
 * - int  _t; // время последнего обращения к объекту
 *
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class Gifts extends BaseModel
{
    /**
     * Возвращает список подарков пользователя.
     *
     * @return array
     */
    public function getGifts()
    {
        $gifts = $this->_cache->hFetch($this->_key, 'g');
        return json_decode($gifts, true);
    }

    /**
     * Устанавливает список подарков пользователя.
     *
     * @param array $gifts массив подарков
     *
     * @return Gifts
     */
    public function setGifts($gifts)
    {
        $gifts = json_encode($gifts);
        $this->_cache->hSave($this->_key, 'g', $gifts);

        return $this;
    }

    /**
     * Возвращает объект модели в виде массива.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->getGifts();
    }

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->_cache->hSaveAll(
            $this->_key, array(
                'g'  => '[]',
                '_t' => TIME
            )
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDumpPath()
    {
        return parent::getDumpPath() . 'g.json';
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyPrefix()
    {
        return 'g:%s';
    }

    /**
     * {@inheritDoc}
     */
    public function restore($json)
    {
        if (isset($json['g'])) {
            $this->_cache->hSaveAll($this->_key, $json['g']);
            return true;
        }
        return false;
    }
}
