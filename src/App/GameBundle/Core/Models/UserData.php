<?php
namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Models\BaseModel;

/**
 * @file
 * Содержит модель для хранения данных пользователя.
 *
 * @author Amir Kumalov <amir@-emagic.org>
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Модель хранит данные пользователя.
 *
 * Хранит объект(хеш) в редисе по ключу "u:{userId}".
 * Данные:
 *   Тип  Ключ Описание
 * - int    lv; // lives
 * - int    lt; // lastLivesAddStamp
 * - int    cv; // consecutiveVisitsCount
 * - array  sg; // sentGifts
 *     - string i; // toId
 *     - string g; // gift
 *     - int    t; // sentStamp
 * - array  ib; // inGameBoosts
 * - string ub; // unlockedBoostNames
 * - int    st; // spinnerTimeStamp
 * - int    gp; // sendGiftPopupStamp
 * - int    pd; // goldPromoDailyStartStamp
 * - int    ps; // goldPromoStartStamp
 * - bool   pu; // goldPromoUsed
 * - array  ts; // tutorialStates, @see UserDataLocation::$_tutorialIds
 * - array  bi; // boughtItemTransaction
 *     - string i; // itemType
 *     - int    t; // transactionTimeStamp
 * - bool   pm; // isPlayingMission
 * - int    em; // useFreeExtraMovesTimeStamp
 * - int    sq; // socialQuestMask. Битовая маска этапов прохождения социального квеста.
 *              // В младшем байте, будет храниться флаг пройденности квеста.
 *              // В следующем по порядку - будет храниться флаг установки игры другом по приглашению.
 *              // А в остальных, на усмотрение клиента(в каждой соц. сети может быть по-разному, разные этапы и т.д.);
 * - int    gq; // gameQuestId. Id текущего игрового квеста или пустая строка.
 * - array  od; // offerDiscount. Хранит массив из двух элементов для хранения шага и времени доступности акции
 *              // @see http://jira.jgit.ru/confluence/pages/viewpage.action?pageId=18908610
 *     - // step
 *     - // accessTimeStamp
 * - int    _t; // время последнего обращения к объекту
 *
 * @author Amir Kumalov <amir@-emagic.org>
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class UserData extends BaseModel
{
    /**
     * Максимальное количество жизней.
     */
    const MAX_LIVES = 5;

    /**
     * Флаг, указывающий на пройденность социального квеста в значении битовой маски sq(socialQuestMask).
     */
    const SQ_MASK_INVITE_FRIEND = 0x02;

    /**
     * Устанавливает новое значение количества жизней.
     *
     * @param int $lives Количество жизней от 0 до ~
     * @return UserData
     */
    public function setLives($lives)
    {
        $this->_cache->hSave($this->_key, 'lv', $lives);
        return $this;
    }

    /**
     * Возвращает текущее значение количества жизней у пользователя.
     *
     * @return int Возвращает количество жизней
     */
    public function getLives()
    {
        return $this->_cache->hFetch($this->_key, 'lv');
    }

    /**
     * Устанавливает последнее время восстановления жизни.
     *
     * @param int $lastLivesAddStamp Время в unixtime
     * @return UserData
     */
    public function setLastLivesAddStamp($lastLivesAddStamp)
    {
        $this->_cache->hSave($this->_key, 'lt', $lastLivesAddStamp);
        return $this;
    }

    /**
     * Возвращает время последнего восстановления жизни.
     *
     * @return int Время в unixtime
     */
    public function getLastLivesAddStamp()
    {
        return $this->_cache->hFetch($this->_key, 'lt');
    }

    /**
     * Устанавливает количество посещений пользвателя в игру подряд(в разные дни).
     *
     * Если пользователь не заходил больше одного суток, то значение обнуляется.
     * Связано с рулеткой.
     *
     * @param int $consecutiveVisitsCount Количество посещений (>=0)
     * @return UserData
     */
    public function setConsecutiveVisitsCount($consecutiveVisitsCount)
    {
        $this->_cache->hSave($this->_key, 'cv', $consecutiveVisitsCount);
        return $this;
    }

    /**
     * Возвращает количество посещений пользователя в игру подряд(в разные дни).
     *
     * Если пользователь не заходил больше одного суток, то значение обнуляется.
     * Связано с рулеткой.
     *
     * @return int
     */
    public function getConsecutiveVisitsCount()
    {
        return $this->_cache->hFetch($this->_key, 'cv');
    }

    /**
     * Устанавливает список отправленных подарков.
     *
     * Может быть пустой список.
     *
     * @param array $sentGifts Массив объектов с данными о подарках.
     * @return UserData
     */
    public function setSentGifts($sentGifts)
    {
        $this->_cache->hSave($this->_key, 'sg', json_encode($sentGifts));
        return $this;
    }

    /**
     * Возвращает список отправленных подарков.
     *
     * @return array Возвращает ассоциативный массив с данными о подарках.
     */
    public function getSentGifts()
    {
        $sentGifts = $this->_cache->hFetch($this->_key, 'sg');
        return json_decode($sentGifts, true);
    }

    /**
     * Устанавливает список бустеров в игре.
     *
     * @param array $inGameBoosts Список бустеров.
     * @return UserData
     */
    public function setInGameBoosts($inGameBoosts)
    {
        $this->_cache->hSave($this->_key, 'ib', json_encode($inGameBoosts));
        return $this;
    }

    /**
     * Возвращает список бустеров в игре.
     *
     * @return array Ассоциативный массив с данными о бустерах.
     */
    public function getInGameBoosts()
    {
        $inGameBoosts = $this->_cache->hFetch($this->_key, 'ib');
        return json_decode($inGameBoosts, true);
    }

    /**
     * Устанавливает список разблокированных(открытых) бустеров.
     *
     * @param string $unlockedBoostNames Названия бустеров через запятую
     * @return UserData
     */
    public function setUnlockedBoostNames($unlockedBoostNames)
    {
        $this->_cache->hSave($this->_key, 'ub', $unlockedBoostNames);
        return $this;
    }

    /**
     * Возвращает список разблокированных бустеров.
     *
     * @return string Список бустеров через запятую.
     */
    public function getUnlockedBoostNames()
    {
        return $this->_cache->hFetch($this->_key, 'ub');
    }

    /**
     * Устанавливает время игры в рулетку.
     *
     * @param int $spinnerTimeStamp Время в unixtime
     * @return UserData
     */
    public function setSpinnerTimeStamp($spinnerTimeStamp)
    {
        $this->_cache->hSave($this->_key, 'st', $spinnerTimeStamp);
        return $this;
    }

    /**
     * Возвращает время последней игры в рулетку.
     *
     * @return int Время в unixtime
     */
    public function getSpinnerTimeStamp()
    {
        return $this->_cache->hFetch($this->_key, 'st');
    }

    /**
     * Устанавливает время показа окна подарков друзьям.
     *
     * Время запоминается для того, чтобы не показывать окно повторно в течении суток.
     *
     * @param int $sendGiftPopupStamp Время в unixtime
     * @return UserData
     */
    public function setSendGiftPopupStamp($sendGiftPopupStamp)
    {
        $this->_cache->hSave($this->_key, 'gp', $sendGiftPopupStamp);
        return $this;
    }

    /**
     * Возвращает время последнего автоматического показа окна подарков друзьям.
     *
     * @return int Время в unixtime
     */
    public function getSendGiftPopupStamp()
    {
        return $this->_cache->hFetch($this->_key, 'gp');
    }

    /**
     * Устанавливает время начала ежедневных промоакций.
     *
     * @param int $goldPromoDailyStartStamp Время в unixtime
     * @return UserData
     */
    public function setGoldPromoDailyStartStamp($goldPromoDailyStartStamp)
    {
        $this->_cache->hSave($this->_key, 'pd', $goldPromoDailyStartStamp);
        return $this;
    }

    /**
     * Возвращает время начала ежедневных промоакций.
     *
     * @return int Время в unixtime
     */
    public function getGoldPromoDailyStartStamp()
    {
        return $this->_cache->hFetch($this->_key, 'pd');
    }

    /**
     * Устанавливает время начала промоакций.
     *
     * @param int $goldPromoStartStamp Время в unixtime
     * @return UserData
     */
    public function setGoldPromoStartStamp($goldPromoStartStamp)
    {
        $this->_cache->hSave($this->_key, 'ps', $goldPromoStartStamp);
        return $this;
    }

    /**
     * Возвращает время начала промоакций.
     *
     * @return int Время в unixtime
     */
    public function getGoldPromoStartStamp()
    {
        return $this->_cache->hFetch($this->_key, 'ps');
    }

    /**
     * Устанавливает флаг покупки золота промоакции.
     *
     * @param bool $goldPromoUsed Флаг покупки золота промоакции.
     * @return UserData
     */
    public function setGoldPromoUsed($goldPromoUsed)
    {
        $this->_cache->hSave($this->_key, 'pu', $goldPromoUsed);
        return $this;
    }

    /**
     * Возвращает флаг покупки золота по промоакции.
     *
     * @return bool Возвращает true, если покупал, иначе false
     */
    public function getGoldPromoUsed()
    {
        return (boolean)$this->_cache->hFetch($this->_key, 'pu');
    }

    /**
     * Устанавливает список состояний пройденности этапов туториала.
     *
     * @param array Ассоциативный массив с данными пройденных этапов.
     * @return UserData
     */
    public function setTutorialStates($tutorialStates)
    {
        $this->_cache->hSave($this->_key, 'ts', json_encode($tutorialStates));
        return $this;
    }

    /**
     * Возвращает список состояний этапов туториала.
     *
     * @return array ассоциативный массив с данными
     */
    public function getTutorialStates()
    {
        $tutorialStates = $this->_cache->hFetch($this->_key, 'ts');
        return json_decode($tutorialStates, true);
    }

    /**
     * Обновляет информацию о последней покупке.
     *
     * @param array $boughtItemTransaction Данные покупки
     * @return UserData
     */
    public function setBoughtItemTransaction($boughtItemTransaction)
    {
        $boughtItemTransaction = json_encode($boughtItemTransaction);
        $this->_cache->hSave($this->_key, 'bi', $boughtItemTransaction);
        return $this;
    }

    /**
     * Возвращает данные последней покупки.
     *
     * @return array Массив с данными
     */
    public function getBoughtItemTransaction()
    {
        $boughtItemTransaction = $this->_cache->hFetch($this->_key, 'bi');
        return json_decode($boughtItemTransaction, true);
    }

    /**
     * Устанавливает флаг состояний игры.
     *
     * Вначале игры устанавливается true, в конце - false.
     *
     * @param bool $isPlayingMission Флаг состояния игры
     * @return UserData
     */
    public function setIsPlayingMission($isPlayingMission)
    {
        $this->_cache->hSave($this->_key, 'pm', $isPlayingMission);
        return $this;
    }

    /**
     * Возвращает флаг состояний прохождения миссии.
     *
     * @return bool
     */
    public function getIsPlayingMission()
    {
        return (bool)$this->_cache->hFetch($this->_key, 'pm');
    }

    /**
     * Устанавливает время использования бесплатных дополнительных ходов.
     *
     * @param integer $useFreeExtraMovesTimeStamp Время в unixtime
     * @return UserData
     */
    public function setUseFreeExtraMovesTimeStamp($useFreeExtraMovesTimeStamp)
    {
        $this->_cache->hSave($this->_key, 'em', $useFreeExtraMovesTimeStamp);
        return $this;
    }

    /**
     * Возвращает время использования бесплатных дополнительных ходов.
     *
     * @return integer
     */
    public function getUseFreeExtraMovesTimeStamp()
    {
        return $this->_cache->hFetch($this->_key, 'em');
    }

    /**
     * Устанавливает новое значение битовой маски этапов прохождения социального квеста.
     *
     * @param int $questMask Битовая маска
     * @return UserData
     */
    public function setSocialQuestMask($questMask)
    {
        $this->_cache->hSave($this->_key, 'sq', $questMask);
        return $this;
    }

    /**
     * Возвращает текущее значение битовой маски этапов прохождения социального квеста.
     *
     * @return int Возвращает битовую маску этапов прохождения квеста
     */
    public function getSocialQuestMask()
    {
        return $this->_cache->hFetch($this->_key, 'sq');
    }

    /**
     * Устанавливает новое значение идентификатора текущего квеста.
     *
     * @param string $questId Id квеста или пустая строка
     * @return UserData
     */
    public function setGameQuestId($questId)
    {
        $this->_cache->hSave($this->_key, 'gq', $questId);
        return $this;
    }

    /**
     * Возвращает текущее значение начатого игрового квеста пользователя.
     *
     * @return string Возвращает пустую строку или идентификатор квеста
     */
    public function getGameQuestId()
    {
        return $this->_cache->hFetch($this->_key, 'gq');
    }

    /**
     * Устанавливает новое значение этапа и времени доступности акции.
     *
     * @param array $offerDiscount Массив с данными об этапе и времени доступности акции
     * @return UserData
     */
    public function setOfferDiscount($offerDiscount)
    {
        $this->_cache->hSave($this->_key, 'od', json_encode($offerDiscount));
        return $this;
    }

    /**
     * Возвращает значение этапа и времени доступности акции.
     *
     * @return array Возвращает массив, где первый элемент - этап, второй - время доступности акции
     */
    public function getOfferDiscount()
    {
        $offerDiscount = $this->_cache->hFetch($this->_key, 'od');
        return json_decode($offerDiscount, true);
    }

    /**
     * Возвращает все данные пользователя в виде ассоциативного массива.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'k'  => $this->getUserId(),
            'lv' => $this->getLives(),
            'lt' => $this->getLastLivesAddStamp(),
            'cv' => $this->getConsecutiveVisitsCount(),
            'sg' => $this->getSentGifts(),
            'ib' => $this->getInGameBoosts(),
            'ub' => $this->getUnlockedBoostNames(),
            'st' => $this->getSpinnerTimeStamp(),
            'gp' => $this->getSendGiftPopupStamp(),
            'pd' => $this->getGoldPromoDailyStartStamp(),
            'ps' => $this->getGoldPromoStartStamp(),
            'pu' => $this->getGoldPromoUsed(),
            'ts' => $this->getTutorialStates(),
            'bi' => $this->getBoughtItemTransaction(),
            'pm' => $this->getIsPlayingMission(),
            'em' => $this->getUseFreeExtraMovesTimeStamp(),
            'sq' => $this->getSocialQuestMask(),
            'gq' => $this->getGameQuestId(),
            'od' => $this->getOfferDiscount()
        );
    }

    /**
     * Возвращает созданный объект c начальными данными.
     *
     * @return UserData
     */
    public function init()
    {
        $this->_cache->hSaveAll(
            $this->_key, array(
                'lv' => self::MAX_LIVES,
                'lt' => TIME,
                'cv' => 0,
                'sg' => '[]',
                'ib' => '[]',
                'ub' => '',
                'st' => 0,
                'gp' => 0,
                'pd' => 0,
                'ps' => 0,
                'pu' => false,
                'ts' => '{"b9":0,"c4":9}',
                'bi' => '[]',
                'pm' => false,
                'em' => 0,
                'sq' => 0,
                'gq' => '',
                'od' => '[1,0]',
                '_t' => TIME
            )
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDumpPath()
    {
        return parent::getDumpPath() . 'u.json';
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyPrefix()
    {
        return 'u:%s';
    }

    /**
     * {@inheritDoc}
     */
    public function restore($json)
    {
        if (isset($json['u'])) {
            $this->_cache->hSaveAll($this->_key, $json['u']);
            return true;
        }
        return false;
    }
}
