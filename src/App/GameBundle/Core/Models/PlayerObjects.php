<?php
namespace App\GameBundle\Core\Models;

use App\GameBundle\Core\Models\BaseModel;
use App\GameBundle\Core\Progress\Location\ProgressLocation;
use App\GameBundle\Core\Item\StageCatalog;

/**
 * @file
 * Содержит модель для хранения данных игры.
 *
 * @author Amir Kumalov <amir@-emagic.org>
 * @author Azamat Bogotov <azamat@-emagic.org>
 */

/**
 * Модель для хранения данных игры.
 *
 * Хранит объект(хеш) в редисе по ключу "p:{userId}".
 * Данные:
 * - int   s; // количество клубник(strawberry)
 * - array m: // сейв(missionsInfo). Массив, где i-ый элемент с данными:
 *     - string m; // missionId ("1_1","1_2")
 *     - int    s; // starsAchieved (0-3)
 *     - bool   p; // isPassed(false|true)
 * - array c; // коллекция(collectibleList)
 *     - int   i; // packId.           Номер локации.
 *     - int   p; // probabilityIndex. Шаг вероятности выпадения.
 *     - array e; // list.             Список элементов в коллекции.
 *         - string m; // gainMissionId. Номер миссии на которой получил.
 *         - bool   c; // isCollected.   Полученность коллекционного предмета.
 * - int   i; // время установки игры(installStamp)
 * - int   p; // время последней игры(lastPlayStamp)
 * - int  _t; // время последнего обращения к объекту
 *
 * При передаче всего объекта клиенту, еще добавляется ключ "k" - идентификатор пользователя.
 * Этот ключ не храниться в БД, т.к. он итак известен.
 * @see toArray()
 *
 * @author Amir Kumalov <amir@-emagic.org>
 * @author Azamat Bogotov <azamat@-emagic.org>
 */
class PlayerObjects extends BaseModel
{
    /**
     * Set strawberry.
     *
     * @param integer $strawberry
     * @return void
     */
    public function setStrawberry($strawberry)
    {
        $this->_cache->hSave($this->_key, 's', $strawberry);
    }

    /**
     * Get strawberry.
     *
     * @return string
     */
    public function getStrawberry()
    {
        return $this->_cache->hFetch($this->_key, 's');
    }

    /**
     * Set missionsInfo.
     *
     * @param string $missionsInfo
     * @return void
     */
    public function setMissionsInfo($missionsInfo)
    {
        $missionsInfo = json_encode($missionsInfo);
        $this->_cache->hSave($this->_key, 'm', $missionsInfo);
    }

    /**
     * Get missionsInfo.
     *
     * @return string
     */
    public function getMissionsInfo()
    {
        $missionsInfo = $this->_cache->hFetch($this->_key, 'm');
        return json_decode($missionsInfo, true);
    }

    /**
     * Set collectibleList.
     *
     * @param string $collectibleList
     * @return PlayerObjects
     */
    public function setCollectibleList($collectibleList)
    {
        $collectibleList = json_encode($collectibleList);
        $this->_cache->hSave($this->_key, 'c', $collectibleList);
    }

    /**
     * Get collectibleList.
     *
     * @return string
     */
    public function getCollectibleList()
    {
        $collectibleList = $this->_cache->hFetch($this->_key, 'c');
        return json_decode($collectibleList, true);
    }

    /**
     * Set installStamp.
     *
     * @param integer $installStamp
     * @return PlayerObjects
     */
    public function setInstallStamp($installStamp)
    {
        $this->_cache->hSave($this->_key, 'i', $installStamp);
    }

    /**
     * Get installStamp.
     *
     * @return integer
     */
    public function getInstallStamp()
    {
        return $this->_cache->hFetch($this->_key, 'i');
    }

    /**
     * Set lastPlayStamp.
     *
     * @param integer $lastPlayStamp
     * @return PlayerObjects
     */
    public function setLastPlayStamp($lastPlayStamp)
    {
        $this->_cache->hSave($this->_key, 'p', $lastPlayStamp);
    }

    /**
     * Get lastPlayStamp.
     *
     * @return integer
     */
    public function getLastPlayStamp()
    {
        return $this->_cache->hFetch($this->_key, 'p');
    }

    /**
     * Return PlayerObject object as array.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            's' => $this->getStrawberry(),
            'm' => $this->getMissionsInfo(),
            // HACK: Отключение функционала коллекционных зверей с v5.9.
            //'c' => $this->getCollectibleListArray(),
            'i' => $this->getInstallStamp(),
            'p' => $this->getLastPlayStamp()
        );
    }

    /**
     * Возвращает доступную коллекцию панд для каждого острова.
     *
     * Для не полученных коллекций устанавливаются параметры по-умолчанию.
     *
     * @return array
     */
    public function getCollectibleListArray()
    {
        $collectibleList = $this->getCollectibleList();

        $item = array(
            'i' => 0,
            'p' => 0,
            'e' => array()
        );

        for ($i = 0; $i < ProgressLocation::MAX_COLLECTION_IN_ISLAND; ++$i) {
            $item['e'][] = array(
                'm' => '',
                'c' => false
            );
        }

        $islandsCount = StageCatalog::getIslandsCount($this->getUserId());
        for ($id = count($collectibleList) + 1; $id <= $islandsCount; ++$id) {
            $item['i']         = $id;
            $collectibleList[] = $item;
        }

        return $collectibleList;
    }

    /**
     * Возвращает созданный объект модели c начальными данными.
     *
     * @return PlayerObjects
     */
    public function init()
    {
        $this->_cache->hSaveAll(
            $this->_key, array(
                's'  => 0,
                'm'  => '[]',
                'c'  => '[]',
                'i'  => TIME,
                'p'  => TIME,
                '_t' => TIME
            )
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDumpPath()
    {
        return parent::getDumpPath() . 'p.json';
    }

    /**
     * {@inheritDoc}
     */
    public function getKeyPrefix()
    {
        return 'p:%s';
    }

    /**
     * {@inheritDoc}
     */
    public function restore($json)
    {
        if (isset($json['p'])) {
            $this->_cache->hSaveAll($this->_key, $json['p']);
            return true;
        }
        return false;
    }
}
