<?php

namespace App\GameBundle\Core\Models\Location;

use App\GameBundle\Core\Models\Coins;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Item\ItemCatalog;

class CoinsLocation
{
    /**
     * Хранит ссылку на объект Coins
     *
     * @var Coins
     */
    private $_coinsObject;

    /**
     * Конструктор.
     *
     * @param Coins $coinsObject    ссылка на объект Coins
     */
    public function __construct(Coins &$coinsObject)
    {
        $this->_coinsObject = $coinsObject;
    }

    /**
     * Вычитывает стоимость покупаемых бустеров со счета и возвращает остаток.
     *
     * @param string $boosts    строка покупаемых бустеров
     * @return integer
     * @throws GameException
     */
    public function buyBoosts($boosts)
    {
        if (empty($boosts)) {
            throw new GameException('There are no subjects for param "boosts".', GameException::WRONG_PARAM);
        }

        $goods = explode(',', $boosts);
        if (empty($goods) || sizeof($goods) <= 0) {
            throw new GameException('There are no goods for purchase.', GameException::WRONG_PARAM);
        }

        $userData = UserData::getObject($this->_coinsObject->getUserId());

        $unlockedBoostNames = $userData->getUnlockedBoostNames();
        $unlockedBoostsArray = explode(',', $unlockedBoostNames);

        if (empty($unlockedBoostsArray) || sizeof($unlockedBoostsArray) <= 0) {
            throw new GameException('There are no unlocked boosts.', GameException::WRONG_ACTION);
        }

        $priceSum = 0;
        foreach ($goods as $boost) {
            if ($boost == '') {
                continue;
            }

            if (!in_array($boost, $unlockedBoostsArray)) {
                throw new GameException(
                    sprintf('Boost "%s" not unlocked.', $boost),
                    GameException::WRONG_ACTION
                );
            }

            $item = ItemCatalog::getItem($boost, $this->_coinsObject->getUserId());
            if ($item === null) {
                throw new GameException(
                    sprintf('Boost "%s" not found in ItemCatalog.', $boost),
                    GameException::GAME_ERROR
                );
            }

            if (!isset($item['PriceCoins'])) {
                throw new GameException(
                    sprintf('Boost "%s" has no price.', $boost),
                    GameException::GAME_ERROR
                );
            }

            $priceSum += $item['PriceCoins'];
        }

        if ($priceSum > 0) {
            $coins = $this->_coinsObject->getCoins();
            if ($priceSum > $coins) {
                throw new GameException(
                    sprintf('There are no coins: %s < price: %s.', $coins, $priceSum),
                    GameException::NO_COINS
                );
            }

            $remainder = max($coins - $priceSum, 0);
            return $remainder;
        }

        throw new GameException('Nothing to buy.', GameException::GAME_ERROR);
    }
}
