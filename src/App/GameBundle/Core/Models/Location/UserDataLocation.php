<?php

namespace App\GameBundle\Core\Models\Location;

use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Item\ItemCatalog;
use App\GameBundle\Core\Item\QuestCatalog;

class UserDataLocation
{
    /**
     * Хранит доступные идентификаторы туториала.
     *
     * Храниться в виде ключ:значение для удобства проверки через функцию isset().
     * Сами значения не используются, просто для расшифровки ключа.
     *
     * @var array
     */
    private static $_tutorialIds = array(
        'a1' => 'hasFreeSpin',
        'a2' => 'collectibleAnnouncementPopup',
        'a3' => 'trophyButton',
        'a4' => 'baboonClapPopup',
        'a5' => 'baboonIceStickPopup',
        'a6' => 'fatalCubePopup',
        'a7' => 'questionCubePopup',
        'a8' => 'spinButton',
        'a9' => 'inGameCollectibePanda',
        'b1' => 'boost',
        'b2' => 'ingameChineseFirework',
        'b3' => 'ingameRadialBomb',
        'b4' => 'bomb',
        'b5' => 'colorBomb',
        'b6' => 'chineseFirework',
        'b7' => 'radialBomb',
        'b8' => 'collectibleReminderSeenCount',
        'b9' => 'seenNewAddedIslandCount',
        'c1' => 'forceAddRowTutorialCount',
        'c2' => 'gameQuest',
        'c3' => 'ingameExtraMoves',
        'c4' => 'showShopDialogCount'
    );

    /**
     * Хранит доступные типы подарков.
     *
     * Храниться в виде ключ:значение для удобства проверки через функцию isset().
     *
     * @var array
     */
    private static $_giftTypes = array(
        'energy'             => 0,
        'gold'               => 0,
        'bonusForExtraMoves' => 0,
        'ask'                => 0
    );

    /**
     * Хранит ссылку на объект UserData.
     *
     * @var UserData
     */
    private $_userData;

    /**
     * Конструктор.
     *
     * @param UserData $userData    ссылка на объект UserData
     */
    public function __construct(UserData &$userData)
    {
        $this->_userData = $userData;
    }

    /**
     * Проверка флага прохождения на корректность.
     *
     * @param bool $isPlayingMission    флаг прохождения
     * @return bool
     */
    public function checkIsPlayingMission($isPlayingMission)
    {
        if (!is_bool($isPlayingMission)) {
            throw new GameException(
                sprintf('The wrong value of param "isPlayingMission": %s.', $isPlayingMission),
                GameException::WRONG_PARAM
            );
        }

        return (bool)$isPlayingMission;
    }

    /**
     * Проверка жизней на корректность.
     *
     * @param integer $lives    итоговое значение жизней.
     * @return integer
     * @throws GameException
     */
    public function checkLives($lives)
    {
        $lives = intval($lives);
        if ($lives < 0) {
            throw new GameException(
                sprintf('The negative value of param "lives": %s.', $lives),
                GameException::WRONG_PARAM
            );
        }

        return $lives;
    }

    /**
     * Проверка на корректность времени обновления жизни.
     *
     * @param integer $lastLivesAddStamp    время обвноления жизни
     * @return integer
     * @throws GameException
     */
    public function checkLastLivesAddStamp($lastLivesAddStamp)
    {
        $lastLivesAddStamp = intval($lastLivesAddStamp);
        if ($lastLivesAddStamp <= 0) {
            throw new GameException(
                sprintf('Wrong value of param "lastLivesAddStamp": %s.', $lastLivesAddStamp),
                GameException::WRONG_PARAM
            );
        }

        return $lastLivesAddStamp;
    }

    /**
     * Проверяет полученную строку бустеров на существование идентификаторов.
     *
     * @param string $unlockedBoostNames    строка идентификаторов открываемых бустеров
     *
     * @return string                       проверенная строка
     * @throws GameException
     */
    public function checkUnlockedBoostNames($unlockedBoostNames)
    {
        if (empty($unlockedBoostNames)) {
            throw new GameException('UnlockedBoostNames error.', GameException::WRONG_PARAM);
        }

        $boosts = explode(',', $unlockedBoostNames);
        if (empty($boosts)) {
            throw new GameException('UnlockedBoostNames is empty.', GameException::WRONG_PARAM);
        }

        foreach ($boosts as $boost) {
            if ($boost == '') {
                continue;
            }

            $item = ItemCatalog::getItem($boost, $this->_userData->getUserId());
            if ($item === null) {
                throw new GameException(sprintf('Boost %s not found.', $boost), GameException::WRONG_PARAM);
            }
        }

        return $unlockedBoostNames;
    }

    /**
     * Проверяет на валидность счетчик посещений.
     *
     * @param integer $consecutiveVisitsCount   счетчик посещений
     * @return integer
     * @throws GameException
     */
    public function checkConsecutiveVisitsCount($consecutiveVisitsCount)
    {
        $consecutiveVisitsCount = intval($consecutiveVisitsCount);
        if ($consecutiveVisitsCount < 0) {
            throw new GameException(
                sprintf('The negative value of param "consecutiveVisitsCount": %s.', $consecutiveVisitsCount),
                GameException::WRONG_PARAM
            );
        }

        return $consecutiveVisitsCount;
    }

    /**
     * Проверяет на валидность последнее время пользования ежедневной лотореей.
     *
     * @param integer $spinnerTimeStamp   время unixtime
     * @return integer
     * @throws GameException
     */
    public function checkSpinnerTimeStamp($spinnerTimeStamp)
    {
        $spinnerTimeStamp = intval($spinnerTimeStamp);
        if ($spinnerTimeStamp <= 0) {
            throw new GameException(
                sprintf('Wrong value of param "spinnerTimeStamp": %s.', $spinnerTimeStamp),
                GameException::WRONG_PARAM
            );
        }

        return $spinnerTimeStamp;
    }

    /**
     * Обновляет параметры туториала.
     *
     * @param array $tutorialStates     сохраняемый туториал
     * @return array
     * @throws GameException
     */
    public function updateTutorialStates($tutorialStates)
    {
        if (!is_array($tutorialStates) || count($tutorialStates) <= 0) {
            throw new GameException('Parameter "tutorialStates" must be not empty Array.', GameException::WRONG_PARAM);
        }

        foreach ($tutorialStates as $tutorialId => $value) {
            if (!isset(self::$_tutorialIds[$tutorialId])) {
                throw new GameException(
                    sprintf('Undefined id tutorial - %s.', $tutorialId),
                    GameException::WRONG_PARAM
                );
            }
        }

        $currentTutorialStates = $this->_userData->getTutorialStates();
        $resultTutorialStates  = array_merge($currentTutorialStates, $tutorialStates);

        return $resultTutorialStates;
    }

    /**
     * Обновляет количество супер бустеров.
     *
     * @param array $inGameBoosts     супер бустеры
     * @return array
     * @throws GameException
     */
    public function updateInGameBoosts($inGameBoosts)
    {
        if (!is_array($inGameBoosts)) {
            throw new GameException(sprintf('InGameBoost[%s] is not an array.', $inGameBoosts),
                                    GameException::WRONG_PARAM);
        }

        foreach ($inGameBoosts as $inGameBoost => $value) {
            $item = ItemCatalog::getItem($inGameBoost, $this->_userData->getUserId());
            if ($item === null || $item['type'] != 'ingameBoostProduct') {
                throw new GameException(sprintf('InGameBoost %s not found.', $inGameBoost), GameException::WRONG_PARAM);
            }

            $value = intval($value);
            if ($value < 0) {
                throw new GameException(sprintf('InGameBoost %s error value %s.', $inGameBoost, $value),
                                        GameException::WRONG_PARAM);
            }

            $inGameBoosts[$inGameBoost] = $value;
        }

        $currentBoosts = $this->_userData->getInGameBoosts();

        return array_merge($currentBoosts, $inGameBoosts);
    }

    /**
     * Обновление информации о последней покупке.
     *
     * @param array $boughtItemTransaction     информация о последней покупке
     * @return array
     * @throws GameException
     */
    public function updateBoughtItemTransaction($boughtItemTransaction)
    {
        if (!isset($boughtItemTransaction['t'])) {
            throw new GameException(
                'Wrong input data "boughtItemTransaction" in UserData object',
                GameException::MISSING_PARAM
            );
        }

        $boughtItemTransaction['t'] = intval($boughtItemTransaction['t']);

        if ($boughtItemTransaction['t'] <= 0) {
            throw new GameException(
                sprintf('Wrong value of param "transactionTimeStamp": %s.', $boughtItemTransaction['t']),
                GameException::WRONG_PARAM
            );
        }

        $boughtItem      = $this->_userData->getBoughtItemTransaction();
        $boughtItem['t'] = $boughtItemTransaction['t'];

        if (isset($boughtItemTransaction['i'])) {
            $item = ItemCatalog::getItem($boughtItemTransaction['i'], $this->_userData->getUserId());

            if ($item === null) {
                throw new GameException(
                    sprintf('boughtItemTransaction item %s not found.', $boughtItemTransaction['i']),
                    GameException::WRONG_PARAM
                );
            }

            $boughtItem['i'] = $boughtItemTransaction['i'];
        }

        return $boughtItem;
    }

    /**
     * Проверяет последнее время показа окна отправки подарков на валидность.
     *
     * @param int $sendGiftPopupStamp unixtime-время в миллисекундах
     *
     * @return float Возвращает преобразованное время в виде вещественного числа, т.к. в int не помещается
     * @throw GameException
     */
    public function updateSendGiftPopupStamp($sendGiftPopupStamp)
    {
        $mTimeStamp = intval($sendGiftPopupStamp);

        if ($mTimeStamp <= 0) {
            throw new GameException(
                sprintf('Wrong value of param "sendGiftPopupStamp": %s.', $sendGiftPopupStamp),
                GameException::WRONG_PARAM
            );
        }

        return $mTimeStamp;
    }

    /**
     * Проверяет и обновляет список подарков, которые были отправлены в текущие сутки.
     *
     * @param array $sentGifts массив подарков
     *
     * @return array Возвращает массив подарков
     * @throw GameException
     */
    public function updateSentGifts($sentGifts)
    {
        if (!is_array($sentGifts)) {
            throw new GameException(
                'Parameter sentGifts is not an array',
                GameException::WRONG_PARAM
            );
        }

        $checkedSentGifts = array();

        foreach ($sentGifts as $key => $gift) {
            if (!is_array($gift)) {
                throw new GameException(
                    'Parameter gift is not an array',
                    GameException::WRONG_PARAM
                );
            }

            // Если объект пустой, то пропускаем его.
            if (sizeof($gift) <= 0) {
                continue;
            }

            if (!isset($gift['i']) ||
                !isset($gift['g']) ||
                !isset($gift['t'])
            ) {
                throw new GameException(
                    sprintf('Some parameters are empty in gift[%s].', print_r($gift, true)),
                    GameException::WRONG_PARAM
                );
            }

            $sentGifts[$key]['t'] = intval($gift['t']);

            if ($sentGifts[$key]['t'] <= 0) {
                throw new GameException(
                    sprintf('Wrong value of param "sentStamp" in gift[%s].', print_r($gift, true)),
                    GameException::WRONG_PARAM
                );
            }

            if (!isset(self::$_giftTypes[$gift['g']])) {
                throw new GameException(
                    sprintf('Wrong param "gift" in gift[%s].', print_r($gift, true)),
                    GameException::WRONG_PARAM
                );
            }

            if ($gift['i'] <= 0) {
                throw new GameException(
                    sprintf('Wrong param "toId" in gift[%s].', print_r($gift, true)),
                    GameException::WRONG_PARAM
                );
            }

            $checkedSentGifts[] = $gift;
        }

        return $checkedSentGifts;
    }

    /**
     * Возвращает список возможных идентификаторов для шагов туториала.
     *
     * @return array Ассоциативный массив, где ключ - id туториала, значение - полное имя id туториала
     */
    public static function getTutorialStatesList()
    {
        return self::$_tutorialIds;
    }

    /**
     * Возвращает список названий имеющихся типов подарков.
     *
     * @return array Ассоциативный массив, где ключ - название подарка, значение - 0
     */
    public static function getGiftTypes()
    {
        return self::$_giftTypes;
    }

    /**
     * Проверяет на валидность время последнего использования
     * 5 бесплатных дополнительных ходов.
     *
     * @param integer $useFreeExtraMovesTimeStamp   время unixtime
     * @return integer
     * @throws GameException
     */
    public function checkUseFreeExtraMovesTimeStamp($useFreeExtraMovesTimeStamp)
    {
        $useFreeExtraMovesTimeStamp = intval($useFreeExtraMovesTimeStamp);
        if ($useFreeExtraMovesTimeStamp <= 0) {
            throw new GameException(
                sprintf('Wrong value of param "useFreeExtraMovesTimeStamp": %s.', $useFreeExtraMovesTimeStamp),
                GameException::WRONG_PARAM
            );
        }

        return $useFreeExtraMovesTimeStamp;
    }

    /**
     * Проверяет значение идентификатора начатого квеста.
     *
     * @param string $questId Идентификатор квеста
     * @return string Идентификатор квеста
     * @throws GameException
     */
    public function updateGameQuest($questId)
    {
        if ($questId == '' || QuestCatalog::getQuest($questId) === null) {
            // Нельзя начать несуществующий(возможно удаленный) квест.
            throw new GameException(
                sprintf('Wrong value of param "questId"[%s].', $questId),
                GameException::WRONG_PARAM
            );
        }

        return $questId;
    }
}
