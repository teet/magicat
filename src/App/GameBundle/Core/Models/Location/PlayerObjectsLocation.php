<?php

namespace App\GameBundle\Core\Models\Location;

use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Progress\Location\ProgressLocation;
use App\GameBundle\Core\Exception\GameException;

class PlayerObjectsLocation
{
    /**
     * Хранит ссылку на объект PlayerObjects.
     *
     * @var PlayerObjects
     */
    private $_playerObjects;

    /**
     * Конструктор.
     *
     * @param PlayerObjects $playerObjects  ссылка на объект PlayerObjects.
     */
    public function __construct(PlayerObjects &$playerObjects)
    {
        $this->_playerObjects = $playerObjects;
    }

    /**
     * Обновляет прогресс прохождения.
     *
     * @param array $data   массив параметров объекта PlayerObjects
     */
    public function updateProgress($data)
    {
        $strawberry      = null;
        $lastPlayStamp   = null;
        $missionsInfo    = null;
        $collectibleList = null;

        if (isset($data['s'])) {
            $strawberry = $this->checkStrawberry($data['s']);
        }
        if (isset($data['p'])) {
            $lastPlayStamp = $this->checkLastPlayStamp($data['p']);
        }

        $location = new ProgressLocation($this->_playerObjects);
        if (isset($data['m'])) {
            foreach ($data['m'] as $mission) {
                $missionsInfo = $location->updateMissionsInfo($mission);
            }
        }
        // HACK: Отключение функционала коллекционных зверей с v5.9.
        /*if (isset($data['c']) && isset($data['c'][0])) {
            $collectibleList = $location->updateCollectibleList($data['c'][0]);
        }*/

        if ($strawberry !== null) {
            $this->_playerObjects->setStrawberry($strawberry);
        }
        if ($lastPlayStamp !== null) {
            $this->_playerObjects->setLastPlayStamp($lastPlayStamp);
        }
        if ($missionsInfo !== null) {
            $this->_playerObjects->setMissionsInfo($missionsInfo);
        }
        if ($collectibleList !== null) {
            $this->_playerObjects->setCollectibleList($collectibleList);
        }
    }

    /**
     * Проверяет на корректность сохраняемые клубники.
     * Возвращает входные данные в случае корректности.
     *
     * @param integer $strawberry   клубники
     * @return integer              клубники
     * @throws GameException
     */
    public function checkStrawberry($strawberry)
    {
        $strawberry = intval($strawberry);
        if ($strawberry < 0) {
            throw new GameException(
                sprintf('The negative value of param "strawberry": %s.', $strawberry),
                GameException::WRONG_PARAM
            );
        }

        return $strawberry;
    }

    /**
     * Проверяет на корректность время последней игры.
     * Возвращает входные данные в случае корректности.
     *
     * @param integer $lastPlayStamp   время в unixtime
     * @return integer время в unixtime
     * @throws GameException
     */
    public function checkLastPlayStamp($lastPlayStamp)
    {
        $lastPlayStamp = intval($lastPlayStamp);
        if ($lastPlayStamp <= 0) {
            throw new GameException(
                sprintf('Wrong value of param "lastPlayStamp": %s.', $lastPlayStamp),
                GameException::WRONG_PARAM
            );
        }

        return $lastPlayStamp;
    }
}
