<?php

namespace App\GameBundle\Core\Models\Location;

use App\GameBundle\Core\Models\WeeklyChallenges;
use App\GameBundle\Core\Exception\GameException;

/**
 * Вспомогательный класс для проверки данных о еженедельном клубничном рейтинге.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class WeeklyChallengesLocation
{
    /**
     * Хранит ссылку на объект WeeklyChallenges.
     *
     * @var WeeklyChallenges
     */
    private $_weeklyChallenges;

    /**
     * Конструктор.
     *
     * @param WeeklyChallenges $weeklyChallenges ссылка на объект WeeklyChallenges
     */
    public function __construct(WeeklyChallenges &$weeklyChallenges)
    {
        $this->_weeklyChallenges = $weeklyChallenges;
    }

    /**
     * Проверка на корректность номера этапа еженедельного рейтинга.
     *
     * @param integer $milestone номер этапа еженедельного рейтинга
     *
     * @return integer
     * @throws GameException
     */
    public function checkAndGetMilestone($milestone)
    {
        $milestone = intval($milestone);

        if ($milestone < 0 || $milestone > 5) {
            throw new GameException('Milestone[' . $milestone . '] error.', GameException::WRONG_PARAM);
        }

        return $milestone;
    }

    /**
     * Проверка на корректность времени получения монет за этап еженедельного рейтинга.
     *
     * @param integer $milestoneHitStamp время получения монет за этап еженедельного рейтинга(в миллисекундах)
     *
     * @return int
     * @throws GameException
     */
    public function checkAndGetMilestoneHitStamp($milestoneHitStamp)
    {
        $mHitStamp = intval($milestoneHitStamp);

        if ($mHitStamp <= 0) {
            throw new GameException('MilestoneHitStamp[' . $milestoneHitStamp . '] error.', GameException::WRONG_PARAM);
        }

        return $mHitStamp;
    }
}
