<?php

namespace App\GameBundle\Core\Log;

use App\GameBundle\Core\Config\Config;

class Log
{
    /**
     * File extesion
     *
     * @var string
     *
     */
    protected $_extension = 'csv';
    /**
     * Path to log file
     *
     * @var string
     *
     */
    protected $_path;

    /**
     * Handle to opened file (fopen)
     *
     * @var resource
     *
     */
    protected $_handle = null;

    /**
     * __construct
     *
     * @param mixed  $var       Can be instanse of class (class name would be used ) or string.
     * @param string $extension Extension for log file
     * @return Log Instanse of this class
     *
     */
    public function __construct($var, $extension = '')
    {
        if (is_object($var)) {
            $fileName = get_class($var);
        } else if (is_string($var)) {
            $fileName = $var;
        } else {
            throw new Exception('Unexcepted type for input var');
        }

        if ($extension != '') {
            $this->_extesion = $extension;
        }

        $this->_path = sprintf("%s/%s.%s", Config::get('logDir'), $fileName, $this->_extension);
        $this->_handle = fopen($this->_path, 'a+');
    }

    /**
     * This  __destruct
     *
     * @return void
     *
     */
    public function __destruct()
    {
        if ($this->_handle) {
            fclose($this->_handle);
        }
    }

    /**
     * write - write to log stream
     *
     * @param mixed $param
     * @return void
     *
     */
    public function write($param)
    {
        $format = 'Y-m-d H:i:s';
        $message = sprintf("%s;%s\n",  date($format), $param);
        fwrite($this->_handle, $message);
    }
}
