<?php

/**
 * @file
 * Содержит код рассылки массовых оповещений в сети Одноклассники.
 *
 * @see http://jira.jgit.ru/confluence/pages/viewpage.action?pageId=18910091
 * @see http://apiok.ru/wiki/pages/viewpage.action?pageId=46137373 метод notifications.sendMass
 *
 * cron: 10 4 * * * cd ~/html/src/App/GameBundle/Core/Cron/; /usr/bin/php -f odSendMass.php
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

namespace App\GameBundle\Core\Cron;

use App\GameBundle\Core\SocialNetworks\Api\SocialNetworkOD;

$loader = require_once '../../../../../app/bootstrap.php.cache';
set_time_limit(0);

$api            = new SocialNetworkOD();
$currentTime    = time();
$notifyLifeTime = 86400 * 20; // Срок действия оповещения (20 дней).
$sendApiTimeout = 2;          // таймаут ожидания между запросами к апи.

// Тексты сообщений
$textMessages = array(
    'Привет, ${user.first-name}! Еще не все котята спасены! Не оставляй их в беде!',
    'У тебя восстановились все сердечки. Скорее, помоги Анюте спасти своих котят от Злого Мага!',
    'Злой Маг похитил маленьких котят! Срочно нужна твоя помощь!',
    'Хочешь получить бесплатно монетки? Скорее сыграй в ежедневную лотерею и получи!',
    'Скорее заходи в игру! Анюта одна не справляется со Злым Магом!',
    'Котята в опасности! Скорее в игру, на помощь Анюте!'
);

// Критерии фильтрации для метода notifications.sendMass.
$filter = array(
    //'text'               => '',            // должен быть задан позже, иначе будет ошибка запроса к апи.
    //'status'             => 'ADMINS_ONLY', // оповещения увидят только админы приложения
    'expires'            => date('Y.m.d H:i', $currentTime + $notifyLifeTime),
    'first_access_range' => '',
    'last_access_range'  => ''
);

// На следующий день после регистрации в игре, если пользователь еще не заходил в игру.
$yesterday                    = date("Y.m.d", $currentTime - 86400);
$filter['text']               = $textMessages[0];
$filter['first_access_range'] = $yesterday . '-' . $yesterday;
$filter['last_access_range']  = $yesterday . '-' . $yesterday;

if ($api->sendMassNotifications($filter) === false) {
    echo 'error: ' . $api->getErrorCode() . '|' . $api->getErrorText();
    if ($api->getErrorCode() == SocialNetworkOD::ERROR_TOTAL_LIMIT_REACHED) {
        exit();
    }
}

// В остальных оповещениях параметр уже не понадобиться.
unset($filter['first_access_range']);

// Если пользователь не заходил в игру больше 1 дня,
// начиная с 1 по 5 день пропуска, ему будет отправлено одно из 5 сообщений.
$n = count($textMessages);

for ($i = 1; $i < $n; $i++) {
    sleep($sendApiTimeout);

    $day                         = date("Y.m.d", $currentTime - 86400 * ($i + 1));
    $filter['text']              = $textMessages[$i];
    $filter['last_access_range'] = $day . '-' . $day;

    if ($api->sendMassNotifications($filter) === false) {
        echo 'error: ' . $api->getErrorCode() . '|' . $api->getErrorText();
        if ($api->getErrorCode() == SocialNetworkOD::ERROR_TOTAL_LIMIT_REACHED) {
            exit();
        }
    }
}
