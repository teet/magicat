<?php

/**
 * @file
 * Содержит вспомогательные функции для демона рассылки оповещений пользователям Вконтакте.
 *
 * Очищает множество неактивных пользователей(кому не удалось отправить поповещение),
 * и множество тех, кому удалось отправить оповещение.
 *
 * Демон рассылки пополняет эти множества, а этот демон очищает их.
 *
 * Сортированные множества хранят пары(score => member):
 * время добавления в мн-во => идентификатор пользователя в соц. сети.
 *
 * cron: 0 * * * * cd ~/html/src/App/GameBundle/Core/Cron/; /usr/bin/php -f notifyHelper.php
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

namespace App\GameBundle\Core\Cron;

use Redis;
use App\GameBundle\Core\Config\Config;

require_once('../Config/Config.php');

$inactiveUnlockTime = 86400 * 7; // 1 неделя для удаления из мн-ва тех, кому не удалось отправить оповещение.
$notifiedUnlockTime = 86400;     // 1 сутки

// Инициализация соединения с сервером редис.
$redisServer = Config::get('redisStatistic');
$redis       = new Redis();

if ($redis->connect($redisServer['host'], $redisServer['port']) === false) {
    printf("notifyHelper:: Redis connection error[%s].\n",
           $redisServer['host'] . ':' . $redisServer['port'] . '/' . $redisServer['db']);
    exit();
}

if ($redisServer['pass'] != '' && $redis->auth($redisServer['pass']) === false) {
    printf("notifyHelper:: Redis auth error[%s].\n",
           $redisServer['host'] . ':' . $redisServer['port'] . '/' . $redisServer['db']);
    exit();
}

if ($redis->select($redisServer['db']) === false) {
    printf("notifyHelper:: Redis select db error[%s].\n",
           $redisServer['host'] . ':' . $redisServer['port'] . '/' . $redisServer['db']);
    exit();
}

// Очистка множеств.
$redis->zRemRangeByScore('notifyInactiveUsers', 0, time() - $inactiveUnlockTime);
$redis->zRemRangeByScore('notifiedUsers', 0, time() - $notifiedUnlockTime);
