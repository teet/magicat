<?php

namespace App\GameBundle\Core\Exception;

use Exception;

class GameException extends Exception
{
    const INTERNAL_ERROR         = 100; // Общая ошибка фреймворка.
    const GAME_ERROR             = 101; // Ошибка игры.
    const GAME_NOT_AVAIBLE       = 103; // Игра недоступна.

    const SOCIAL_AUTH_FAIL       = 110; // Ошибка аутентификации в соцсети.
    const INCORRECT_SIGNATURE    = 111; // Некорректная сигнатура.
    const SESSION_FAIL           = 112; // Мультисессия.
    const RESPONSE_COUNTER_ERROR = 113; // Ошибка счетчика запросов.

    const USER_NOT_FOUND         = 120; // Пользователь не найден.

    const MISSING_PARAM          = 130; // Параметр отсутствует.
    const WRONG_PARAM            = 131; // Неправильный параметр.

    const CACHE_ERROR            = 140; // Ошибка с кэшем.
    const CACHE_CONNECTION_ERROR = 141; // Ошибка подключения к кэшу.

    const WRONG_ACTION           = 150; // Недопустимое действие.
    const NO_COINS               = 160; // Недостаточно монет.

    const SOCIAL_API_ERROR       = 170; // Ошибка запроса к Api социальной сети.

    /**
     * Возвращает строку параметров для логирования.
     *
     * @param array $request    массив входных параметров
     * @return string
     */
    public static function getRequestLog($request)
    {
        $log = '';
        if (isset($request['userId'])) {
            $log .= 'userId: ' . $request['userId'];
            unset($request['userId']);
        }

        foreach ($request as $param => $value) {
            $log .= ' | ' . $param . ': ' . (is_array($value) ? json_encode($value) : $value);
        }

        return $log;
    }
}
