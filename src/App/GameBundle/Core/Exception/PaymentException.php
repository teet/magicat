<?php

namespace App\GameBundle\Core\Exception;

use App\GameBundle\Core\Exception\GameException;

class PaymentException extends GameException
{
    const PAYMENT_ERROR       = 10; // Общая ошибка.
    const ADD_RESOURCES_ERROR = 11; // Ошибка начисления ресурсов пользователю.
}
