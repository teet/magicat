<?php
namespace App\GameBundle\Core\Progress\Location;

use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Partner\Partner;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Item\StageCatalog;

class ProgressLocation
{
    /** Максимальное количество возможных звезд за уровень. */
    const MAX_STARS = 3;

    /** Максимальное количество предметов в коллекции на одном острове. */
    const MAX_COLLECTION_IN_ISLAND = 6;

    /**
     * Хранит идентификатор пользователя.
     *
     * @var int
     */
    private $_playerObjects;

    private $_missionsInfo;
    private $_collectibleList;

    /**
     * __construct
     */
    public function __construct(PlayerObjects &$playerObjects)
    {
        $this->_playerObjects   = $playerObjects;
        $this->_missionsInfo    = $playerObjects->getMissionsInfo();
        $this->_collectibleList = $playerObjects->getCollectibleList();
    }

    /**
     * Возвращает сейв.
     *
     * @return array
     */
    public function getMissionsInfo()
    {
        return $this->_missionsInfo;
    }

    /**
     * Сохранение пройденной миссии.
     *
     * @param array $mission    пройденная миссия
     * @return void
     * @throws GameException
     */
    public function updateMissionsInfo($mission)
    {
        if (!isset($mission['m'])
            || !isset($mission['s'])
            || !isset($mission['p'])
        ) {
            throw new GameException('There are no mission object parameters.', GameException::WRONG_PARAM);
        }

        $tmp = explode('_', $mission['m']);
        $completeIslandId = $tmp[0]; // текущий пройденный остров
        $completeLevelId  = $tmp[1]; // текущий пройденный уровень

        if ($completeIslandId > StageCatalog::getIslandsCount($this->_playerObjects->getUserId())) {
            throw new GameException('Max island error.', GameException::WRONG_ACTION);
        }

        if ($completeLevelId > StageCatalog::getMissionsCount($completeIslandId, $this->_playerObjects->getUserId())) {
            throw new GameException('Max level error.', GameException::WRONG_ACTION);
        }

        $mission['s'] = min($mission['s'], self::MAX_STARS);
        $mission['p'] = (bool)$mission['p'];

        // если сейв пуст
        if (empty($this->_missionsInfo)) {
            if ($completeIslandId != 1 || $completeLevelId != 1) {
                throw new GameException('Empty mission save error.', GameException::WRONG_ACTION);
            }

            $this->_missionsInfo[] = $mission;
        // если повторно прошли миссию
        } elseif (($key = $this->searchMission($mission['m'])) !== null) {
            $this->_missionsInfo[$key]['s'] = max(
                $mission['s'],
                min($this->_missionsInfo[$key]['s'], self::MAX_STARS)
            );
            $this->_missionsInfo[$key]['p'] = $mission['p'];
        // если прошли новый уровень
        } else {
            // последняя пройденная миссия в сейве
            $key = count($this->_missionsInfo) - 1;
            $lastMissionId = explode('_', $this->_missionsInfo[$key]['m']);

            $lastIslandId = $lastMissionId[0]; // последний пройденный остров в сейве
            $lastLevelId  = $lastMissionId[1]; // последний пройденный уровень в сейве

            // если прошли уровень на текущем острове
            if ($completeIslandId == $lastIslandId
                && $completeLevelId == ($lastLevelId + 1)
            ) {
                $this->_missionsInfo[] = $mission;

                // УСЛОВИЕ ВЫПОЛНЕНИЯ ПАРТНЕРСКОЙ ПРОГРАММЫ:
                // нужно открыть 11-й остров. HACK: пока только для VK
                if (Config::get('socialNetwork') == 'VK'
                    && $completeIslandId == 10
                    && $completeLevelId == StageCatalog::getMissionsCount($lastIslandId, $this->_playerObjects->getUserId())
                ) {
                    $partner = new Partner($this->_playerObjects->getUserId());
                    $partner->giveBonus(Partner::LEAD_ID_1);
                }
            // если прошли уровень на новом острове
            } elseif ($completeIslandId == ($lastIslandId + 1)
                && $completeLevelId == 1
                && $lastLevelId == StageCatalog::getMissionsCount($lastIslandId, $this->_playerObjects->getUserId())
                && $this->checkIslandUnlocked($lastIslandId)
            ) {
                $this->_missionsInfo[] = $mission;

            } else {
                throw new GameException('Mission save error.', GameException::WRONG_ACTION);
            }
        }

        return $this->_missionsInfo;
    }

    /**
     * Обновление коллекции панд.
     *
     * Возвращает обновленную коллекцию.
     *
     * @param array $collection коллекция с полученными пандами
     * @return array            обновленная коллекция
     * @throws GameException
     */
    public function updateCollectibleList($collection)
    {
        if (!isset($collection['i'])
            || !isset($collection['p'])
            || !isset($collection['e'])
        ) {
            throw new GameException('There are no collection object parameters', GameException::WRONG_PARAM);
        }

        $packId = $collection['i'];

        if ($packId > StageCatalog::getIslandsCount($this->_playerObjects->getUserId())) {
            throw new GameException('Max packId error.', GameException::WRONG_PARAM);
        }

        if ($packId < 1) {
            throw new GameException('PackId signed error.', GameException::WRONG_PARAM);
        }

        if ($collection['p'] < 0) {
            throw new GameException('ProbabilityIndex signed error.', GameException::WRONG_PARAM);
        }

        if (count($collection['e']) != self::MAX_COLLECTION_IN_ISLAND) {
            throw new GameException('Collection list count error: ' . count($collection['e']),
                                    GameException::WRONG_PARAM);
        }

        foreach ($collection['e'] as $item) {
            if (isset($item['m']) && isset($item['c'])) {
                $item['c'] = (bool)$item['c'];
                if ($item['c'] == true) {
                    $tmp = explode('_', $item['m']);
                    if (count($tmp) != 2) {
                        throw new GameException('gainMissionId error.', GameException::WRONG_PARAM);
                    }

                    if ($tmp[0] != $collection['i'] ||
                        $tmp[1] > StageCatalog::getMissionsCount($collection['i'], $this->_playerObjects->getUserId())
                    ) {
                        throw new GameException('gainMissionId level error.', GameException::WRONG_PARAM);
                    }
                }
            }
        }

        // если коллекция пуста
        if (empty($this->_collectibleList)) {
            if ($packId != 1) {
                throw new GameException('Collection empty and pack more 1.', GameException::WRONG_ACTION);
            }

            $this->_collectibleList[] = $collection;

        // если получена панда в существующей коллекции
        } elseif (isset($this->_collectibleList[$packId - 1])
            && $this->_collectibleList[$packId - 1]['i'] == $packId
        ) {
            $this->_collectibleList[$packId - 1]['p'] = $collection['p'];
            foreach ($this->_collectibleList[$packId - 1]['e'] as $itemId => $item) {
                if ($item['c'] == false && $collection['e'][$itemId]['m'] == true) {
                    $this->_collectibleList[$packId - 1]['e'][$itemId]['m'] = $collection['e'][$itemId]['m'];
                    $this->_collectibleList[$packId - 1]['e'][$itemId]['c'] = true;
                }
            }

        // если получена панда в новой коллекции
        } else  {
            $key = count($this->_collectibleList) - 1;
            $lastPackId = $this->_collectibleList[$key]['i'];

            if ($packId == ($lastPackId + 1)) {
                $this->_collectibleList[] = $collection;
            } else {
                throw new GameException('Collection save error.', GameException::WRONG_ACTION);
            }
        }

        return $this->_collectibleList;
    }

    /**
     *
     * @param type $missionId
     * @return null
     */
    public function searchMission($missionId)
    {
        foreach ($this->_missionsInfo as $key => $mission) {
            if ($mission['m'] == $missionId) {
                return $key;
            }
        }

        return null;
    }

    /**
     * Проверяет условие открытия острова.
     *
     * Проверка суммы заработанных звезд на предыдущем острове, необходимой
     * для открытия нового острова.
     *
     * @param int $islandId идентификатор открытого острова
     * @param array $this->_missionsInfo   сейв
     * @return boolean
     */
    public function checkIslandUnlocked($islandId)
    {
        $sumStars = $this->getSumStarsAchievedIsland($islandId);
        if ($sumStars < StageCatalog::getStarsToUnlockIsland($islandId, $this->_playerObjects->getUserId())) {
            return false;
        }

        return true;
    }

    /**
     * Вычисляет сумму полученных звезд на острове.
     *
     * @param int $islandId         идентификатор острова
     * @return int
     */
    public function getSumStarsAchievedIsland($islandId)
    {
        $sumStars = 0;
        foreach ($this->_missionsInfo as $mission) {
            if (preg_match('/' . $islandId . '_[0-9]*/', $mission['m'])) {
                $sumStars += $mission['s'];
            }
        }

        return $sumStars;
    }

    /**
     * Возвращает номер последнего пройденного уровня.
     *
     * @return string Возвращает номер острова и уровня в формате "1_3".
     *                Если нет пройденных уровней, то возвращается "1_0"
     */
    public function getLastLevelId()
    {
        $mission = end($this->_missionsInfo);

        if ($mission === false || count($this->_missionsInfo) <= 0) {
            return '1_0';
        }

        return $mission['m'];
    }

    /**
     * Возвращает номер текущего острова и уровня.
     *
     * Учитывается, что пользователь мог пройти все уровни острова,
     * но не набрать достаточное ко-во звезд.
     *
     * @return string Возвращает номер острова и уровня в формате "1_3".
     *                Если нет пройденных уровня, то возвращается "1_1".
     *                Если пройдены все уровни, то номер последнего острова и уровня.
     */
    public function getCurrentLevelId()
    {
        $mission = end($this->_missionsInfo);

        if ($mission === false || count($this->_missionsInfo) <= 0) {
            return '1_1';
        }

        $tmp      = explode('_', $mission['m']);
        $islandId = intval($tmp[0]);
        $levelId  = intval($tmp[1]);

        // Если пользователь не прошел последний уровень острова, то возвращаем следующий уровень на острове.
        if ($levelId < StageCatalog::getMissionsCount($islandId, $this->_playerObjects->getUserId())) {
            return $islandId . '_' . ($levelId + 1);
        }

        // прошел все уровни на островах, возвращаем номер последнего уровня и острова как есть.
        if ($islandId >= StageCatalog::getIslandsCount($this->_playerObjects->getUserId())) {
            return $islandId . '_' . $levelId;
        }

        // Определяем, прошел остров или нет, из-за недостатка звезд.
        $stars = 0;

        while (true) {
            $stars  += $mission['s'];
            $mission = prev($this->_missionsInfo);

            if ($mission === false) {
                break;
            } else {
                $tmp = explode('_', $mission['m']);
                if ($tmp[0] != $islandId) {
                    break;
                }
            }
        }

        if ($stars < StageCatalog::getStarsToUnlockIsland($islandId, $this->_playerObjects->getUserId())) {
            return $islandId . '_' . $levelId;
        }

        return ($islandId + 1) . '_1';
    }
}
