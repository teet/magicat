<?php

namespace App\GameBundle\Core\Item\ItemSplits;

/**
 * @file
 * This file build automatically.
 * @see ItemCatalog::buildSplit().
 */

class ItemCatalog0
{
    public static function getCatalog()
    {
        return self::$_catalog;
    }

    private static $_catalog = array(
        0  => array(
            'title'         => '2 000 золотых монет',
            'type'          => 'coinProduct',
            'PriceSocial'   => 6,
            'goldCount'     => 2000,
            'prevGoldCount' => 1800,
            'key'           => 'goldPack1',
            'itemId'        => 1,
        ),
        1  => array(
            'title'         => '3 000 золотых монет',
            'type'          => 'coinProduct',
            'PriceSocial'   => 9,
            'goldCount'     => 3000,
            'prevGoldCount' => 2600,
            'key'           => 'goldPack2',
            'itemId'        => 2,
        ),
        2  => array(
            'title'         => '7 500 золотых монет',
            'type'          => 'coinProduct',
            'PriceSocial'   => 19,
            'goldCount'     => 7500,
            'prevGoldCount' => 6000,
            'key'           => 'goldPack3',
            'itemId'        => 3,
        ),
        3  => array(
            'title'         => '45 000 золотых монет',
            'type'          => 'coinProduct',
            'PriceSocial'   => 99,
            'goldCount'     => 45000,
            'prevGoldCount' => 30000,
            'key'           => 'goldPack4',
            'itemId'        => 4,
        ),
        4  => array(
            'title'         => '1 000 золотых монет',
            'type'          => 'coinProduct',
            'PriceSocial'   => 3,
            'goldCount'     => 1000,
            'prevGoldCount' => 1000,
            'key'           => 'goldPack5',
            'itemId'        => 5,
        ),
        5  => array(
            'title'         => '20 000 золотых монет',
            'type'          => 'coinProduct',
            'PriceSocial'   => 49,
            'goldCount'     => 20000,
            'prevGoldCount' => 15000,
            'key'           => 'goldPack6',
            'itemId'        => 6,
        ),
        6  => array(
            'title'       => 'Розыгрыш лотереи',
            'type'        => 'spin',
            'PriceSocial' => 2,
            'key'         => 'spin',
            'itemId'      => 7,
        ),
        7  => array(
            'title'       => '1 500 золотых монет',
            'type'        => 'spinCoinProduct',
            'PriceSocial' => 4,
            'goldCount'   => 1500,
            'key'         => 'spinPopupGoldPack',
            'itemId'      => 8,
        ),
        8  => array(
            'title'         => '1 500 золотых монет',
            'type'          => 'offerProduct',
            'PriceSocial'   => 1,
            'goldCount'     => 1500,
            'prevGoldCount' => 300,
            'unlockMission' => 1,
            'step'          => 1,
            'key'           => 'd1',
            'itemId'        => 9,
        ),
        9  => array(
            'title'         => '3 000 золотых монет',
            'type'          => 'offerProduct',
            'PriceSocial'   => 5,
            'goldCount'     => 3000,
            'prevGoldCount' => 1500,
            'unlockMission' => 1,
            'step'          => 2,
            'key'           => 'd2',
            'itemId'        => 10,
        ),
        10 => array(
            'title'         => '10 000 золотых монет',
            'type'          => 'offerProduct',
            'PriceSocial'   => 20,
            'goldCount'     => 10000,
            'prevGoldCount' => 6000,
            'unlockMission' => 1,
            'step'          => 3,
            'key'           => 'd3',
            'itemId'        => 11,
        ),
        11 => array(
            'title'           => 'Цветная бомба',
            'type'            => 'boost',
            'PriceCoins'      => 200,
            'unlockMissionId' => '1_5',
            'probability'     => 0.015,
            'isOnBoard'       => '1',
            'key'             => 'colorBomb',
            'itemId'          => 12,
        ),
        12 => array(
            'title'           => 'Фейерверк',
            'type'            => 'boost',
            'PriceCoins'      => 150,
            'unlockMissionId' => '2_1',
            'probability'     => 0.02,
            'isOnBoard'       => '1',
            'itemId'          => 13,
            'key'             => 'chineseFirework',
        ),
        13 => array(
            'title'           => 'Бомба',
            'type'            => 'boost',
            'PriceCoins'      => 300,
            'unlockMissionId' => '2_3',
            'probability'     => 0.015,
            'isOnBoard'       => '1',
            'key'             => 'radialBomb',
            'itemId'          => 14,
        ),
        14 => array(
            'title'           => 'Ракета',
            'type'            => 'boost',
            'PriceCoins'      => 150,
            'unlockMissionId' => '2_4',
            'probability'     => 0.02,
            'isOnBoard'       => '1',
            'key'             => 'bomb',
            'itemId'          => 15,
        ),
        15 => array(
            'title'      => '+10 доп. ходов',
            'type'       => 'lastExtraMove',
            'PriceCoins' => 900,
            'movesToAdd' => 10,
            'key'        => 'lastExtraMoveByCoins',
            'itemId'     => 16,
        ),
        16 => array(
            'title'      => 'Восстановление сердечек',
            'type'       => 'lifeRefill',
            'PriceCoins' => 900,
            'key'        => 'lifeRefill',
            'itemId'     => 17,
        ),
        17 => array(
            'title'      => 'Ключ к острову',
            'type'       => 'payGates',
            'PriceCoins' => 3000,
            'key'        => 'payGates',
            'itemId'     => 18,
        ),
        18 => array(
            'title'      => '1 500 золотых монет',
            'type'       => 'questProduct',
            'PriceCoins' => 0,
            'goldCount'  => 1500,
            'key'        => 'sq',
            'itemId'     => 19,
        ),
        19 => array(
            'title'  => 'Супер Бомба',
            'type'   => 'ingameBoostProduct',
            'items'  => array(
                0 => array(
                    0 => 2,
                    1 => 900,
                    2 => 1000,
                ),
                1 => array(
                    0 => 3,
                    1 => 1200,
                    2 => 1500,
                ),
                2 => array(
                    0 => 5,
                    1 => 1800,
                    2 => 2500,
                ),
            ),
            'key'    => 'ingameRadialBomb',
            'itemId' => 20,
        ),
        20 => array(
            'title'  => 'Супер Фейерверк',
            'type'   => 'ingameBoostProduct',
            'items'  => array(
                0 => array(
                    0 => 2,
                    1 => 1000,
                    2 => 1200,
                ),
                1 => array(
                    0 => 3,
                    1 => 1500,
                    2 => 1800,
                ),
                2 => array(
                    0 => 5,
                    1 => 2100,
                    2 => 3000,
                ),
            ),
            'key'    => 'ingameChineseFirework',
            'itemId' => 21,
        ),
        21 => array(
            'title'  => '+10 доп. ходов',
            'type'   => 'ingameBoostProduct',
            'items'  => array(
                0 => array(
                    0 => 1,
                    1 => 900,
                    2 => 0,
                ),
                1 => array(
                    0 => 3,
                    1 => 1800,
                    2 => 2570,
                ),
            ),
            'key'    => 'ingameExtraMoves',
            'itemId' => 22,
        ),
        22 => array(
            'title'       => '+10 доп. ходов',
            'type'        => 'extraMoves',
            'PriceSocial' => 3,
            'movesToAdd'  => 10,
            'key'         => 'extraMovesBySocial',
            'itemId'      => 23,
        ),
        23 => array(
            'title'       => 'Кол-во монет при регистрации',
            'type'        => 'startCoins',
            'PriceSocial' => 0,
            'goldCount'   => 2500,
            'key'         => 'startCoins',
            'itemId'      => 24,
        ),
    );
}
