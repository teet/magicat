<?php

namespace App\GameBundle\Core\Item\StageSplits;

/**
 * @file
 * This file build automatically.
 * @see StageCatalog::buildSplit(). 
 */

class StageCatalog0
{
    public static function getCatalog()
    {
        return self::$_catalog;
    }

    private static $_catalog = array(
        'i1'  => array(
            'starsToUnlock' => 12,
            'missionsCount' => 6,
        ),
        'i2'  => array(
            'starsToUnlock' => 14,
            'missionsCount' => 7,
        ),
        'i3'  => array(
            'starsToUnlock' => 16,
            'missionsCount' => 8,
        ),
        'i4'  => array(
            'starsToUnlock' => 18,
            'missionsCount' => 9,
        ),
        'i5'  => array(
            'starsToUnlock' => 18,
            'missionsCount' => 9,
        ),
        'i6'  => array(
            'starsToUnlock' => 20,
            'missionsCount' => 10,
        ),
        'i7'  => array(
            'starsToUnlock' => 22,
            'missionsCount' => 11,
        ),
        'i8'  => array(
            'starsToUnlock' => 22,
            'missionsCount' => 11,
        ),
        'i9'  => array(
            'starsToUnlock' => 22,
            'missionsCount' => 11,
        ),
        'i10' => array(
            'starsToUnlock' => 22,
            'missionsCount' => 12,
        ),
        'i11' => array(
            'starsToUnlock' => 26,
            'missionsCount' => 13,
        ),
        'i12' => array(
            'starsToUnlock' => 20,
            'missionsCount' => 10,
        ),
        'i13' => array(
            'starsToUnlock' => 24,
            'missionsCount' => 12,
        ),
    );
}
