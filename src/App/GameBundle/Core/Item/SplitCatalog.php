<?php

namespace App\GameBundle\Core\Item;

use App\GameBundle\Core\Tools\ArrayToText;

/**
 * Класс для работы со списком сплитов.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class SplitCatalog
{
    /** Состояния сплитов. */
    const STATUS_NONE    = 0;
    const STATUS_STARTED = 1;
    const STATUS_STOPPED = 2;
    const STATUS_REMOVED = 3;

    /**
     * Генерирует содержимое каталога из json файла.
     *
     * @param $path путь к json-файлу с параметрами сплитов.
     * @return void
     */
    public static function build($path)
    {
        $data = file_get_contents($path);
        $data = json_decode($data, true);

        // формирование списка запущенных сплитов. Просто созданные не трогаем, т.к. они ни на что не повлияют.
        $items = array();

        foreach ($data['items'] as $group) {
            $status = self::STATUS_NONE;

            if ($group['removeTime'] > 0) {
                $status = self::STATUS_REMOVED;
            } elseif ($group['stopTime'] > 0) {
                $status = self::STATUS_STOPPED;
            } elseif ($group['startTime'] > 0) {
                $status = self::STATUS_STARTED;
            }

            if ($status != self::STATUS_NONE) {
                foreach ($group['splits'] as $splitInfo) {
                    $items['i' . $splitInfo['id']] = $status;
                }
            }
        }

        $content = preg_replace(
            '/private static \$_items = array\(.*?\);/s',
            sprintf("private static \$_items = array(\n%s    );", ArrayToText::parse($items)),
            file_get_contents(__FILE__)
        );

        file_put_contents(__FILE__, $content);
    }

    /**
     * Возвращает статус сплита.
     *
     * @param int $splitId Номер сплита
     * @return int
     * @see self::STATUS_*
     */
    public static function getStatus($splitId)
    {
        if (!isset(self::$_items['i' . $splitId])) {
            return null;
        }

        return self::$_items['i' . $splitId];
    }

    /**
     * Проверяет сплит на существование.
     *
     * Необходим для проверки существования сплита, когда нужно выдать его номер клиенту при авторизации,
     * либо получить данные настроек, в зависимости от него.
     *
     * @param int  $splitId       Номер сплита
     * @param bool $findInRemoved Флаг, при котором сплит ищется в списке удаленных
     *
     * @return bool
     */
    public static function isExists($splitId, $findInRemoved = true)
    {
        $status = self::getStatus($splitId);

        if (is_null($status) ||
            (($status == self::STATUS_REMOVED) && !$findInRemoved)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Список сплитов с ихними статусами.
     *
     * @see self::STATUS_*
     *
     * @var array
     */
    private static $_items = array(
        'i1' => 1,
        'i2' => 1,
        'i3' => 1
    );
}
