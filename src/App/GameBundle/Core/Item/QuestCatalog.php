<?php

namespace App\GameBundle\Core\Item;

use App\GameBundle\Core\Tools\ArrayToText;

/**
 * Класс для работы с игровыми квестами.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class QuestCatalog
{
    /**
     * Генерирует содержимое каталога из json файла.
     *
     * @param $path путь к json-файлу с параметрами квестов.
     * @return void
     */
    public static function build($path)
    {
        $data = file_get_contents($path);
        $data = json_decode($data, true);

        $content = preg_replace(
            '/private static \$_items = array\(.*?\);/s',
            sprintf("private static \$_items = array(\n%s    );", ArrayToText::parse($data)),
            file_get_contents(__FILE__)
        );

        file_put_contents(__FILE__, $content);
    }

    /**
     * Возвращает информацию о квесте по идентификатору.
     *
     * @param string $questId   идентификатор квеста
     * @return mixed Возвращает информацию о квесте или null, если квест не найден
     */
    public static function getQuest($questId)
    {
        if (isset(self::$_items['active'][$questId])) {
            return self::$_items['active'][$questId];
        } elseif (isset(self::$_items['inactive'][$questId])) {
            return self::$_items['inactive'][$questId];
        }

        return null;
    }

    /**
     * Возвращает список активных квестов, с учетом текущего активного квеста пользователя.
     *
     * @param string $userQuestId   идентификатор текущего квеста пользователя
     * @return array Возвращает информацию о квестах в виде ассоциативного массива
     */
    public static function getCurrentQuests($userQuestId)
    {
        $quests = self::$_items['active'];

        if (($userQuestId != '')
            && !isset(self::$_items['active'][$userQuestId])
            && isset(self::$_items['inactive'][$userQuestId])
        ) {
            $quests[$userQuestId] = self::$_items['inactive'][$userQuestId];
        }

        return $quests;
    }

    /**
     * Возвращает весь каталог с параметрами квестов.
     *
     * @return assoc
     */
    public static function getCatalog()
    {
        return self::$_items;
    }

    /**
     * Список параметров квестов.
     *
     * @var array
     */
    private static $_items = array(
        'lastId'   => 0,
        'active'   => array(),
        'inactive' => array(),
    );
}
