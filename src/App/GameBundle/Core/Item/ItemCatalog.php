<?php

namespace App\GameBundle\Core\Item;

use App\GameBundle\Core\Tools\Json;
use App\GameBundle\Core\Tools\ArrayToText;
use App\GameBundle\Core\SplitManager\SplitManager;

/**
 * Класс для работы с каталог товаров.
 *
 * Возвращает информацию о товарах, с учетом номера сплита пользователей.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class ItemCatalog
{
    /** Пространство имен для расположения версий каталога, в зависимости от сплита. */
    const SPLIT_NAMESPACE  = 'App\GameBundle\Core\Item\ItemSplits';

    /** Расположение классов для работы с версиями каталога, относительно web/ или app/. */
    const SPLITS_PATH = '../src/App/GameBundle/Core/Item/ItemSplits/';

    /** Префикс имени класса для сплитов. */
    const SPLIT_CLASS_NAME = 'ItemCatalog';

    /** Кэш для хранения экземпляров классов работы с каталогом для каждого пользователя. */
    private static $_splits = array();

    /**
     * Генерирует классы для доступа к данным каталога для всех существующих сплитов.
     *
     * @param string $path путь к json-файлам с параметрами каталогов для каждого сплита.
     * @return void
     * @throws \Exception
     */
    public static function build($path)
    {
        $dir = opendir($path);

        while (false !== ($fileName = readdir($dir))) {
            if (is_dir($fileName)) {
                continue;
            }

            $t = explode('.', $fileName);

            if (count($t) < 2 || !is_numeric($t[0]) || $t[1] != 'json') {
                continue;
            }

            $splitId = $t[0];
            self::buildSplit($splitId, $path . $fileName);
        }

        closedir($dir);
    }

    /**
     * Генерирует класс для доступа к данным каталога товаров для сплита.
     *
     * @param string $splitId Айди спдита
     * @param string $path    Расположение json-файла с данными
     *
     * @return void
     * @throws \Exception
     */
    public static function buildSplit($splitId, $path)
    {
        $template = "<?php\n\n"
                  . "namespace " . self::SPLIT_NAMESPACE . ";\n\n"
                  . "/**\n * @file\n * This file build automatically.\n"
                  . " * @see ItemCatalog::buildSplit(). \n */\n\n"
                  . "class " . self::SPLIT_CLASS_NAME . "%s\n{\n"
                  . "    public static function getCatalog()\n    {\n"
                  . "        return self::\$_catalog;\n"
                  . "    }\n\n"
                  . "    private static \$_catalog = array(\n%s    );\n}\n";

        $data = file_get_contents($path);
        $data = Json::decode($data, true);
        $data = ArrayToText::parse($data);

        $splitFileName = self::getSplitFileName($splitId);
        $f = fopen($splitFileName, "w");

        if ($f === null) {
            throw new \Exception('Error create file: ' . $splitFileName);
        }

        fwrite($f, sprintf($template, $splitId, $data));
        fclose($f);
    }

    /**
     * Возвращает полный путь к расположению класса для работы с каталогом.
     *
     * @param int $splitId Номер сплита
     * @return string
     */
    public static function getSplitFileName($splitId)
    {
        return self::SPLITS_PATH . self::SPLIT_CLASS_NAME . $splitId . '.php';
    }

    /**
     * Возвращает ссылку на класс каталога, в зависимости от номера сплита пользователя.
     *
     * Для каждого пользователя, храниться своя ссылка на экземпляр класса каталога,
     * поэтому можно смотреть для разных пользователей.
     * При повторном вызове метода для одного и тогоже пользователя, данные будут браться из кэша.
     *
     * @param string $userId идентификатор пользователя или null, если нужно искать в каталоге по умолчанию
     * @return mixed
     */
    private static function _getSplit($userId)
    {
        if (!isset(self::$_splits['i' . $userId])) {
            $splitId = (is_null($userId) ? SplitManager::DEFAULT_SPLIT_ID : SplitManager::getUserSplitId($userId));
            $catalog = self::SPLIT_NAMESPACE . '\\' . self::SPLIT_CLASS_NAME . $splitId;
            self::$_splits['i' . $userId] = new $catalog();
        }

        return self::$_splits['i' . $userId];
    }

    /**
     * Возвращает массив с данными каталога.
     *
     * @param string $userId идентификатор пользователя или null, если нужно искать в каталоге по умолчанию
     * @return array
     */
    public static function getCatalog($userId)
    {
        $split = self::_getSplit($userId);
        return $split::getCatalog();
    }

    /**
     * Возвращает описание предмета по ключу.
     *
     * @param string $key    id-ключ предмета
     * @param string $userId идентификатор пользователя или null, если нужно искать в каталоге по умолчанию
     * @return null
     */
    public static function getItem($key, $userId)
    {
        $catalog = self::getCatalog($userId);
        foreach ($catalog as $item) {
            if ($item['key'] == $key) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Возвращает описание предмета по идентификатору.
     *
     * @param integer $itemId идентификатор предмета
     * @param string  $userId идентификатор пользователя или null, если нужно искать в каталоге по умолчанию
     * @return null
     */
    public static function getItemByItemId($itemId, $userId)
    {
        $catalog = self::getCatalog($userId);
        foreach ($catalog as $item) {
            if ($item['itemId'] == $itemId) {
                return $item;
            }
        }

        return null;
    }
}
