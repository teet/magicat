<?php

namespace App\GameBundle\Core\Response;

use Symfony\Component\HttpFoundation\Response;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Tools\Json;

class GameResponse extends Response
{
    /**
     * Constructor.
     *
     * @param mixed   $data    The response data
     * @param integer $status  The response status code
     * @param array   $headers An array of response headers
     */
    public function __construct($data = null, $status = 200, $headers = array())
    {
        $dataJson = Json::encode($data);
        $strChars = preg_split('//u', $dataJson, -1, PREG_SPLIT_NO_EMPTY);
        sort($strChars);
        $md5 = md5(implode('', $strChars) . Config::get('signatureKey'));

        $response = '{"response":' . $dataJson .',"md5":"' . $md5 . '"}';

        parent::__construct($response, $status, $headers);
    }
}

