<?php

namespace App\GameBundle\Core\Partner;

use App\GameBundle\Core\Singleton\RedisStore;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Log\Log;
use App\GameBundle\Core\Authorize\AuthorizeSignature;

class Partner
{
    const URL = 'https://api.vk.com/method/leads.complete';

    const LEAD_ID_1 = 7776;

    /**
     * Хранит id пользователя.
     *
     * @var int
     */
    private $_userId;

    /**
     * Хранит ссылку на объект RedisStore.
     *
     * @var RedisStore
     */
    private $_cache;

    /**
     * Конструктор.
     *
     * @param int $userId   id пользователя
     */
    public function __construct($userId)
    {
        $this->_userId = $userId;
        $this->_cache  = RedisStore::getInstance();
    }

    /**
     * Сохраняет параметры перехода партнерской ссылке.
     *
     * @see http://vk.com/dev/leads
     *
     * @param Request $request  Request объект
     * @return mixed
     */
    public function saveOffer(Request &$request)
    {
        $vk_sid     = $request->query->get('vk_sid');
        $vk_lead_id = $request->query->get('vk_lead_id');
        //$vk_uid     = $request->query->get('vk_uid');
        $vk_hash    = $request->query->get('vk_hash');

        if (null !== $vk_sid && null !== $vk_lead_id && /*null !== $vk_uid &&*/ null !== $vk_hash) {
            $key = self::getKey($vk_lead_id, $this->_userId);

            if ( ! $this->_cache->exists($key)) {

                $strToLog = $this->_userId . ';' . $vk_sid . ';' . $vk_lead_id . ';' . /*$vk_uid . ';' .*/ $vk_hash;

                $partnerSecret = Config::get('partnerSecret');
                if ( ! isset($partnerSecret[$vk_lead_id])) {
                    $log = new Log('Partner_Error_Start_Lead_Id');
                    $log->write($strToLog);
                    return null;
                }

                /*$hashParams = array($vk_sid, $vk_lead_id, $vk_uid, $partnerSecret[$vk_lead_id]);
                if ($vk_hash != md5(implode('_', $hashParams))) {
                    $log = new Log('Partner_Error_Start_Hash');
                    $log->write($strToLog . ';' . $partnerSecret[$vk_lead_id]);
                    return null;
                }*/

                $this->_cache->hSaveAll($key, array(
                    's'  => $vk_sid,
                    'l'  => $vk_lead_id,
                    'h'  => $vk_hash,
                    'st' => 0,
                    't'  => time()
                ));

                $log = new Log('Partner_Success_Start');
                $log->write($strToLog);

                return array(
                    'vk_sid'     => $vk_sid,
                    'vk_lead_id' => $vk_lead_id,
                    'vk_hash'    => $vk_hash
                );
            }
        }

        return null;
    }

    /**
     * Выдает бонус за переход по партнерской ссылке.
     *
     * Если есть сохраненная ссылка и бонус не выдавался, то выдает бонус.
     * @see http://vk.com/dev/leads.complete
     *
     * @param int $leadId   id рекламной акции
     * @return void
     */
    public function giveBonus($leadId)
    {
        $key = self::getKey($leadId, $this->_userId);

        if ($this->_cache->exists($key) &&
            $this->_cache->hFetch($key, 'st') == 0
        ) {
            $sid = $this->_cache->hFetch($key, 's');

            $strToLog = $this->_userId . ';' . $sid . ';' .
                $this->_cache->hFetch($key, 'l') . ';' .
                $this->_cache->hFetch($key, 'h') . ';' .
                $this->_cache->hFetch($key, 'st') . ';' .
                $this->_cache->hFetch($key, 't');

            $partnerSecret = Config::get('partnerSecret');
            if ( ! isset($partnerSecret[$leadId])) {
                $log = new Log('Partner_Error_Complete_Lead_Id');
                $log->write($strToLog . ';' . $leadId);

                return;
            }

            $url = self::URL . '?vk_sid=' . $sid . '&secret=' . $partnerSecret[$leadId];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);

            $content = curl_exec($curl);
            curl_close($curl);

            $result = json_decode($content, true);
            if (isset($result['response']['success']) &&
                $result['response']['success'] == 1
            ) {
                $this->_cache->hSave($key, 'st', 1);
                $log = new Log('Partner_Success_Complete');
            } else {
                $log = new Log('Partner_Error_Complete_Response');
                if (isset($result['error']['error_code'])) {
                    $log->write('error_code;' . $result['error']['error_code']);
                }
            }

            $log->write($strToLog);
        }
    }

    /**
     * Возвращает ключ кэша хранения оффера.
     *
     * @param int $leadId   id рекламной акции
     * @param int $userId   id пользователя
     * @return string
     */
    public static function getKey($leadId, $userId)
    {
        return 'pr:' . $leadId . ':' . $userId;
    }
}
