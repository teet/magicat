<?php

namespace App\GameBundle\Core\Tools;

/**
 * Содержит класс для кодирования и декодирования данных в формат json и обратно.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для кодирования и декодирования данных в формат json и обратно.
 *
 * Добавлен для корректной работы с кирилицей в кодировке utf-8.
 * Если версия php не будет поддерживать json encode и decode, то здесь можно их переопределить.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
abstract class Json
{
    /**
     * Кодирует значение в json-строку.
     *
     * Корректно работает с данными в кодировке utf-8.
     *
     * @param mixed $value Кодируемое значение
     * @return string
     */
    public static function encode($value)
    {
        return preg_replace("#(?<=[^\\\\])\\\\u([[:xdigit:]]{4})#eiu",
                            'html_entity_decode("&#".hexdec("$1").";", ENT_NOQUOTES, "UTF-8")',
                            json_encode($value));
    }

    /**
     * Декодирует json-строку.
     *
     * @param string $json  Декодируемая строка в формате JSON
     * @param bool   $assoc Если флаг установлен, то результат будет в виде ассоциативного массива
     *
     * @return mixed Результат преобразования строки в массив
     */
    public static function decode($json, $assoc = true)
    {
        return json_decode($json, $assoc);
    }
}
