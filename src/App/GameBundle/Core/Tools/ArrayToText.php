<?php

namespace App\GameBundle\Core\Tools;

/**
 * ArrayToText трансформирует входной массив в код PHP по стандартам
 * PEAR 1. Класс используется при кодогенерации в каталогах.
 *
 * @category  Tools
 * @package   Tools
 * @author    Nikolay Bondarenko <n.bondarenko@tvxgames.ru>
 * @copyright 2003-2010 TVXGames
 * @license   http://www.gnu.org/licenses/lgpl.txt LGPL License 3.0
 * @version   Release: @package_version@
 * @link      http://tvxgames.ru/
 */
class ArrayToText
{
    /**
     * parse разбирает массив и возвращает строку
     *
     * @param array   &$data Массив для обработки
     * @param integer $deep  Какое количество отступов использовать
     *
     * @return string Возвращает отформатированную строку
     *
     */
    final static public function parse(array &$data, $deep = 2)
    {
        $currentTabDeep = $deep;
        $tabShift       = str_repeat(' ', $currentTabDeep * 4);
        $maxKeyLenght   = 0;
        $text           = '';

        array_walk(
            $data,
            function($item, $key) use (&$maxKeyLenght)
            {
                if ($maxKeyLenght < strlen((string)$key)) {
                    $maxKeyLenght = strlen((string)$key);
                }
            }
        );

        foreach ($data as $key => $value) {
            $keyValueShift = str_repeat(' ', $maxKeyLenght - strlen((string)$key));

            // Если ключ не целочисленный, то обрабляем его одинарными кавычками.
            if (!is_integer($key)) {
                $key = "'$key'";
            }

            if (is_array($value)) {
                if (count($value) > 0) {
                    $text .= $tabShift . $key . "$keyValueShift => array(\n"
                          . self::parse($value, $deep + 1)
                          . $tabShift . "),\n";
                } else {
                    $text .= $tabShift . $key . "$keyValueShift => array(),\n";
                }
            } else {
                if (is_int($value)) {
                    $value = intval($value);
                } else if (is_float($value)) {
                    $value = floatval($value);
                } else {
                    $value = "'" . $value . "'";
                }
                $text .= $tabShift . $key . "$keyValueShift => $value,\n";
            }
        }

        return $text;
    }
}
