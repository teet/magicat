<?php

namespace App\GameBundle\Core\SocialNetworks\Authorize;

use App\GameBundle\Core\SocialNetworks\Authorize\AuthorizeSocial;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Config\Config;


/**
 * @file
 * Содержит класс для авторизации пользователя в социальной сети Вконтакте.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для авторизации пользователя в социальной сети Вконтакте.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class AuthorizeSocialVK extends AuthorizeSocial
{
    /**
     * Процесс авторизации.
     *
     * http://vk.com/dev.php?method=auth_key
     *
     * @param Request $request  объект с полученными от клиента данными
     * @throws GameException
     */
    public function process(Request &$request)
    {
        $userId  = null;
        $authKey = null;

        if ($request->getMethod() == 'POST') {
            $userId  = $request->request->get('userId');
            $authKey = $request->request->get('authKey');
        } elseif ($request->getMethod() == 'GET') {
            $userId  = $request->query->get('viewer_id');
            $authKey = $request->query->get('auth_key');
        }

        if ($userId === null || $authKey === null) {
            throw new GameException('Auth fail.', GameException::MISSING_PARAM);
        }

        if ($authKey != $this->getAuthHash($userId)) {
            throw new GameException('Auth fail.', GameException::SOCIAL_AUTH_FAIL);
        }
    }

    /**
     * Возвращает md5-подпись для проверки подлинности ключа авторизации от социальной сети.
     *
     * @param int $userId Идентификатор пользователя в социальной сети
     *
     * @return string Возвращает md5-подпись
     */
    public function getAuthHash($userId)
    {
        return md5(Config::get('appId') . '_' . $userId . '_' . Config::get('serverSecretKey'));
    }
}
