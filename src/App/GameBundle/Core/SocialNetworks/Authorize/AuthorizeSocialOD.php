<?php

namespace App\GameBundle\Core\SocialNetworks\Authorize;

use App\GameBundle\Core\SocialNetworks\Authorize\AuthorizeSocial;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Config\Config;


/**
 * @file
 * Содержит класс для авторизации пользователя в социальной сети Одноклассники.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */

/**
 * Класс для авторизации пользователя в социальной сети Одноклассники.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */
class AuthorizeSocialOD extends AuthorizeSocial
{
    private $_sessionKey = null;

    /**
     * Процесс авторизации.
     *
     * http://apiok.ru/wiki/pages/viewpage.action?pageId=42476523
     *
     * @param Request $request  объект с полученными от клиента данными
     * @throws GameException
     */
    public function process(Request &$request)
    {
        $userId  = null;
        $authKey = null;

        if ($request->getMethod() == 'POST') {
            $userId  = $request->request->get('userId');
            $authKey = $request->request->get('authKey');
            $this->_sessionKey = $request->request->get('sessionKey');
        } elseif ($request->getMethod() == 'GET') {
            $userId  = $request->query->get('logged_user_id');
            $authKey = $request->query->get('auth_sig');
            $this->_sessionKey = $request->query->get('session_key');
        }

        if ($userId === null || $authKey === null || $this->_sessionKey === null) {
            throw new GameException('Auth fail.', GameException::MISSING_PARAM);
        }

        if ($authKey != $this->getAuthHash($userId)) {
            throw new GameException('Auth fail.', GameException::SOCIAL_AUTH_FAIL);
        }
    }

    /**
     * Возвращает md5-подпись для проверки подлинности ключа авторизации от социальной сети.
     *
     * @param int $userId Идентификатор пользователя в социальной сети
     *
     * @return string Возвращает md5-подпись
     */
    public function getAuthHash($userId)
    {
        return md5($userId . $this->_sessionKey . Config::get('serverSecretKey'));
    }
}
