<?php

namespace App\GameBundle\Core\SocialNetworks\Authorize;

use App\GameBundle\Core\SocialNetworks\Authorize\AuthorizeSocial;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;

/**
 * @file
 * Содержит класс для авторизации пользователя в социальной сети Фотострана.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для авторизации пользователя в социальной сети Фотострана.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class AuthorizeSocialFS extends AuthorizeSocial
{
    /**
     * Процесс авторизации.
     *
     * @param Request $request объект с полученными от клиента данными
     * @throws GameException
     */
    public function process(Request &$request)
    {
        $userId  = null;
        $authKey = null;

        if ($request->getMethod() == 'POST') {
            $userId  = $request->request->get('userId');
            $authKey = $request->request->get('authKey');
        } elseif ($request->getMethod() == 'GET') {
            $userId  = $request->query->get('viewerId');
            $authKey = $request->query->get('authKey');
        }

        if ($userId === null || $authKey === null) {
            throw new GameException('Auth fail.', GameException::MISSING_PARAM);
        }

        if ($authKey != $this->getAuthHash($userId)) {
            throw new GameException('Auth fail.', GameException::SOCIAL_AUTH_FAIL);
        }
    }

    /**
     * Возвращает md5-подпись для проверки подлинности ключа авторизации от социальной сети.
     *
     * @param int $userId Идентификатор пользователя в социальной сети
     *
     * @return string Возвращает md5-подпись
     */
    public function getAuthHash($userId)
    {
        return md5(Config::get('appId') . '_' . $userId . '_' . Config::get('serverSecretKey'));
    }
}
