<?php

namespace App\GameBundle\Core\SocialNetworks\Authorize;

use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;

/**
 * @file
 * Содержит класс для авторизации пользователя в социальной сети.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Базовый абстрактный класс для авторизации пользователя в социальной сети.
 *
 * Содержит метод, позволяющий получить экземпляр класса для авторизации,
 * в зависимости от социальной сети, идентификатор которого прописан в главном конфиг. файле.
 *
 * <code>
 * <?php
 * AuthorizeSocial::create()->process($request);
 * ?>
 * </code>
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
abstract class AuthorizeSocial
{
    /**
     * Создает нужный объект класса, в зависимости от социальной сети.
     *
     * @return AuthorizeSocial
     * @throws GameException
     */
    public static function create()
    {
        $className = __CLASS__ . Config::get('socialNetwork');

        if (!class_exists($className)) {
            throw new GameException($className . ' not found.', GameException::GAME_ERROR);
        }

        return new $className();
    }

    /**
     * Производит процесс авторизации в игре по данным социальной сети.
     *
     * @param $request пост и гет параметры, пришедшие от клиента
     *
     * @return void
     * @throws GameException
     */
    abstract public function process(Request &$request);

    /**
     * Возвращает подпись для проверки подлинности ключа авторизации от социальной сети.
     *
     * @param int $userId Идентификатор пользователя в социальной сети
     *
     * @return string Возвращает md5-подпись для проверки подлинности данных из соц. сети
     */
    abstract public function getAuthHash($userId);
}
