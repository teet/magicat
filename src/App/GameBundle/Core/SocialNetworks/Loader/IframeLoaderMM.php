<?php

namespace App\GameBundle\Core\SocialNetworks\Loader;

use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\SocialNetworks\Loader\IframeLoader;
use App\GameBundle\Core\Models\UserData;

/**
 * @file
 * Содержит класс для инициализации параметров ифрейма, для социальной сети Мой Мир.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */

/**
 * Класс для инициализации параметров ифрейма, для социальной сети Мой Мир.
 *
 * Класс можно создать напрямую и использовать ее. Но, если функционал общий для всех социальных сетей,
 * то следует вызывать ее через базовый класс.
 *
 * <code>
 * $loader = IframeLoader::create($request);
 * </code>
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */
class IframeLoaderMM extends IframeLoader
{
    /**
     * Возвращает ключ, по которому приходит значения идентификатора пользователя в социальной сети в гет-параметрах.
     *
     * @return string
     */
    public function getViewerIdKey()
    {
        return 'vid';
    }

    /**
     * Выполняет необходимые действия при загрузке главной страницы ифрейма.
     *
     * @return array ассоциативный массив параметров для передачи в ифрейм
     * @throws GameException
     */
    public function process()
    {
        $userId = $this->getUserId();

        // Социальный квест. Фиксация установки приложения по приглашению.
        // если присутствует параметр $_GET['referer_type'] со значением
        // 'invitation', то пользователь перешел по приглашению.
        // @see http://api.mail.ru/docs/guides/ref/
        if ($this->_request->query->has('referer_type') &&
            $this->_request->query->has('referer_id')   &&
            $this->_request->query->get('referer_type') == 'invitation'
        ) {
            $refUserId = $this->_request->query->get('referer_id');

            if (is_numeric($refUserId) && $refUserId > 0 && $refUserId != $userId) {
                $userData = new UserData($refUserId);
                if ($userData->isExists()) {
                    $mask = $userData->getSocialQuestMask() | UserData::SQ_MASK_INVITE_FRIEND;
                    $userData->setSocialQuestMask($mask);
                }
            }
        }

        return array(
            'CLIENT_SECRET_KEY' => Config::get('clientSecretKey')
        );
    }
}
