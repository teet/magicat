<?php

namespace App\GameBundle\Core\SocialNetworks\Loader;

use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;

/**
 * @file
 * Содержит класс для инициализации параметров ифрейма, в зависимости от социальной сети.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Базовый абстрактный класс для инициализации параметров ифрейма, в зависимости от социальной сети.
 *
 * Содержит метод, позволяющий получить экземпляр нужного класса для загрузки ифрейма,
 * в зависимости от социальной сети, идентификатор которого прописан в главном конфиг. файле.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
abstract class IframeLoader
{
    /**
     * @var Request параметры запроса
     */
    protected $_request;

    /**
     * Конструктор класса.
     *
     * @param Request $request параметры запроса
     *
     * @return IframeLoader
     * @throws GameException
     */
    public function __construct(Request &$request)
    {
        if ($request == null) {
            throw new GameException('Request is null.', GameException::MISSING_PARAM);
        }

        $this->_request = $request;
    }

    /**
     * Создает нужный объект класса, в зависимости от социальной сети.
     *
     * @param Request $request параметры запроса
     *
     * @return IframeLoader
     * @throws GameException
     */
    public static function create(Request &$request)
    {
        $className = __CLASS__ . Config::get('socialNetwork');

        if (!class_exists($className)) {
            throw new GameException($className . ' not found.', GameException::GAME_ERROR);
        }

        return new $className($request);
    }

    /**
     * Возвращает идентификатор пользователя в социальной сети.
     *
     * @return int Идентификатор пользователя в социальной сети
     * @throws GameException
     */
    public function getUserId()
    {
        $userId = $this->_request->query->get($this->getViewerIdKey());

        if ($userId === null) {
            throw new GameException(
                'Parameter "' . $this->getViewerIdKey() . '" is missing.',
                GameException::MISSING_PARAM
            );
        }

        return $userId;
    }

    /**
     * Возвращает ключ, по которому приходит значения идентификатора пользователя в социальной сети в гет-параметрах.
     *
     * @return string
     */
    abstract public function getViewerIdKey();

    /**
     * Выполняет необходимые действия при загрузке главной страницы ифрейма.
     *
     * @return array ассоциативный массив параметров для передачи в ифрейм
     * @throws GameException
     */
    abstract public function process();
}
