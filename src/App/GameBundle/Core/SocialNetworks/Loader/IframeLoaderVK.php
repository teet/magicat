<?php

namespace App\GameBundle\Core\SocialNetworks\Loader;

use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\SocialNetworks\Loader\IframeLoader;
use App\GameBundle\Core\Partner\Partner;
use App\GameBundle\Core\Models\UserData;

/**
 * @file
 * Содержит класс для инициализации параметров ифрейма, для социальной сети Вконтакте.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для инициализации параметров ифрейма, для социальной сети Вконтакте.
 *
 * Класс можно создать напрямую и использовать ее. Но, если функционал общий для всех социальных сетей,
 * то следует вызывать ее через базовый класс.
 *
 * <code>
 * $loader = IframeLoader::create($request);
 * </code>
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class IframeLoaderVK extends IframeLoader
{
    /**
     * Возвращает ключ, по которому приходит значения идентификатора пользователя в социальной сети в гет-параметрах.
     *
     * @return string
     */
    public function getViewerIdKey()
    {
        return 'viewer_id';
    }

    /**
     * Выполняет необходимые действия при загрузке главной страницы ифрейма.
     *
     * @return array ассоциативный массив дополнительных параметров для передачи в ифрейм
     * @throws GameException
     */
    public function process()
    {
        $userId     = $this->getUserId();
        $parameters = array();

        // Если пользователь пришел по партнерской ссылке, сохраняем параметры.
        $partner = new Partner($userId);
        $offer   = $partner->saveOffer($this->_request);

        if (null === $offer) {
            $parameters['IS_OFFER']   = 0;
        } else {
            $parameters['IS_OFFER']   = 1;
            $parameters['VK_SID']     = $offer['vk_sid'];
            $parameters['VK_LEAD_ID'] = $offer['vk_lead_id'];
            $parameters['VK_HASH']    = $offer['vk_hash'];
        }

        // Социальный квест. Фиксация установки приложения по приглашению.
        // если присутствует параметр $_GET['referrer'] со значением 'request' или 'join_request',
        // то пользователь перешел по приглашению.
        if ($this->_request->query->has('referrer') &&
            $this->_request->query->has('user_id') &&
            in_array($this->_request->query->get('referrer'), array('request', 'join_request'))
        ) {
            $refUserId = $this->_request->query->get('user_id');

            if (is_numeric($refUserId) && $refUserId > 0 && $refUserId != $userId) {
                $userData = new UserData($refUserId);
                if ($userData->isExists()) {
                    $mask = $userData->getSocialQuestMask() | UserData::SQ_MASK_INVITE_FRIEND;
                    $userData->setSocialQuestMask($mask);
                }
            }
        }

        return $parameters;
    }
}
