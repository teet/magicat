<?php

namespace App\GameBundle\Core\SocialNetworks\Loader;

use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\SocialNetworks\Loader\IframeLoader;
use App\GameBundle\Core\Models\UserData;

/**
 * @file
 * Содержит класс для инициализации параметров ифрейма, для социальной сети Фотострана.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для инициализации параметров ифрейма, для социальной сети Фотострана.
 *
 * Класс можно создать напрямую и использовать ее. Но, если функционал общий для всех социальных сетей,
 * то следует вызывать ее через базовый класс.
 *
 * <code>
 * $loader = IframeLoader::create($request);
 * </code>
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class IframeLoaderFS extends IframeLoader
{
    /**
     * Возвращает ключ, по которому приходит значения идентификатора пользователя в социальной сети в гет-параметрах.
     *
     * @return string
     */
    public function getViewerIdKey()
    {
        return 'viewerId';
    }

    /**
     * Выполняет необходимые действия при загрузке главной страницы ифрейма.
     *
     * @return array ассоциативный массив параметров для передачи в ифрейм
     * @throws GameException
     */
    public function process()
    {
        $userId = $this->getUserId();

        // Социальный квест. Фиксация установки приложения по приглашению.
        // Фотострана присылает 10 последних айдишников, которые приглошали в игру.
        if ($this->_request->query->has('inviterIds')) {
            $friends = $this->_request->query->get('inviterIds');
            if ($friends != '') {
                $friends = explode(',', $friends);

                for ($i = 0; $i < sizeof($friends); $i++) {
                    if (is_numeric($friends[$i]) && $friends[$i] > 0 && $friends[$i] != $userId) {
                        $userData = new UserData($friends[$i]);
                        if ($userData->isExists()) {
                            $mask = $userData->getSocialQuestMask() | UserData::SQ_MASK_INVITE_FRIEND;
                            $userData->setSocialQuestMask($mask);
                        }
                        unset($userData);
                    }
                }
            }
        }

        return array(
            'CLIENT_SECRET_KEY' => Config::get('clientSecretKey')
        );
    }
}
