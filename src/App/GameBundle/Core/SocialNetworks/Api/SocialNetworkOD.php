<?php

namespace App\GameBundle\Core\SocialNetworks\Api;

use App\GameBundle\Core\SocialNetworks\Api\SocialNetwork;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Tools\Json;

/**
 * @file
 * Содержит класс для работы с апи социальной сети Одноклассники.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс, реализующий доступ к api для социальной сети Одноклассники.
 *
 * Подробнее здесь: http://apiok.ru/wiki/display/api/Odnoklassniki+REST+API+ru
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class SocialNetworkOD extends SocialNetwork
{
    /**
     * Адрес api-сервиса.
     *
     * Метод передаётся как параметр запроса.
     */
    const API_URL = 'http://api.odnoklassniki.ru/fb.do';

    /**
     * Ошибка рассылки нотификаций: достигнут лимит нотификаций для приложения.
     */
    const ERROR_TOTAL_LIMIT_REACHED = 26;

    /**
     * Код ошибки.
     *
     * @var integer
     */
    private $_errorCode = 0;

    /**
     * Текст сообщения об ошибке.
     *
     * @var string
     */
    private $_errorText = '';

    /**
     * Отправляет запрос к API.
     *
     * @param array  $data   дополнительные параметры запроса
     *
     * @return mixed Возвращает ответ в виде массива, или false
     */
    private function _sendRequest($data, $format = 'JSON')
    {
        $data['application_key'] = Config::get('clientPublicKey');
        $data['format']          = $format;

        ksort($data);

        $sig      = '';
        $postData = '';

        foreach ($data as $key => $value) {
            $sig      .= $key . '=' . $value;
            $postData .= $key . '=' . urlencode($value) . '&';
        }

        $sig      .= Config::get('serverSecretKey');
        $postData .= 'sig=' . md5($sig);

        // send data method post
        $curlOptions = curl_init();
        curl_setopt($curlOptions, CURLOPT_URL, self::API_URL);
        curl_setopt($curlOptions, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlOptions, CURLOPT_HEADER, 0);
        curl_setopt($curlOptions, CURLOPT_POST, 1);
        curl_setopt($curlOptions, CURLOPT_POSTFIELDS, $postData);

        $content = curl_exec($curlOptions);
        curl_close($curlOptions);

        if ($this->_isError($content)) {
            return false;
        }

        return Json::decode($content, true);
    }

    /**
     * Проверяет ответ от соцсети на ошибку.
     *
     * @param mixed $content content response curl
     *
     * @return bool
     */
    private function _isError($content)
    {
        if (false === $content) {
            $this->_errorCode = 0;
            $this->_errorText = 'Content is false';

            return true;
        }

        $response = Json::decode($content, true);
        if (is_array($response)
            && isset($response['error_code'])
            && isset($response['error_msg'])
        ) {
            $this->_errorCode = $response['error_code'];
            $this->_errorText = $response['error_msg'];

            return true;
        }

        return false;
    }

    /**
     * Возвращает код ошибки последнего запроса.
     *
     * Коды ошибок ОД:
     * http://apiok.ru/wiki/pages/viewpage.action?pageId=77824003
     *
     * @return integer
     */
    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    /**
     * Возвращает текст ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorText()
    {
        return $this->_errorText;
    }

    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * @param integer $userId идентификатор пользователя.
     * @param integer $level  новый уровень пользователя
     *
     * @return void
     */
    public function setUserLevel($userId, $level)
    {
        return;
    }

    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * @see http://apiok.ru/wiki/pages/viewpage.action?pageId=46137373 notifications.sendMass
     *
     * @param array $filterData Критерии фильтрации оповещаемых пользователей
     *
     * @return string|false Возвращает идентификатор массовой рассылки в случае успеха, иначе false
     */
    public function sendMassNotifications($filterData)
    {
        if (!is_array($filterData) || count($filterData) <= 0) {
            $this->_errorCode = 0;
            $this->_errorText = 'Incorrect value of parameter "filterData"';
            return false;
        }

        $filterData['method'] = 'notifications.sendMass';

        return $this->_sendRequest($filterData);
    }
}
