<?php

namespace App\GameBundle\Core\SocialNetworks\Api;

use App\GameBundle\Core\SocialNetworks\Api\SocialNetwork;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Tools\Json;

/**
 * @file
 * Содержит класс для работы с апи социальной сети Фотострана.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс, реализующий доступ к api для социальной сети Фотострана.
 *
 * http://fotostrana.ru/api/doc/
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class SocialNetworkFS extends SocialNetwork
{
    /**
     * Адрес api-сервиса.
     */
    const API_URL = 'http://api.fotostrana.ru/apifs.php';

    /**
     * @name Форматы ответа.
     * @{
     */
    const RESPONSE_FORMAT_JSON = 1;
    const RESPONSE_FORMAT_XML  = 2;
    /** @} */

    /**
     * @name Коды ошибок.
     * @{
     */
    const ERROR_BAD_REQUEST           = 1000;
    const ERROR_UNKNOWN_RESPONSE      = 1001;
    const ERROR_UNKNOWN               = 0;
    const ERROR_UNKNOWN_METHOD        = 202;
    const ERROR_INVALID_TIMESTAMP     = 206;
    const ERROR_INVALID_RAND          = 207;
    const ERROR_INVALID_APP_PARAMS    = 208;
    const ERROR_INVALID_SIGNATURE     = 209;
    const ERROR_INCORRECT_SIG         = 210;
    const ERROR_APPLICATION_DISABLED  = 211;
    const ERROR_INVALID_REQUEST       = 214;
    const ERROR_PERMISION_DENIED      = 400;
    const ERROR_TOO_MANY_REQUEST      = 405;
    const ERROR_WITHDRAW_ACTION       = 409;
    /** @} */

    /**
     * Хранит массив пар(номер ошибки: описание).
     *
     * @var array
     */
    protected $_errorsDescription = array(
        self::ERROR_BAD_REQUEST           => 'Ошибка запроса.',
        self::ERROR_UNKNOWN_RESPONSE      => 'Неверный ответ.',
        self::ERROR_UNKNOWN               => 'Неведомая ошибка.',
        self::ERROR_UNKNOWN_METHOD        => 'Неведомый метод.',
        self::ERROR_INVALID_TIMESTAMP     => 'Некорректное время(timestamp).',
        self::ERROR_INVALID_RAND          => 'Некорректное значение случайного числа(rand).',
        self::ERROR_INVALID_APP_PARAMS    => 'Неверные настройки приложения (appId, serverSecretKey)',
        self::ERROR_INVALID_SIGNATURE     => 'Неверная подпись запроса',
        self::ERROR_INCORRECT_SIG         => 'Некорректная сигнатура.',
        self::ERROR_APPLICATION_DISABLED  => 'Приложение отключено.',
        self::ERROR_INVALID_REQUEST       => 'Неверный запрос.',
        self::ERROR_PERMISION_DENIED      => 'Нет доступа.',
        self::ERROR_TOO_MANY_REQUEST      => 'Слишком много запросов в секунду.',
        self::ERROR_WITHDRAW_ACTION       => 'Перевод денежных средств невозможен.'
    );

    /**
     * Код ошибки.
     *
     * @var integer
     */
    private $_errorCode = 0;

    /**
     * Текст сообщения об ошибке.
     *
     * @var string
     */
    private $_errorText = '';

    /**
     * Отправляет запрос к API.
     *
     * @param array $params дополнительные параметры запроса
     *
     * @return mixed Возвращает ответ в виде массива, или false
     */
    private function _sendRequest($params)
    {
        $params['appId']     = Config::get('appId');
        $params['timestamp'] = time();
        $params['rand']      = mt_rand(1, 999999);
        $params['format']    = self::RESPONSE_FORMAT_JSON;

        ksort($params);

        $sig  = '';
        $data = '';

        foreach ($params as $key => $value) {
            $sig  .= $key . '=' . $value;
            $data .= $key . '=' . urlencode($value) . '&';
        }

        $data .= 'sig=' . md5($sig . Config::get('serverSecretKey'));

        // отправка get-запроса
        $curlOptions = curl_init();
        curl_setopt($curlOptions, CURLOPT_URL, self::API_URL . '?' . $data);
        curl_setopt($curlOptions, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlOptions, CURLOPT_HEADER, 0);

        $content = curl_exec ($curlOptions);
        curl_close ($curlOptions);

        if ($content === false) {
            $this->_setError(self::ERROR_BAD_REQUEST);
            return false;
        }

        $response = Json::decode($content);

        if (!is_array($response)) {
            $this->_setError(self::ERROR_UNKNOWN_RESPONSE);
            return false;
        }

        if (isset($response['error'])) {
            if (isset($response['error']['error_subcode'])) {
                $this->_setError($response['error']['error_subcode']);
            } else {
                $this->_setError($response['error']['error_code']);
            }

            return false;
        }

        if (!isset($response['response'])) {
            $this->_setError(self::ERROR_UNKNOWN_RESPONSE);
            return false;
        }

        return $response['response'];
    }

    /**
     * Устанавливает код и текст ошибки.
     *
     * @param integer $errorCode код ошибки
     * @return bool
     *
     */
    private function _setError($errorCode)
    {
        $this->_errorCode = $errorCode;

        if (isset($this->_errorsDescription[$errorCode])) {
            $this->_errorText = $errorCode . ': ' . $this->_errorsDescription[$errorCode];
        } else {
            $this->_errorText = 'Unknown error:' . $errorCode;
        }

        return true;
    }

    /**
     * Возвращает код ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    /**
     * Возвращает текст ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorText()
    {
        return $this->_errorText;
    }

    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * На момент разработи, такого функионала не было в Фотостране, поэтому поставлена заглушка.
     *
     * @param integer $userId идентификатор пользователя.
     * @param integer $level  новый уровень пользователя
     *
     * @return void
     * @throws GameException
     */
    public function setUserLevel($userId, $level)
    {
        return;
    }
}
