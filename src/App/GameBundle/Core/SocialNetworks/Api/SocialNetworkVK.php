<?php

namespace App\GameBundle\Core\SocialNetworks\Api;

use App\GameBundle\Core\SocialNetworks\Api\SocialNetwork;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Tools\Json;
use Redis;

/**
 * @file
 * Содержит класс для работы с апи социальной сети Вконтакте.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс, реализующий доступ к api для социальной сети Вконтакте.
 *
 * Подробнее здесь: https://vk.com/page-1_2369497
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class SocialNetworkVK extends SocialNetwork
{
    /**
     * Адрес api-сервиса.
     */
    const API_URL = 'http://api.vk.com/api.php';

    /**
     * Разница во времени между сервером приложения и сервером вконтакте.
     *
     * Если разница будет больше 100 секунд, то может выдать ошибку ERROR_INVALID_TIMESTAMP.
     */
    const TIME_DIFF = 0;

    /**
     * Ошибки.
     */
    const ERROR_BAD_REQUEST           = 1000;
    const ERROR_UNKNOWN_RESPONSE      = 1001;
    const ERROR_UNKNOWN               = 1;
    const ERROR_APPLICATION_DISABLED  = 2;
    const ERROR_UNKNOWN_METHOD        = 3;
    const ERROR_INCORRECT_SIG         = 4;
    const ERROR_USER_AUTH_FAIL        = 5;
    const ERROR_TOO_MANY_REQUEST      = 6;
    const ERROR_INVALID_REQUEST       = 8;
    const ERROR_NO_ACTIVITY_FROM_USER = 15;
    const ERROR_INVALID_APPID         = 101;
    const ERROR_INVALID_USERID        = 113;
    const ERROR_INVALID_TIMESTAMP     = 150;
    const ERROR_PERMISION_DENIED      = 500;

    /**
     * Хранит массив пар(номер ошибки: описание).
     *
     * @var array
     */
    protected $_errorsDescription = array(
        self::ERROR_BAD_REQUEST           => 'Неверный запрос.',
        self::ERROR_UNKNOWN_RESPONSE      => 'Неверный ответ.',
        self::ERROR_UNKNOWN               => 'Неведомая ошибка.',
        self::ERROR_APPLICATION_DISABLED  => 'Приложение отключено.',
        self::ERROR_UNKNOWN_METHOD        => 'Неведомый метод.',
        self::ERROR_INCORRECT_SIG         => 'Некорректная сигнатура.',
        self::ERROR_USER_AUTH_FAIL        => 'Не удалось авторизовать пользователя.',
        self::ERROR_TOO_MANY_REQUEST      => 'Слишком много запросов в секунду.',
        self::ERROR_INVALID_REQUEST       => 'Неверный запрос.',
        self::ERROR_NO_ACTIVITY_FROM_USER => 'Access denied: no activity from user for last 3 days',
        self::ERROR_INVALID_APPID         => 'Неверный идентификатор приложения.',
        self::ERROR_INVALID_USERID        => 'Некорректный идентификатор пользователя.',
        self::ERROR_INVALID_TIMESTAMP     => 'Некорректное время(timestamp).',
        self::ERROR_PERMISION_DENIED      => 'Нет доступа.'
    );

    /**
     * Код ошибки.
     *
     * @var integer
     */
    private $_errorCode = 0;

    /**
     * Текст сообщения об ошибке.
     *
     * @var string
     */
    private $_errorText = '';

    /**
     * Отправляет запрос к API.
     *
     * @param array  $data   дополнительные параметры запроса
     *
     * @return mixed Возвращает ответ в виде массива, или false
     */
    private function _sendRequest($data)
    {
        $data['api_id']    = Config::get('appId');
        $data['format']    = 'JSON';
        $data['v']         = '3.0';
        $data['timestamp'] = time() + self::TIME_DIFF;
        $data['random']    = mt_rand();

        ksort($data);

        $sig      = '';
        $postData = '';

        foreach ($data as $key => $value) {
            $sig      .= $key . '=' . $value;
            $postData .= $key . '=' . urlencode($value) . '&';
        }

        $sig      .= Config::get('serverSecretKey');
        $postData .= 'sig=' . md5($sig);

        // отправка запроса
        $curlOptions = curl_init();
        curl_setopt($curlOptions, CURLOPT_URL, self::API_URL);
        curl_setopt($curlOptions, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlOptions, CURLOPT_HEADER, 0);
        curl_setopt($curlOptions, CURLOPT_POST, 1);
        curl_setopt($curlOptions, CURLOPT_POSTFIELDS, $postData);

        $content = curl_exec ($curlOptions);
        curl_close ($curlOptions);

        if ($content === false) {
            $this->_setError(self::ERROR_BAD_REQUEST);
            return false;
        }

        $response = Json::decode($content);

        if (!is_array($response)) {
            $this->_setError(self::ERROR_UNKNOWN_RESPONSE);
            return false;
        }

        if (isset($response['error'])) {
            $this->_setError($response['error']['error_code']);
            return false;
        }

        return $response['response'];
    }

    /**
     * Устанавливает код и текст ошибки.
     *
     * @param integer $errorCode код ошибки
     * @return bool
     *
     */
    private function _setError($errorCode)
    {
        $this->_errorCode = $errorCode;

        if (isset($this->_errorsDescription[$errorCode])) {
            $this->_errorText = $errorCode . ': ' . $this->_errorsDescription[$errorCode];
        } else {
            $this->_errorText = 'Unknown error:' . $errorCode;
        }

        return true;
    }

    /**
     * Возвращает код ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    /**
     * Возвращает текст ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorText()
    {
        return $this->_errorText;
    }

    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * Метод просто добавляет новый элемент в очередь.
     * Эта очередь постепенно обрабатывается демоном на Си для отправки запросов порциями.
     *
     * Новый элемент должен предствлять из себя строку "userId:level".
     * @see https://vk.com/dev/secure.setUserLevel.
     *
     * @param integer $userId идентификатор пользователя.
     * @param integer $level  новый уровень пользователя
     *
     * @return void
     * @throws GameException
     */
    public function setUserLevel($userId, $level)
    {
        // Инициализация соединения с сервером редис.
        $redisServer = Config::get('redisStatistic');
        $redis       = new Redis();

        if ($redis->connect($redisServer['host'], $redisServer['port']) === false) {
            throw new GameException('Redis statistics connect error', GameException::SOCIAL_API_ERROR);
        }

        if ($redisServer['pass'] != '' && $redis->auth($redisServer['pass']) === false) {
            throw new GameException('Redis statistics auth error', GameException::SOCIAL_API_ERROR);
        }

        if ($redis->select($redisServer['db']) === false) {
            throw new GameException('Redis statistics select db error', GameException::SOCIAL_API_ERROR);
        }

        // Добавляет элемент в список
        $redis->rPush('userLevels', $userId . ':' . $level);
    }
}
