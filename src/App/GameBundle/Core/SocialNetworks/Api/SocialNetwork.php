<?php

namespace App\GameBundle\Core\SocialNetworks\Api;

use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;

/**
 * @file
 * Содержит базовый класс для работы с апи социальных сетей.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Базовый класс для работы с апи социальных сетей.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
abstract class SocialNetwork
{
    /**
     * Создает нужный объект класса, в зависимости от социальной сети.
     *
     * @return SocialNetwork
     * @throws GameException
     */
    public static function create()
    {
        $className = __CLASS__ . Config::get('socialNetwork');

        if (!class_exists($className)) {
            throw new GameException($className . ' not found.', GameException::GAME_ERROR);
        }

        return new $className();
    }

    /**
     * Возвращает код ошибки последнего запроса.
     *
     * @return integer
     */
    abstract public function getErrorCode();

    /**
     * Возвращает текст ошибки последнего запроса.
     *
     * @return integer
     */
    abstract public function getErrorText();

    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * @param integer $userId идентификатор пользователя.
     * @param integer $level  новый уровень пользователя
     *
     * @return void
     * @throws GameException
     */
    abstract public function setUserLevel($userId, $level);
}
