<?php

namespace App\GameBundle\Core\SocialNetworks\Api;

use App\GameBundle\Core\SocialNetworks\Api\SocialNetwork;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Tools\Json;

/**
 * @file
 * Содержит класс для работы с апи социальной сети Мой Мир.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */

/**
 * Класс, реализующий доступ к api для социальной сети Мой Мир.
 *
 * Подробнее здесь: http://api.mail.ru/docs/guides/restapi/
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */
class SocialNetworkMM extends SocialNetwork
{
    /**
     * Адрес api-сервиса.
     *
     * Метод передаётся как параметр запроса.
     */
    const API_URL = 'http://www.appsmail.ru/platform/api';

    /**
     * Код ошибки.
     *
     * @var integer
     */
    private $_errorCode = 0;

    /**
     * Текст сообщения об ошибке.
     *
     * @var string
     */
    private $_errorText = '';

    /**
     * Отправляет запрос к API.
     *
     * @param array  $data   дополнительные параметры запроса
     *
     * @return mixed Возвращает ответ в виде массива, или false
     */
    private function _sendRequest($data)
    {
        $data['app_id'] = Config::get('appId');
        $data['secure'] = 1; // Схема "Сервер-сервер".
        $data['format'] = 'json';

        ksort($data);

        $sig = '';
        $postData = '';
        foreach ($data as $key => $value) {
            $sig .= $key . '=' . $value;
            $postData .= $key . '=' . urlencode($value) . '&';
        }
        $sig .= Config::get('serverSecretKey');
        $postData .= 'sig=' . md5($sig);

        // send data method post
        $curlOptions = curl_init();
        curl_setopt($curlOptions, CURLOPT_URL, self::API_URL);
        curl_setopt($curlOptions, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlOptions, CURLOPT_HEADER, 0);
        curl_setopt($curlOptions, CURLOPT_POST, 1);
        curl_setopt($curlOptions, CURLOPT_POSTFIELDS, $postData);

        $content = curl_exec($curlOptions);
        curl_close($curlOptions);

        if ($this->_isError($content)) {
            return false;
        }

        return Json::decode($content, true);
    }

    /**
     * Проверяет ответ от соцсети на ошибку.
     *
     * @param mixed $content content response curl
     *
     * @return bool
     */
    private function _isError($content)
    {
        if (false === $content) {
            $this->_errorCode = 0;
            $this->_errorText = 'Content is false';

            return true;
        }

        $response = Json::decode($content, true);
        if (is_array($response)
            && isset($response['error']['error_code'])
            && isset($response['error']['error_msg'])
        ) {
            $this->_errorCode = $response['error']['error_code'];
            $this->_errorText = $response['error']['error_msg'];

            return true;
        }

        return false;
    }

    /**
     * Возвращает код ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    /**
     * Возвращает текст ошибки последнего запроса.
     *
     * @return integer
     */
    public function getErrorText()
    {
        return $this->_errorText;
    }

    /**
     * Отправляет уведомления пользователям о новом уровне игрока.
     *
     * @param integer $userId идентификатор пользователя.
     * @param integer $level  новый уровень пользователя
     *
     * @return void
     */
    public function setUserLevel($userId, $level)
    {
        return;
    }
}
