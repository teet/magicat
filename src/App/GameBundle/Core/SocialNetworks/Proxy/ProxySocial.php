<?php

namespace App\GameBundle\Core\SocialNetworks\Proxy;

use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Exception\PaymentException;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Models\Coins;

/**
 * @file
 * Содержит базовый классс для произведения платежей, в зависимости от социальной сети.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Базовый абстрактный класс для произведения платежей, в зависимости от социальной сети.
 *
 * Содержит метод, позволяющий получить экземпляр нужного класса,
 * в зависимости от социальной сети, идентификатор которого прописан в главном конфиг. файле.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
abstract class ProxySocial
{
    /**
     * Максимальное время по unixtime.
     *
     * 19 января 2038 года, 03:14:07.
     */
    const MAX_UNIX_TIMESTAMP = 2147483647;

    /**
     * Создает нужный объект класса, в зависимости от социальной сети.
     *
     * @return ProxySocial
     * @throws GameException
     */
    public static function create()
    {
        $className = __CLASS__ . Config::get('socialNetwork');

        if (!class_exists($className)) {
            throw new GameException($className . ' not found.', GameException::GAME_ERROR);
        }

        return new $className();
    }

    /**
     * Добавляет ресурсы пользователю.
     *
     * @param string   $userId   идентификатор пользователя
     * @param array    $item     данные покупаемого предмета
     * @param UserData $userData ссылка на объект UserData
     *
     * @return void
     * @throws PaymentException
     */
    protected function _addResources($userId, $item, &$userData)
    {
        // Данные этапа скидочной акции (монетизационные ивенты для новичков).
        $offerDiscount = $userData->getOfferDiscount();

        // Если первая покупка за реал, кроме ивентов, то блокируем дальнейшую покупку ивентов
        if ($item['type'] != 'offerProduct' && $offerDiscount[1] != self::MAX_UNIX_TIMESTAMP) {
            // устанавливаем макс. время открытия ивента(т.е. чтобы, никогда не открылась)
            $offerDiscount[1] = self::MAX_UNIX_TIMESTAMP;
            $userData->setOfferDiscount($offerDiscount);
        }

        switch ($item['type']) {
            case 'offerProduct':
                // Покупка монет по скидочной акции. Изменяем состояние этапа и времени доступности акции.
                if (isset($item['step'])
                    && ($item['step'] >= $offerDiscount[0])
                    && (time() >= $offerDiscount[1])
                ) {
                    // этап акции увеличивается и становится доступным со следующих суток
                    $offerDiscount[0] = $item['step'] + 1;
                    $offerDiscount[1] = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
                    $userData->setOfferDiscount($offerDiscount);
                } else {
                    // ошибка, недопустимое действие
                    throw new PaymentException(
                        "Wrong action, product is not avaible",
                        PaymentException::ADD_RESOURCES_ERROR
                    );
                }
                // without break, еще начисляется золото
            /* HACK: временно убрано
            case 'discountProduct':
                // если убрать HACK и раскомментировать, то это условие необходимо.
                if ($item['type'] == 'discountProduct') {
                    $lives = $userData->getLives();
                    $lives += $item['lifeCount'];
                    $userData->setLives($lives);
                }
                // without break, еще начисляется золото
            */
            case 'coinProduct':
            case 'spinCoinProduct':
                // Начисляем золото.
                $coinsObject = new Coins($userId);
                $coins  = $coinsObject->getCoins();
                $coins += $item['goldCount'];
                $coinsObject->setCoins($coins);
                break;
        }
    }

    /**
     * Выполняет необходимые действия при произведении платежа.
     *
     * @return void
     */
    abstract public function process();
}
