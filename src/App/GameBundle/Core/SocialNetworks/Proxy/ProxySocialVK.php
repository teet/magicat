<?php

namespace App\GameBundle\Core\SocialNetworks\Proxy;

use App\GameBundle\Core\Item\ItemCatalog;
use App\GameBundle\Core\Exception\PaymentException;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Log\Log;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocial;
use Exception;

/**
 * @file
 * Содержит классс для обработки платежей в социальной сети Вконтакте.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для обработки платежей в социальной сети Вконтакте.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class ProxySocialVK extends ProxySocial
{
    /**
     * @name Состояния платежа.
     * @{
     */
    const PAYMENT_STATE_FAILED  = 0; // Платеж не удалось совершить с 1-й попытки.
    const PAYMENT_STATE_SUCCESS = 1; // Платеж прошел успешно.
    /** @} */

    /** Время хранения данных о транзакции в БД. */
    const CACHE_TIMEOUT         = 172800;
    /** TODO добавить описание. Передается соц. сети, при запросе данных о покупаемом продукте. */
    const ITEM_CACHE_EXPIRATION = 600;

    /**
     * @name Коды ошибок.
     * @{
     */
    const ERROR_GENERAL          = 1;
    const ERROR_DATABASE         = 2;
    const ERROR_SIGNATURE        = 10;
    const ERROR_PARAMS           = 11;
    const ERROR_NONEXIST_ITEM    = 20;
    const ERROR_UNAVAILABLE_ITEM = 21;
    const ERROR_NONEXIST_USER    = 22;
    const ERROR_NONEXIST_STATUS  = 100;
    /** @} */

    static protected $_errorDescription = array (
        1 => array (
            'error_code' => 1,
            'error_msg'  => 'общая ошибка',
            'critical'   => 'false',
        ),
        2 => array (
            'error_code' => 2,
            'error_msg'  => 'временная ошибка базы данных',
            'critical'   => 'false',
        ),
        10 => array (
            'error_code' => 10,
            'error_msg'  => 'несовпадение вычисленной и переданной подписи',
            'critical'   => 'true',
        ),
        11 => array (
            'error_code' => 11,
            'error_msg'  => 'параметры запроса не соответствуют спецификации',
            'critical'   => 'true',
        ),
        20 => array (
            'error_code' => 20,
            'error_msg'  => 'товара не существует',
            'critical'   => 'true',
        ),
        21 => array (
            'error_code' => 21,
            'error_msg'  => 'товара нет в наличии',
            'critical'   => 'true',
        ),
        22 => array (
            'error_code' => 22,
            'error_msg'  => 'пользователя не существует',
            'critical'   => 'true',
        ),
        100 => array (
            'error_code' => 100,
            'error_msg'  => 'некорректный статус платежа',
            'critical'   => 'true',
        )
    );

    /**
     * Хранит ссылку на объект Log.
     *
     * @var Log
     */
    protected $_log;

    /**
     * Хранит ссылку на объект UserData.
     *
     * @var UserData
     */
    private $_userData = null;

    /**
     * Конструктор.
     */
    public function __construct()
    {
        $this->_log = new Log('ProxyVkontakte');
    }

    /**
     * Обработка платежного запроса.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    public function process()
    {
        $this->_log->write(print_r($_POST, true));

        try {
            $this->_checkRequestData();

            switch ($_POST['notification_type']) {
                // Получение информации о товаре.
                // http://vk.com/dev/payments_getitem
                case 'get_item':
                case 'get_item_test':
                    $this->_sendItemInfo();
                    break;

                // Изменение статуса заказа.
                // http://vk.com/dev/payments_status
                case 'order_status_change':
                case 'order_status_change_test':
                    $this->_makePayment();
                    break;

                default:
                    // Ошибка в случае неизвестного типа запроса.
                    $this->_sendError(self::ERROR_PARAMS);
                    break;
            }
        } catch (PaymentException $e) {
            $this->_sendError($e->getCode());
        } catch (Exception $e) {
            $this->_sendError(self::ERROR_GENERAL);
        }
    }

    /**
     * Отправка информации о товаре.
     *
     * @return void
     */
    protected function _sendItemInfo()
    {
        $item = $this->_getItem($_POST['item']);
        $this->_sendResponse(array(
            'response' => array(
                'title'      => isset($item['title']) ? $item['title'] : 'undefined',
                'photo_url'  => 'http://' . Config::get('statServer') . '/stat/media/payItemsIcon/'
                                . $item['key'] . '.png?v=' . Config::get('version'),
                'price'      => $item['PriceSocial'],
                'item_id'    => $item['key'],
                'expiration' => self::ITEM_CACHE_EXPIRATION
        )));
    }

    /**
     * Изменение статуса заказа.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _makePayment()
    {
        // Проверка статуса платежа.
        if ($_POST['status'] != 'chargeable') {
            throw new PaymentException(
                self::$_errorDescription[self::ERROR_NONEXIST_STATUS]['error_msg'],
                self::ERROR_NONEXIST_STATUS
            );
        }

        $cache = RedisStore::getInstance();
        $key = self::getKey($_POST['order_id']);

        if (!$cache->exists($key)) {
            $this->_makeNewPayment();
        } else {
            $state = $cache->fetch($key);
            if ($state == self::PAYMENT_STATE_FAILED) {
                $this->_makeFailedPayment();
                return;
            }

            $this->_makeSuccessPayment();
        }
    }

    /**
     * Производит новый платеж.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    protected function _makeNewPayment()
    {
        $userId = $_POST['user_id'];
        $item   = $this->_getItem($_POST['item']);

        try {
            $cache = RedisStore::getInstance();

            // Пополнение ресурсов пользователя, в зависимости от предмета покупки.
            try {
                $this->_addResources($userId, $item, $this->_userData);
            } catch (PaymentException $e) {
                throw new PaymentException(
                    $e->getMessage(),
                    self::ERROR_UNAVAILABLE_ITEM
                );
            }

            $cache->save(self::getKey($_POST['order_id']), self::PAYMENT_STATE_SUCCESS, self::CACHE_TIMEOUT);
            $this->_sendPayment();

            $log = new Log('Payments_Success');
            $log->write($_POST['order_id'] . ";" . $userId . ";" . $_POST['item_price'] . ";" . $_POST['item']);

            $this->_log->write('_makeNewPayment; ok');

            // В отдельный файл сохраняем список завершенных транзакций.
            $this->saveTransactionInLog($_POST['order_id']);
        } catch (PaymentException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->_log->write($e->getCode() . ' | ' . $e->getMessage());
            $this->_log->write('_makeNewPayment; failed');

            $cache->save(self::getKey($_POST['order_id']), self::PAYMENT_STATE_FAILED, self::CACHE_TIMEOUT);
            $this->_sendError(self::ERROR_DATABASE);

            $log = new Log('Payments_Failed');
            $log->write($_POST['order_id'] . ";" . $userId . ";" . $_POST['item_price'] . ";" . $_POST['item']);
        }
    }

    /**
     * Сохраняет в файл завершенную транзакцию.
     *
     * return void
     */
    protected function saveTransactionInLog($transactionId)
    {
        $f = fopen(Config::get('logDir') . "/Transactions.csv", 'a+');
        fwrite($f, $transactionId . "\n");
        fclose($f);
    }

    /**
     * Повторный платеж для провалившейся операции.
     *
     * @return void
     */
    private function _makeFailedPayment()
    {
        $this->_makeNewPayment();
        $this->_log->write('_makeFailedPayment; ok');
    }

    /**
     * Обработка ситуации для обработанного ранее платежа.
     *
     * @return void
     *
     */
    private function _makeSuccessPayment()
    {
        $this->_log->write('_makeSuccessPayment; true');
        $this->_sendPayment();
    }

    /**
     * Получение товара из каталога товаров.
     *
     * @param string $itemId  наименование товара, переданное диалоговому окну покупки
     *
     * @return array
     * @throws PaymentException
     */
    protected function _getItem($itemId)
    {
        $catalog = ItemCatalog::getCatalog($_POST['user_id']);
        foreach ($catalog as $item) {
            if ($item['key'] == $itemId) {
                return $item;
            }
        }

        throw new PaymentException(self::$_errorDescription[self::ERROR_NONEXIST_ITEM]['error_msg'], self::ERROR_NONEXIST_ITEM);
    }

    /**
     * Отправка статуса ошибки.
     *
     * @param integer $errorCode    код ошибки
     * @return void
     */
    protected function _sendError($errorCode)
    {
        $this->_sendResponse(array(
            'error' => self::$_errorDescription[$errorCode]
        ));
    }

    /**
     *
     * @param type $id
     */
    protected function _sendPayment()
    {
        $this->_sendResponse(array(
            'response' => array(
                'order_id' => $_POST['order_id']
        )));
    }

    /**
     * Отправка ответа биллингу.
     *
     * @param array $data   массив с данными ответа
     * @return void
     */
    protected function _sendResponse($data)
    {
        $jsonData = json_encode($data);
        echo $jsonData;

        $this->_log->write($jsonData);
    }

    /**
     * Вычисление сигнатуры.
     *
     * @return string
     */
    protected function _getSig()
    {
        $env = $_POST;
        unset($env['sig']);
        ksort($env);

        $sigString = '';
        foreach ($env as $key => $value) {
            $sigString .= $key . '=' . $value;
        }

        return md5($sigString . Config::get('serverSecretKey'));
    }

    /**
     * Проверка входных данных.
     *
     * @throws PaymentException
     */
    protected function _checkRequestData()
    {
        if (   !isset($_POST['notification_type'])
            || !isset($_POST['app_id'])
            || !isset($_POST['user_id'])
            || !isset($_POST['receiver_id'])
            || !isset($_POST['order_id'])
            || !isset($_POST['item'])
            || !isset($_POST['sig'])
            || (
                   ($_POST['notification_type'] == 'order_status_change_test'
                || $_POST['notification_type'] == 'order_status_change')
                && !(
                       isset($_POST['item_id'])
                    && isset($_POST['item_title'])
                    && isset($_POST['item_price'])
                )
            )
        ) {
            throw new PaymentException(
                self::$_errorDescription[self::ERROR_PARAMS]['error_msg'],
                self::ERROR_PARAMS
            );
        }

        if ($this->_getSig() != $_POST['sig']) {
            throw new PaymentException(
                self::$_errorDescription[self::ERROR_SIGNATURE]['error_msg'],
                self::ERROR_SIGNATURE
            );
        }

        $this->_userData = new UserData($_POST['user_id']);

        if (!$this->_userData->isExists()) {
            throw new PaymentException(
                self::$_errorDescription[self::ERROR_NONEXIST_USER]['error_msg'],
                self::ERROR_NONEXIST_USER
            );
        }
    }

    /**
     * Возвращает ключ транзакции в кэше.
     *
     * @param integer $transactionId    id транзакции
     * @return string
     */
    public static function getKey($transactionId)
    {
        return 't:' . $transactionId;
    }
}
