<?php

namespace App\GameBundle\Core\SocialNetworks\Proxy;

use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Item\ItemCatalog;
use App\GameBundle\Core\Exception\PaymentException;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocial;
use App\GameBundle\Core\Log\Log;
use App\GameBundle\Core\Tools\Json;
use Exception;

/**
 * @file
 * Содержит классс для обработки платежей в социальной сети Фотострана.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

/**
 * Класс для обработки платежей в социальной сети Фотострана.
 *
 * <code>
 * $dispatcher = new ProxySocialFS();
 * $dispatcher->process();
 * </code>
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */
class ProxySocialFS extends ProxySocial
{
    /**
     * @name Тип запроса от социальной сети.
     * @{
     */
    const REQUEST_TYPE_PAYMENT = 1; // Произведение платежа пользователем.
    const REQUEST_TYPE_CHECK   = 2; // Проверка транзакции на завершенность.
    /** @} */

    /**
     * @name Коды ошибок.
     * @{
     */
    const ERROR_PARAMS            = 1;
    const ERROR_INVALID_SIGNATURE = 2;
    const ERROR_ITEM_NOT_EXISTS   = 3;
    const ERROR_USER_NOT_FOUND    = 4;
    /** @} */

    /**
     * @var array Описание ошибок.
     */
    protected static $_errorDescription = array (
        self::ERROR_PARAMS            => 'параметры запроса не соответствуют спецификации',
        self::ERROR_INVALID_SIGNATURE => 'неверная сигнатура запроса',
        self::ERROR_ITEM_NOT_EXISTS   => 'товара не существует',
        self::ERROR_USER_NOT_FOUND    => 'пользователя не существует'
    );

    /**
     * Хранит ссылку на объект Log.
     *
     * @var Log
     */
    protected $_log;

    /**
     * @var UserData Хранит ссылку на объект UserData.
     */
    protected $_userData = null;

    /**
     * @var array Данные о покупаемом предмете.
     */
    protected $_item = null;

    /**
     * Конструктор.
     */
    public function __construct()
    {
        // хранит поля: сообщение;ид транзакции;post-данные;
        $this->_log = new Log('ProxyFsDebug');
    }

    /**
     * Обработка платежного запроса.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    public function process()
    {
        try {
            $this->_checkRequestData();

            if ($_POST['type'] == self::REQUEST_TYPE_CHECK) {
                $this->_checkTransaction($_POST['transactionId']);
            } else {
                $this->_makePayment();
            }
        } catch (Exception $e) {
            $errorData = Json::encode(array(
                'error' => $e->getMessage(),
                'code'  => $e->getCode()
            ));

            $log = new Log('ProxyFsError');
            $log->write(Json::encode($_POST) . ';' . $errorData);

            $this->_sendResponse(array(
                'result' => 0,
                'isTemp' => 0
            ));
        }
    }

    /**
     * Вычисление сигнатуры.
     *
     * @return string
     */
    protected function _getSig()
    {
        $params = $_POST;
        unset($params['sig']);
        ksort($params);

        $sig = '';
        foreach ($params as $key => $value) {
            $sig .= $key . '=' . $value;
        }

        return md5($sig . Config::get('serverSecretKey'));
    }

    /**
     * Проверка входных данных и сигнатуры запроса.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkRequestData()
    {
        if (!isset($_POST['type'])
            || ($_POST['type'] != self::REQUEST_TYPE_CHECK && $_POST['type'] != self::REQUEST_TYPE_PAYMENT)
            || !isset($_POST['appUid'])
            || !isset($_POST['transactionId'])
            || !isset($_POST['sig'])
        ) {
            throw new PaymentException(self::$_errorDescription[self::ERROR_PARAMS], self::ERROR_PARAMS);
        }

        if ($_POST['type'] == self::REQUEST_TYPE_PAYMENT) {
            if (!isset($_POST['itemId'])
                || !isset($_POST['userId'])
                || !isset($_POST['receiverId'])
                || !isset($_POST['priceFmCents'])
                || !isset($_POST['forwardData'])
                || !isset($_POST['isDebug'])
            ) {
                throw new PaymentException(self::$_errorDescription[self::ERROR_PARAMS], self::ERROR_PARAMS);
            }

            // Данные о покупаемом предмете.
            $this->_item = ItemCatalog::getItem($_POST['forwardData'], $_POST['receiverId']);

            if (!is_array($this->_item) || !isset($this->_item['PriceSocial'])) {
                throw new PaymentException(self::$_errorDescription[self::ERROR_ITEM_NOT_EXISTS],
                                           self::ERROR_ITEM_NOT_EXISTS);
            }

            // Проверка существования пользователя и выборка его данных.
            $this->_userData = new UserData($_POST['receiverId']);

            if (!$this->_userData->isExists()) {
                throw new PaymentException(self::$_errorDescription[self::ERROR_USER_NOT_FOUND],
                                           self::ERROR_USER_NOT_FOUND);
            }
        }

        if ($this->_getSig() != $_POST['sig']) {
            throw new PaymentException(self::$_errorDescription[self::ERROR_INVALID_SIGNATURE],
                                       self::ERROR_INVALID_SIGNATURE);
        }
    }

    /**
     * Производит проверки и начисление нужных ресурсов пользователю.
     *
     * @return void
     * @throws PaymentException, GameException
     */
    protected function _makePayment()
    {
        $userId = $_POST['receiverId'];
        $cache  = RedisStore::getInstance();
        $key    = self::getKey($_POST['transactionId']);

        // проверка на существование транзакции
        if ($cache->exists($key)) {
            $this->_log->write('TransactionExists;' . $_POST['transactionId'] . ';' . Json::encode($_POST));
            $this->_sendResponse(array(
                'result' => 0,
                'isTemp' => 1
            ));

            return;
        }

        if ($this->_item['PriceSocial'] != $_POST['priceFmCents']) {
            $this->_log->write('IncorrectPrice;' . $_POST['transactionId'] . ';'
                               . Json::encode($_POST) . ';' . $this->_item['PriceSocial']);
        }

        // Пополнение ресурсов пользователя, в зависимости от предмета покупки.
        try {
            $this->_addResources($userId, $this->_item, $this->_userData);
        } catch (PaymentException $e) {
            throw new PaymentException(
                $e->getMessage(),
                self::ERROR_ITEM_NOT_EXISTS
            );
        }

        // Получение нового уникального идентификатора транзакции и занесение в БД.
        $transId = $this->_getNewTransactionId();
        $cache->save($key, $transId);

        // Отправка биллингу успешного ответа с идентификатором внутренней транзакции.
        $this->_sendResponse(array(
            'result'        => 1,
            'transactionId' => $transId
        ));

        $log = new Log('ProxyFsSuccess');
        $log->write($userId . ";" . $_POST['transactionId'] . ';'
                    . $transId . ';' . $this->_item['key'] . ';'
                    . $_POST['priceFmCents'] . ';' . $_POST['isDebug']);
    }

    /**
     * Возвращает уникальный идентификатор для новой внутренней транзакции.
     *
     * Если ключа не существует, то он пропишется и вернет 1,
     * иначе вернет следующий по порядку идентификатор транзакции.
     *
     * @return int
     */
    private function _getNewTransactionId()
    {
        $cache = RedisStore::getInstance();
        return $cache->incr(self::getTransactionCounterKey());
    }

    /**
     * Проверяет статус транзакции в системе Фотостраны.
     *
     * При запросе проверки статуса платежа ($_POST['type'] == 2), необходимо проверить,
     * была ли успешно завешена покупка по идентификатору транзакции $_POST['transactionId'].
     *
     * Если такая транзакция уже была обработана успешно,
     * то возвращается ид транзакции в приложении, иначе 0.
     *
     * @param string $transactionId идентификатор транзакции
     *
     * @return void
     */
    private function _checkTransaction($transactionId)
    {
        $cache   = RedisStore::getInstance();
        $key     = self::getKey($transactionId);
        $transId = 0;

        if ($cache->exists($key)) {
            $transId = $cache->fetch($key);
        }

        $this->_sendResponse(array(
            'transactionId' => $transId
        ));

        $this->_log->write('CheckTransaction;' . $transactionId . ';' . Json::encode($_POST) . ';' . $transId);
    }

    /**
     * Отправка ответа биллингу.
     *
     * @param array $data массив с данными ответа
     * @return void
     */
    protected function _sendResponse($data)
    {
        echo Json::encode($data);
    }

    /**
     * Возвращает ключ, по которому храниться счетчик внутренних транзакций в кэше.
     *
     * @see _makePayment
     * @return string
     */
    public static function getTransactionCounterKey()
    {
        return 'tc';
    }

    /**
     * Возвращает ключ транзакции в кэше.
     *
     * По этому ключу храниться уникальный идентификатор транзакции в приложении.
     * transactions:123{ид транзакции в фотостране} => 456{ид транзакции у нас в приложении}
     *
     * @param string $transactionId идентификатор транзакции в фотостране
     * @return string
     */
    public static function getKey($transactionId)
    {
        return 't:' . $transactionId;
    }
}
