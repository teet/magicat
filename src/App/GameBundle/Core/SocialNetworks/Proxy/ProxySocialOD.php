<?php

namespace App\GameBundle\Core\SocialNetworks\Proxy;

use App\GameBundle\Core\Item\ItemCatalog;
use App\GameBundle\Core\Exception\PaymentException;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Log\Log;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocial;
use Exception;

/**
 * @file
 * Содержит классс для обработки платежей в социальной сети Одноклассники.
 * @see http://apiok.ru/wiki/display/api/Payment+processing
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */

/**
 * Класс для обработки платежей в социальной сети Одноклассники.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */
class ProxySocialOD extends ProxySocial
{
    /**
     * @name Состояния платежа.
     * @{
     */
    const PAYMENT_STATE_FAILED  = 0; // Платеж не удалось совершить с 1-й попытки.
    const PAYMENT_STATE_SUCCESS = 1; // Платеж прошел успешно.
    /** @} */

    /**
     * @name Коды ошибок.
     * @see http://apiok.ru/wiki/display/api/callbacks.payment
     * @{
     */
    // Unknown error.
    const ERROR_UNKNOWN                  = 1;
    // Service temporary unavailable.
    const ERROR_SERVICE                  = 2;
    // Payment is invalid and can not be processed.
    const ERROR_CALLBACK_INVALID_PAYMENT = 1001;
    // Critical system failure, which can not be recovered.
    const ERROR_SYSTEM                   = 9999;
    // Invalid signature.
    const ERROR_PARAM_SIGNATURE          = 104;
    /** @} */

    /** Время хранения данных о транзакции в БД. */
    const CACHE_TIMEOUT = 86400;

    /**
     * Описание ошибок.
     *
     * @var array
     */
    protected static $_errorMsg = array(
        self::ERROR_UNKNOWN                  => 'UNKNOWN : Unknown error',
        self::ERROR_SERVICE                  => 'SERVICE : Service temporary unavailable',
        self::ERROR_CALLBACK_INVALID_PAYMENT => 'CALLBACK_INVALID_PAYMENT : Payment is invalid and can not be processed',
        self::ERROR_SYSTEM                   => 'SYSTEM : Critical system failure, which can not be recovered',
        self::ERROR_PARAM_SIGNATURE          => 'PARAM_SIGNATURE : Invalid signature '
    );

    /**
     * Хранит ссылку на объект Log.
     *
     * @var Log
     */
    protected $_log;

    /**
     * Хранит ссылку на объект UserData.
     *
     * @var UserData
     */
    protected $_userData = null;

    /**
     * Хранит информацию о покупаемом предмете.
     *
     * @var array
     */
    protected $_item = null;

    /**
     * Конструктор.
     */
    public function __construct()
    {
        $this->_log = new Log('ProxySocialOD');
    }

    /**
     * Обработка платежного запроса.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    public function process()
    {
        $this->_log->write(print_r($_SERVER['QUERY_STRING'], true));

        try {
            $this->_checkRequestData();

            $cache = RedisStore::getInstance();
            $key   = self::getKey($_GET['transaction_id']);

            if (!$cache->exists($key)) {
                $this->_makeNewPayment();
            } else {
                $state = $cache->fetch($key);
                if ($state == self::PAYMENT_STATE_SUCCESS) {
                    $this->_makeSuccessPayment();
                } else {
                    $this->_makeFailedPayment();
                }
            }
        } catch (PaymentException $e) {
            $this->_sendResponse(array(
                'code' => $e->getCode(),
                'msg'  => $e->getMessage()
            ));
        } catch (Exception $e) {
            $this->_sendResponse(array(
                'code' => self::ERROR_UNKNOWN,
                'msg'  => self::$_errorMsg[self::ERROR_UNKNOWN]
            ));
            $this->_log->write('ERROR' . ';' . $e->getCode() . ';' . $e->getMessage());
        }
    }

    /**
     * Проверяет входные $_GET данные на валидность запроса
     * билинга от odnoklassniki.ru и кидает исключение если данные не верные.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkRequestData()
    {
        if (!isset($_GET['uid'])
            || !isset($_GET['transaction_time'])
            || !isset($_GET['transaction_id'])
            || !isset($_GET['product_code'])
            || !isset($_GET['amount'])
            || !isset($_GET['sig'])
            || !isset($_GET['application_key'])
        ) {
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_PARAM_SIGNATURE],
                self::ERROR_PARAM_SIGNATURE
            );
        }

        if ($_GET['sig'] != $this->_getSig()) {
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_PARAM_SIGNATURE],
                self::ERROR_PARAM_SIGNATURE
            );
        }

        $this->_checkAndInitItem();
        $this->_checkAndInitUserData();
    }

    /**
     * Проверяет пользователя на существование.
     * Инициализирует свойство $_userData.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkAndInitUserData()
    {
        $userData = new UserData($_GET['uid']);
        if (!$userData->isExists()) {
            $this->_log->write('ERROR;0;User not found');
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_CALLBACK_INVALID_PAYMENT],
                self::ERROR_CALLBACK_INVALID_PAYMENT
            );
        }

        $this->_userData = $userData;
    }

    /**
     * Проверяет покупаемый предмет на существование в каталоге предметов.
     * Инициализирует свойство $_item.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkAndInitItem()
    {
        $item = ItemCatalog::getItem($_GET['product_code'], $_GET['uid']);
        if (null === $item) {
            $this->_log->write(sprintf('ERROR;0;Item not found: %s', $_GET['product_code']));
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_CALLBACK_INVALID_PAYMENT],
                self::ERROR_CALLBACK_INVALID_PAYMENT
            );
        }
        if (!isset($item['PriceSocial'])) {
            $this->_log->write(sprintf('ERROR;0;Item price not set: %s', $_GET['product_code']));
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_CALLBACK_INVALID_PAYMENT],
                self::ERROR_CALLBACK_INVALID_PAYMENT
            );
        }
        if ($item['PriceSocial'] != $_GET['amount']) {
            $this->_log->write(sprintf('ERROR;0;"amount" and "PriceSocial" not equal: %s != %s', $_GET['amount'], $item['PriceSocial']));
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_CALLBACK_INVALID_PAYMENT],
                self::ERROR_CALLBACK_INVALID_PAYMENT
            );
        }

        $this->_item = $item;
    }

    /**
     * Создает SIG подпись из данных запроса.
     * @see http://apiok.ru/wiki/display/api/Authentication+and+authorization
     *
     * @return string   SIG строка
     */
    protected function _getSig()
    {
        $env = $_GET;
        unset($env['sig']);
        ksort($env);

        $sigString = '';
        foreach ($env as $key => $value) {
            $sigString .= $key . '=' . $value;
        }

        return md5($sigString . Config::get('serverSecretKey'));
    }

    /**
     * Производит новый платеж.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    protected function _makeNewPayment()
    {
        $userId = $_GET['uid'];

        try {
            $cache = RedisStore::getInstance();
            $key   = self::getKey($_GET['transaction_id']);

            try {
                $this->_addResources($userId, $this->_item, $this->_userData);
            } catch (PaymentException $e) {
                throw new PaymentException(
                    $e->getMessage(),
                    self::ERROR_CALLBACK_INVALID_PAYMENT
                );
            }

            $this->_log->write('_makeNewPayment; ok');

            $log = new Log('Payments_Success');
            $log->write($userId . ';' . $_GET['transaction_id'] . ';' . $_GET['amount'] . ';' . $_GET['product_code']);

            $this->_sendResponse();
            $cache->save($key, self::PAYMENT_STATE_SUCCESS, self::CACHE_TIMEOUT);
        } catch (PaymentException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->_log->write('_makeNewPayment; failed');

            $log = new Log('Payments_Failed');
            $log->write($userId . ';' . $_GET['transaction_id'] . ';' . $_GET['amount'] . ';' . $_GET['product_code']);

            $cache->save($key, self::PAYMENT_STATE_FAILED, self::CACHE_TIMEOUT);

            throw $e;
        }
    }

    /**
     * Обрабатывает ситуацию, когда пришел запрос на
     * успешно проведенный ранее платеж.
     *
     * @return void
     */
    private function _makeSuccessPayment()
    {
        $this->_sendResponse();
        $this->_log->write('_makeSuccessPayment; ok');
    }

    /**
     * Повторный платеж для провалившейся операции.
     *
     * @return void
     */
    private function _makeFailedPayment()
    {
        $this->_log->write('_makeFailedPayment; start');
        $this->_makeNewPayment();
        $this->_log->write('_makeFailedPayment; ok');
    }

    /**
     * Отправляет ответ биллингу.
     *
     * @param mixed $data   данные для отправки
     * @return void
     */
    protected function _sendResponse($data = null)
    {
        header('Content-Type: application/xml; charset=utf-8');
        $response = '';
        if (null === $data) {
            $response = '<?xml version="1.0" encoding="UTF-8"?>
                <callbacks_payment_response xmlns="http://api.forticom.com/1.0/">
                true
                </callbacks_payment_response>';
        } else {
            header('invocation-error: ' . $data['code']);
            $response = '<?xml version="1.0" encoding="UTF-8"?>
                <ns2:error_response xmlns:ns2="http://api.forticom.com/1.0/">
                    <error_code>' . $data['code'] . '</error_code>
                    <error_msg>' . $data['msg'] . '</error_msg>
                </ns2:error_response>';
        }

        echo $response;

        $this->_log->write($response);
    }

    /**
     * Возвращает ключ транзакции в кэше.
     *
     * @param integer $transactionId    id транзакции
     * @return string
     */
    public static function getKey($transactionId)
    {
        return 't:' . $transactionId;
    }
}
