<?php

namespace App\GameBundle\Core\SocialNetworks\Proxy;

use App\GameBundle\Core\Item\ItemCatalog;
use App\GameBundle\Core\Exception\PaymentException;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Log\Log;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Singleton\RedisStore;
use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocial;
use Exception;

/**
 * @file
 * Содержит классс для обработки платежей в социальной сети Мой Мир.
 * @see http://api.mail.ru/docs/guides/billing/
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */

/**
 * Класс для обработки платежей в социальной сети Одноклассники.
 *
 * @author Amir Kumalov <amir@e-magic.org>
 */
class ProxySocialMM extends ProxySocial
{
    /**
     * @name Состояния платежа.
     * @{
     */
    const PAYMENT_STATE_FAILED   = 0; // Услуга на данный момент не оказана, но может быть оказана позднее.
    const PAYMENT_STATE_SUCCESS  = 1; // Услуга оказана успешно.
    const PAYMENT_STATE_CANCELED = 2; // Услуга не может быть оказана.
    /** @} */

    /**
     * @name Коды ошибок.
     * @{
     */
    // Other error — другая ошибка.
    const ERROR_OTHER             = 700;
    // User not found — если приложение не смогло найти пользователя для оказания услуги.
    const ERROR_USER_NOT_FOUND    = 701;
    // Service not found — если услуга с данным идентификатором не существует в вашем приложении.
    const ERROR_SERVICE_NOT_FOUND = 702;
    // Incorrect price for given uid and service_id — если данная услуга для данного пользователя
    // не могла быть оказана за указанную цену.
    const ERROR_INCORRECT_PRICE   = 703;
    /** @} */

    /** Время хранения данных о транзакции в БД. */
    const CACHE_TIMEOUT = 86400;

    /**
     * Описание ошибок.
     *
     * @var array
     */
    protected static $_errorMsg = array(
        self::ERROR_OTHER             => 'Other error',
        self::ERROR_USER_NOT_FOUND    => 'User not found',
        self::ERROR_SERVICE_NOT_FOUND => 'Service not found',
        self::ERROR_INCORRECT_PRICE   => 'Incorrect price for given uid and service_id'
    );

    /**
     * Хранит ссылку на объект Log.
     *
     * @var Log
     */
    protected $_log;

    /**
     * Хранит ссылку на объект UserData.
     *
     * @var UserData
     */
    protected $_userData = null;

    /**
     * Хранит информацию о покупаемом предмете.
     *
     * @var array
     */
    protected $_item = null;

    /**
     * Конструктор.
     */
    public function __construct()
    {
        $this->_log = new Log('ProxySocialMM');
    }

    /**
     * Обработка платежного запроса.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    public function process()
    {
        $this->_log->write(print_r($_SERVER['QUERY_STRING'], true));

        try {
            $this->_checkRequestData();

            $cache = RedisStore::getInstance();
            $key   = self::getKey($_GET['transaction_id']);

            if (!$cache->exists($key)) {
                $this->_makeNewPayment();
            } else {
                $state = $cache->fetch($key);
                if ($state == self::PAYMENT_STATE_SUCCESS) {
                    $this->_makeSuccessPayment();
                } else {
                    $this->_makeCanceledPayment();
                }
            }
        } catch (PaymentException $e) {
            $this->_sendResponse(self::PAYMENT_STATE_CANCELED, $e->getCode());
            $this->_log->write('ERROR' . ';' . $e->getCode() . ';' . $e->getMessage());
        } catch (Exception $e) {
            $this->_sendResponse(self::PAYMENT_STATE_CANCELED, self::ERROR_OTHER);
            $this->_log->write('ERROR' . ';' . $e->getCode() . ';' . $e->getMessage());
        }
    }

    /**
     * Проверяет входные $_GET данные на валидность запроса
     * билинга от odnoklassniki.ru и кидает исключение если данные не верные.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkRequestData()
    {
        if (!isset($_GET['app_id'])
            || !isset($_GET['transaction_id'])
            || !isset($_GET['service_id'])
            || !isset($_GET['uid'])
            || !isset($_GET['sig'])
            || !isset($_GET['mailiki_price'])
            || !isset($_GET['profit'])
        ) {
            throw new PaymentException(
                'Missing params',
                self::ERROR_OTHER
            );
        }

        if ($_GET['sig'] != $this->_getSig()) {
            throw new PaymentException(
                'Invalid signature',
                self::ERROR_OTHER
            );
        }

        $this->_checkAndInitItem();
        $this->_checkAndInitUserData();
    }

    /**
     * Проверяет пользователя на существование.
     * Инициализирует свойство $_userData.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkAndInitUserData()
    {
        $userData = new UserData($_GET['uid']);
        if (!$userData->isExists()) {
            throw new PaymentException(
                self::$_errorMsg[self::ERROR_USER_NOT_FOUND] . ': ' . $_GET['uid'],
                self::ERROR_USER_NOT_FOUND
            );
        }

        $this->_userData = $userData;
    }

    /**
     * Проверяет покупаемый предмет на существование в каталоге предметов.
     * Инициализирует свойство $_item.
     *
     * @return void
     * @throws PaymentException
     */
    protected function _checkAndInitItem()
    {
        $item = ItemCatalog::getItemByItemId($_GET['service_id'], $_GET['uid']);
        if (null === $item) {
            throw new PaymentException(
                sprintf('Item not found: %s', $_GET['service_id']),
                self::ERROR_SERVICE_NOT_FOUND
            );
        }
        if (!isset($item['PriceSocial'])) {
            throw new PaymentException(
                sprintf('Item price not set: %s', $_GET['service_id']),
                self::ERROR_INCORRECT_PRICE
            );
        }
        if ($item['PriceSocial'] != $_GET['mailiki_price']) {
            throw new PaymentException(
                sprintf('"mailiki_price" and "PriceSocial" not equal: %s != %s', $_GET['mailiki_price'], $item['PriceSocial']),
                self::ERROR_INCORRECT_PRICE
            );
        }

        $this->_item = $item;
    }

    /**
     * Создает SIG подпись из данных запроса.
     * @see http://api.mail.ru/docs/guides/restapi/#secure
     *
     * @return string   SIG строка
     */
    protected function _getSig()
    {
        $env = $_GET;
        unset($env['sig']);
        ksort($env);

        $sigString = '';
        foreach ($env as $key => $value) {
            $sigString .= $key . '=' . $value;
        }

        return md5($sigString . Config::get('serverSecretKey'));
    }

    /**
     * Производит новый платеж.
     *
     * @return void
     * @throws PaymentException, Exception
     */
    protected function _makeNewPayment()
    {
        $userId = $_GET['uid'];

        try {
            $cache = RedisStore::getInstance();
            $key   = self::getKey($_GET['transaction_id']);

            try {
                $this->_addResources($userId, $this->_item, $this->_userData);
            } catch (PaymentException $e) {
                throw new PaymentException(
                    $e->getMessage(),
                    self::ERROR_OTHER
                );
            }

            $this->_log->write('_makeNewPayment; ok');

            $log = new Log('Payments_Success');
            $log->write($userId . ';' . $_GET['transaction_id'] . ';' . $_GET['mailiki_price'] . ';' . $this->_item['key']);

            $cache->save($key, self::PAYMENT_STATE_SUCCESS, self::CACHE_TIMEOUT);
            $this->_sendResponse(self::PAYMENT_STATE_SUCCESS);
        } catch (PaymentException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->_log->write('_makeNewPayment; canceled');

            $log = new Log('Payments_Canceled');
            $log->write($userId . ';' . $_GET['transaction_id'] . ';' . $_GET['mailiki_price'] . ';' . $this->_item['key']);

            $cache->save($key, self::PAYMENT_STATE_CANCELED, self::CACHE_TIMEOUT);

            throw $e;
        }
    }

    /**
     * Обрабатывает ситуацию, когда пришел запрос на
     * успешно проведенный ранее платеж.
     *
     * @return void
     */
    private function _makeSuccessPayment()
    {
        $this->_sendResponse(self::PAYMENT_STATE_SUCCESS);
        $this->_log->write('_makeSuccessPayment; ok');
    }

    /**
     * Повторный платеж для провалившейся операции.
     *
     * @return void
     */
    private function _makeCanceledPayment()
    {
        $this->_log->write('_makeCanceledPayment; start');
        $this->_sendResponse(self::PAYMENT_STATE_CANCELED, self::ERROR_OTHER);
        $this->_log->write('_makeCanceledPayment; ok');
    }

    /**
     * Отправляет ответ биллингу.
     *
     * @param integer $status       статус обработки услуги:
     *                              0 — услуга на данный момент не оказана, но может быть оказана позднее
     *                              1 — услуга оказана успешно;
     *                              2 — услуга не может быть оказана;
     * @param integer $errorCode    код ошибки, если услуга не оказана (status равен 0 или 2);
     *                              если status=1, то error_code передавать не нужно
     *
     * @return void
     */
    protected function _sendResponse($status, $errorCode = null)
    {
        $response = array(
            'status' => $status
        );

        if ($errorCode !== null) {
            $response['error_code'] = $errorCode;
        }

        $response = json_encode($response);
        echo $response;

        $this->_log->write($response);
    }

    /**
     * Возвращает ключ транзакции в кэше.
     *
     * @param integer $transactionId    id транзакции
     * @return string
     */
    public static function getKey($transactionId)
    {
        return 't:' . $transactionId;
    }
}
