<?php
namespace App\GameBundle\Core\Authorize\Location;

use App\GameBundle\Core\Models\PlayerObjects;
use App\GameBundle\Core\Models\UserData;
use App\GameBundle\Core\Models\Coins;
use App\GameBundle\Core\Models\WeeklyChallenges;
use App\GameBundle\Core\Models\Gifts;
use App\GameBundle\Core\Models\SplitData;

class AuthorizeLocation
{
    /**
     * Идентификатор пользователя.
     *
     * @var int
     */
    private $_userId;

    /**
     * __construct
     *
     * @param int $userId           идентификатор пользователя
     */
    public function __construct($userId)
    {
        $this->_userId = $userId;
    }

    /**
     * Регистрирует нового пользователя.
     *
     * @return boolean
     */
    public function register()
    {
        $userData = new UserData($this->_userId);
        if ($userData->isExists() == false) {
            $userData->init();
        } else {
            return false;
        }

        $playerObject = new PlayerObjects($this->_userId);
        if ($playerObject->isExists() == false) {
            $playerObject->init();
        }

        $coins = new Coins($this->_userId);
        if ($coins->isExists() == false) {
            $coins->init();
        }

        $weeklyChallenges = new WeeklyChallenges($this->_userId);
        if (!$weeklyChallenges->isExists()) {
            $weeklyChallenges->init();
        }

        $gifts = new Gifts($this->_userId);
        if (!$gifts->isExists()) {
            $gifts->init();
        }

        $splitData = new SplitData($this->_userId);
        if (!$splitData->isExists()) {
            $splitData->init();
        }

        return true;
    }
}
