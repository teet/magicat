<?php

namespace App\GameBundle\Core\Authorize;

use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Singleton\RedisStore;

/**
 * @file
 * Класс проверки подписи для входных данных.
 *
 * @author Amir Kumalov amir@e-magic.org
 */

/**
 * Класс проверки подписи для входных данных.
 *
 * Содержит метод process, который проверяет подпись.
 *
 * @author Amir Kumalov amir@e-magic.org
 * @version 1.0
 */
class AuthorizeSignature
{
    const SESSION_EXPIRE = 129600; // 36 часов.

    /**
     * Проверка входных данных.
     *
     * @return void
     */
    public function process()
    {
        $this->_checkSignature();
        $this->_checkSession();
        //$this->_checkResponseCounter();
    }

    /**
     * Проверка подписи для входных данных.
     *
     * @return void
     * @throws GameException
     */
    private function _checkSignature()
    {
        if (!isset($_POST['md5'])) {
            throw new GameException('Missing "md5" parameter.', GameException::INCORRECT_SIGNATURE);
        }

        $request = $_POST;
        unset($request['md5']);

        $md5Str = Config::get('signatureKey');
        foreach ($request as $param => $value) {
            $md5Str .= $value;
        }

        $strParts = preg_split('//u', $md5Str, -1, PREG_SPLIT_NO_EMPTY);
        sort($strParts);
        $md5Str = implode('', $strParts);

        $md5 = md5($md5Str);

        if ($md5 != $_POST['md5']) {
            throw new GameException('Incorrect signature.', GameException::INCORRECT_SIGNATURE);
        }
    }

    /**
     * Проверка сессии.
     *
     * @return void
     * @throws GameException
     */
    private function _checkSession()
    {
        $cache = RedisStore::getInstance();
        $session = $cache->fetch(self::getSessionKey($_POST['userId']));

        if ($session != $_POST['session']) {
            throw new GameException('Session fail.', GameException::SESSION_FAIL);
        }
    }

    /**
     * Проверка счетчика для полученного запроса.
     *
     * Если для запроса '/progress/get' получена новая сессия, то
     * сбрасываются все счетчики путем удаления ключа из кэша.
     *
     * @return void
     * @throws GameException
     */
    private function _checkResponseCounter()
    {
        $cache = RedisStore::getInstance();
        $key = self::getResponseCounterKey($_POST['userId']);

        $counter = $cache->hFetch($key, $_SERVER['REQUEST_URI']);
        if ($counter === false) {
            $counter = -1;
        }
        if ($_POST['counter'] != $counter + 1) {
            throw new GameException('Response counter error.', GameException::RESPONSE_COUNTER_ERROR);
        }

        $cache->hSave($key, $_SERVER['REQUEST_URI'], $_POST['counter']);
        $cache->expire($key, self::SESSION_EXPIRE);
    }

    /**
     * Генерирует, сохраняет и возвращает новую сессию для клиента.
     * Очищает счетчики запросов.
     *
     * @param integer $userId   идентификатор пользователя
     * @return integer
     */
    public static function generateAndSaveSession($userId)
    {
        $session = mt_rand(1,999999);

        $cache = RedisStore::getInstance();
        $cache->save(self::getSessionKey($userId), $session, self::SESSION_EXPIRE);

        // Очищаем счетчики запросов.
        //$cache->delete(self::getResponseCounterKey($userId));

        return $session;
    }

    /**
     * Возвращает ключ сессии.
     *
     * @param integer $userId   id пользователя
     * @return string
     */
    public static function getSessionKey($userId)
    {
        return 's:s:' . $userId;
    }

    /**
     * Возвращает ключ счетчика запросов.
     *
     * @param integer $userId   id пользователя
     * @return string
     */
    public static function getResponseCounterKey($userId)
    {
        return 's:r:' . $userId;
    }
}
