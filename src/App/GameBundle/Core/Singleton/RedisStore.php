<?php

namespace App\GameBundle\Core\Singleton;

use Redis;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\Exception\GameException;

class RedisStore
{
    private static $_instance;

    private $_redis;

    private function __construct()
    {
        $this->_redis = new Redis();
        $redisServer = Config::get('redisServer');
        try {
            $this->_redis->connect($redisServer['host'], $redisServer['port']);
        } catch (\RedisException $e) {
            throw new GameException($e->getMessage(), GameException::CACHE_CONNECTION_ERROR);
        }

        if ($redisServer['pass'] != "") {
            $result = $this->_redis->auth($redisServer['pass']);
            if (false === $result) {
                throw new GameException('Auth redis fail.', GameException::CACHE_CONNECTION_ERROR);
            }
        }

        $result = $this->_redis->select($redisServer['db']);
        if (false === $result) {
            throw new GameException('Error select redis db.', GameException::CACHE_ERROR);
        }
    }

    private function __clone() {}
    private function __wakeup() {}

    public static function getInstance()
    {
        if (empty(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function hFetch($key, $field)
    {
        return $this->_redis->hGet($key, $field);
    }

    public function hFetchAll($key)
    {
        return $this->_redis->hGetAll($key);
    }

    public function hSave($key, $field, $value)
    {
        $this->_redis->hSet($key, $field, $value);
    }

    /**
     * Заполняет все поля в хеш-таблице
     *
     * @param $key string Имя сохраняемого ключа
     * @param $array array Массив key => value
     *
     * @see https://github.com/nicolasff/phpredis#hmset
     */
    public function hSaveAll($key, $array)
    {
        $this->_redis->hMset($key, $array);
    }

    public function save($key, $value, $timeout = null)
    {
        if ($timeout === null) {
            $this->_redis->set($key, $value);
        } else {
            $this->_redis->set($key, $value, $timeout);
        }
    }

    public function fetch($key)
    {
        return $this->_redis->get($key);
    }

    /**
     * Проверяет существование ключа в кэше.
     *
     * @param string $key   название ключа
     * @return boolean  true  - ключ существует
     *                  false - ключ отсутствует
     */
    public function exists($key)
    {
        return $this->_redis->exists($key);
    }

    /**
     * Запуск транзакции.
     *
     * @return void
     */
    public function multi()
    {
        $this->_redis->multi();
    }

    /**
     * Завершение транзакции.
     *
     * @return void
     */
    public function exec()
    {
        $this->_redis->exec();
    }

    public function discard()
    {
        $this->_redis->discard();
    }

    /**
     * Удаляет ключ.
     *
     * @param string $key
     * @return integer
     */
    public function delete($key)
    {
        $this->_redis->delete($key);
    }

    /**
     * Устанавливает время жизни ключу.
     *
     * @param string  $key
     * @param integer $ttl
     * @return bool
     */
    public function expire($key, $ttl)
    {
        $this->_redis->expire($key, $ttl);
    }

    /**
     * Возвращает массив найденных ключей по заданому шаблону.
     *
     * @param string $pattern   шаблона для поиска
     * @return array
     */
    public function keys($pattern)
    {
        return $this->_redis->keys($pattern);
    }

    /**
     * Прибавляет единицу к целочисленному значению по указанному ключу.
     *
     * Если ключа не существует, то он создается и заносится занчение 1.
     *
     * @param string $key название ключа
     * @return int Возвращает значение, которое хранилось по ключу, увеличенное на единицу.
     */
    public function incr($key)
    {
        return $this->_redis->incr($key);
    }

    /**
     * Прибавляет единицу к целочисленному значению поля hash-таблицы.
     *
     * @param string $key   название ключа
     * @param string $field название поля
     * @param int    $value значение, на которое нужно увеличить поле
     *
     * @return int Возвращает значение, которое хранилось по ключу, увеличенное на указанное значение.
     */
    public function hIncrBy($key, $field, $value)
    {
        return $this->_redis->hIncrBy($key, $field, $value);
    }
}
