/**
 * @file
 * Скрипт запуска nodejs-сервера для ведения статистики онлайн/оффлайн игрков в игре.
 *
 * Держит в актуальном состоянии, множества онлайн и оффлайн пользователей в редисе.
 * Отправляет данные о времени захода в игру и продолжительности сессии
 * на сервер статистики.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 * @author Amir Kumalov <amir@e-magic.org>
 */

// Загрузка параметров из конфигурационного файла.
try {
    var config = require('./config.json');
} catch(err) {
    console.log("Ошибка загрузки конфигурационного файла." + err);
    process.exit(1);
}

// Инициализация модулей.
var extra         = require('./extra'); // дополнительные функции для работы приложения.
var socialNetwork = require('./sn/' + config.socialNetwork.toLowerCase()); // функции для работы с соц. сетью.
var fs            = require('fs');
var redis         = require('redis');
var server        = require('socket.io');

// Инициализация подключения к redis-серверу.
var redisStore = redis.createClient(config.redisStatistic.port, config.redisStatistic.host);

redisStore.on("error", function (err) {
    console.log("Redis error: " + err);
});

if (config.redisStatistic.pass != '') {
    redisStore.auth(config.redisStatistic.pass, function() {
        redisStore.select(config.redisStatistic.db);
        startNewSession();
    });
} else {
    redisStore.select(config.redisStatistic.db);
    startNewSession();
}

if (typeof gc === 'function') {
    setInterval(gc, 60 * 1000)
}

/**
 * Старт новой сессии сервера.
 *
 * @return void
 */
function startNewSession()
{
    var lastSessionCloseTs = extra.getTime();

    extra.initialize(config, redisStore);
    extra.moveAllToOffline(lastSessionCloseTs, runServer);
}

/**
 * Инициализация и запуск сервера.
 *
 * @return void
 */
function runServer()
{
    // Инициализация socket.io
    var io;

    if (config.nodeServer.host.indexOf('https://') == 0 && config.nodeServer.sslKey != '') {
        // запускаем socket.io через https
        var sslOptions = {
            key:  fs.readFileSync(config.nodeServer.sslKey),
            cert: fs.readFileSync(config.nodeServer.sslCert)
        };

        var app = require('https').createServer(sslOptions, function(request, response) {
            response.writeHead(200);
            response.end("Service running...");
        });

        io = server.listen(app);
        app.listen(config.nodeServer.port);
    } else {
        io = server.listen(config.nodeServer.port);
    }

    /**
     * set RedisStore as store instance for client storage
     * to scale to multiple process and/or multiple servers
     * @see https://github.com/LearnBoost/Socket.IO/wiki/Configuring-Socket.IO
     */
    var RedisStore = require('socket.io/lib/stores/redis')
      , pub = redis.createClient(config.redisStatistic.port, config.redisStatistic.host)
      , sub = redis.createClient(config.redisStatistic.port, config.redisStatistic.host)
      , client = redis.createClient(config.redisStatistic.port, config.redisStatistic.host);

    if (config.redisStatistic.pass != '') {
        pub.auth(config.redisStatistic.pass, function() {
            pub.select(3);
        });
        sub.auth(config.redisStatistic.pass, function() {
            sub.select(3);
        });
        client.auth(config.redisStatistic.pass, function() {
            client.select(3);
        });
    } else {
        pub.select(3);
        sub.select(3);
        client.select(3);
    }

    pub.on('error', function(err) {
        console.log('pub:', err);
    });
    sub.on('error', function(err) {
        console.log('sub:', err);
    });
    client.on('error', function(err) {
        console.log('client:', err);
    });

    io.set('store', new RedisStore({
        redis:       redis,
        redisPub:    pub,
        redisSub:    sub,
        redisClient: client
    }));

    // Уровень вывода ошибок socket.io.
    io.set('log level', 0);

    io.sockets.on('connection', function (socket) {
        socket.userId = 0;

        socket.on('doAuth', function(userId, authKey, params) {
            var isAuthorized = socialNetwork.auth(config, userId, authKey, params);

            if (isAuthorized) {
                socket.userId           = userId;
                socket.timeSessionStart = extra.getTime();
                extra.addToOnline(socket.userId);
            }

            socket.emit("onAuth", isAuthorized);
        });

        socket.on('disconnect', function() {
            if (socket.userId > 0) {
                extra.addToOffline(socket.userId);

                // Отправляем на сервер статистики событие окончания сессии.
                extra.sendStatistics({
                    "userId": socket.userId,
                    "data": JSON.stringify([{
                        "event": "sessionEnd",
                        "timestamp": extra.getTime() * 1000, // Время окончания сессии.
                        "timeStart": socket.timeSessionStart
                    }])
                });
            }
        });
    });
}
