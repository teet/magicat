/**
 * @file
 * Скрипт запуска nodejs-воркеров
 * Для запуска воркеров используется пакет cluster.
 * Воркеры запускаются через каждые 10 секунд (сделано для того чтобы отчистка мусора не "резонировала")
 *
 * @author Arthur <teet@e-magic.org>
 */

var cluster = require( 'cluster' );
var wcount  = 4;

cluster.setupMaster({
    exec: 'app.js'
});

var tid = setInterval( function() {
    if (--wcount < 0) {
        clearInterval(tid);
    }
    else {
        cluster.fork();
    }
}, 10000 );
