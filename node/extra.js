/**
 * @file
 * Модуль содержит вспомогательные функции для работы серера.
 *
 * Для использоваиния модуля, его сначала нужно инициализировать, вызвав функцию initialize().
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

var request = require('request');

// Конфигурационные параметры.
var config = {};

// Клиент хранилища редис.
var redisStore;

// Ключи в редисе, по которым хранятся множества олнайн и оффлайн пользователей.
var onlineUsersKey  = 'onlineUsers';
var offlineUsersKey = 'offlineUsers';

// Размер элем-ов, которых нужно переместить из множества онлайн во множество оффлайн за раз.
// Если установть в -1, то будут перемещены все элементы за один проход. @see moveAllToOffline.
var moveToOfflineChunkSize = 1000 - 1; // отнимаем 1, т.к. элементы выбираются начиная с нуля до указанного значения.

/**
 * Инициализация модуля.
 *
 * @param cfg         объект, содержащий настроки приложения
 * @param redisClient клиент хранилища redis
 * @param debug       флаг, определяющий режим отладки
 *
 * @return void
 */
exports.initialize = function(cfg, redisClient)
{
    config     = cfg;
    redisStore = redisClient;
}

/**
 * Возвращает серверное время в unixtime.
 *
 * @return int unix timestamp
 */
function getTime()
{
    return Math.floor((new Date).getTime() / 1000);
}
exports.getTime = getTime;

/**
 * Отправляет данные на сервер статистики.
 *
 * @param postData         Json-объект данных для логирования
 * @param completeCallback Функция обратного вызова, которая будет вызвана при завершении запроса.
 *                         Единственный параметр - (boool)флаг успешности отправки.
 *
 * @return void
 */
exports.sendStatistics = function(postData, completeCallback)
{
    request.post(
        'http://' + config.statisticsServer  + '/statistics/index',
        {form: postData},
        function(error, response, body) {
            if (error) {
                console.log('Request error: user[' + postData.userId + ']: ', error);

                if (completeCallback) {
                    completeCallback(false);
                }
                return;
            }

            if (completeCallback) {
                completeCallback(true);
            }
        }
    );
}

/**
 * Отправка данных о групповом завершении игровой сессии на сервер статистики.
 *
 * Функция вызывается при перезапуске сервера, чтобы данные не потерялись.
 * @see moveAllToOffline
 *
 * @param postData         данные для логирования
 * @param completeCallback Функция обратного вызова, которая будет вызвана при завершении запроса.
 *                         Единственный параметр - (boool)флаг успешности отправки.
 *
 * @return void
 */
function sendGroupStatistics(postData, completeCallback)
{
    request.post(
        'http://' + config.statisticsServer + '/statistics/offlineAll',
        {form: postData},
        function(error, response, body) {
            if (error) {
                console.log('Request error: data[', postData, ']: error: ', error);

                if (completeCallback) {
                    completeCallback(false);
                }
                return;
            }

            console.log("Send statistics: ", body);

            if (completeCallback) {
                completeCallback(true);
            }
        }
    );
}

/**
 * Добавляет онлайн-пользователей, которые остались на предыдущем сеансе в список оффлайн.
 *
 * @param offlineTs        время выхода пользователей в оффлайн(для статистики)
 * @param completeCallback функция обратного вызова
 * @return void
 */
function moveAllToOffline(offlineTs, completeCallback)
{
    redisStore.zcard([onlineUsersKey], function(err, replyInt) {
        if (err) {
           throw err;
        }

        if (replyInt > 0) {
            redisStore.zrange([onlineUsersKey, 0, moveToOfflineChunkSize, "WITHSCORES"], function(err, replyArray) {
                if (err) {
                   throw err;
                }

                if (replyArray.length <= 0) {
                    console.log('_moveUsersToOffline: completed');
                    if (completeCallback) {
                        completeCallback();
                    }
                    return;
                }

                // Отправка статистических данных на сервер
                var postData = {
                    "users": JSON.stringify(replyArray),
                    "timestamp": offlineTs
                };

                sendGroupStatistics(postData, function(requestOk) {
                    if (!requestOk) {
                        return;
                    }

                    var zAddCommand = [offlineUsersKey];

                    for (var i = 0; i < replyArray.length; i += 2) {
                        zAddCommand.push(offlineTs, replyArray[i]);
                    }

                    // Добавление группы пользователей во множество оффлайн
                    redisStore.zadd(zAddCommand, function(err, reply) {
                        if (err) {
                           throw err;
                        }

                        // Удаление обработанной группы пользователей из мн-ва онлайн.
                        redisStore.zremrangebyrank([onlineUsersKey, 0, moveToOfflineChunkSize], function(err, reply) {
                            if (err) {
                               throw err;
                            }

                            // Повторный вызов функции для обработки следующей группы онлайн-пользователей.
                            moveAllToOffline(offlineTs, completeCallback);
                        });
                    });
                });
            });
        } else {
            console.log('moveUsersToOffline: completed');
            if (completeCallback) {
                completeCallback();
            }
        }
    });
}
exports.moveAllToOffline = moveAllToOffline;

/**
 * Добавляет пользователя во множество онлайн-пользователей.
 *
 * @param userId           идентификатор пользователя в социальной сети
 * @param completeCallback функция обратного вызова, где в качестве параметра передается userId
 *
 * @return void
 */
exports.addToOnline = function(userId, completeCallback)
{
    redisStore.zrem([offlineUsersKey, userId], function(err, status) {
        if (err) {
            throw err;
        }

        redisStore.zadd([onlineUsersKey, getTime(), userId], function(err, status) {
            if (err) {
                throw err;
            }

            if (completeCallback) {
                completeCallback(userId);
            }
        });
    });
}

/**
 * Добавляет пользователя во множество оффлайн-пользователей.
 *
 * @param userId           идентификатор пользователя в социальной сети
 * @param completeCallback функция обратного вызова, где в качестве параметра передается userId
 *
 * @return void
 */
exports.addToOffline = function(userId, completeCallback)
{
    redisStore.zrem([onlineUsersKey, userId], function(err, status) {
        if (err) {
            throw err;
        }

        redisStore.zadd([offlineUsersKey, getTime(), userId], function(err, status) {
            if (err) {
                throw err;
            }

            if (completeCallback) {
                completeCallback(userId);
            }
        });
    });
}
