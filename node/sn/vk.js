/**
 * @file
 * Содержит функции для работы с социальной сетью Вконтакте.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

var crypto = require('crypto');

/**
 * Проверяет подпись авторизации пользователя.
 *
 * @param config  параметры приложения для данной соц. сети
 * @param userId  идентификатор пользователя в соц. сети
 * @param authKey подпись авторизации на стороне соц. сети(с которой необходимо сравнить нашу подпись)
 * @param params  объект с дополнительными параметрами
 *
 * @return bool "true", если подпись верна, иначе "false"
 */
exports.auth = function(config, userId, authKey, params)
{
    if (userId == '' || authKey == '') {
        return false;
    }

    var data = '' + config.appId + '_' + userId + '_' + config.serverSecretKey;
    var hash = crypto.createHash('md5').update(data).digest('hex');

    return (hash == authKey);
}
