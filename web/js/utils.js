/**
 * Служебные функции для главной страницы.
 *
 * file: utils.js
 * category: utils
 * author: Artur
 * author: Azamat
 */

/**
 * Функция инициализации игры.
 *
 * @param swf_url
 * @param falsh_vars
 */
function initGame(swfUrl, flashVars)
{
    var minFlashVersion = '11';

    swfobject.embedSWF(
        swfUrl,
        "game",
        "100%",
        "572",
        minFlashVersion,
        "/stat/media/swf/expressInstall.swf",
        flashVars,
        {
            wmode                       : "direct",
            quality                     : "high",
            allowFullScreen             : "true",
            allowFullScreenInteractive  : "true",
            allowScriptAccess           : "always",
            allowNetworking             : "all"
        },
        {
            id: "game"
        },
        embedCallback
    );
}

/**
 * Получение get параметров.
 *
 * @returns {Array}
 */
function getVars()
{
    var vars = [], query, _var;

    // получаем параметры get и записываем в строку
    query = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");

    // разбиваем строку на подстроки
    for (var i = 0; i < query.length; i++) {
        // каждый параметр get записываем в отдельную переменную
        _var = query[i].split("=");
        vars[_var[0]] = _var[1];
    }

    return vars;
}

/**
 * Инициализация общих переменных передаваемых флешке.
 *
 * @param socialNetwork
 * @param host
 * @param unix_time
 * @param session
 * @param statistics_server
 * @returns {Array}
 */
function initVars(socialNetwork, host, unixTime, session, statisticsServer,
                  version, groupId, testMode, currentLevel, splitId)
{
    var vars = getVars();

    vars["social_network"]        = socialNetwork;
    vars["request_url"]           = "//" + host;
    vars["server_url"]            = "http://" + host + "/stat/media/split/" + splitId + "/";
    vars["http_server_unixtime"]  = unixTime;
    vars["session"]               = session;
    vars["statistics_server_url"] = "//" + statisticsServer + "/statistics/index";
    vars["browser"]               = navigator.userAgent;
    vars["files_version"]         = version;
    vars["response_max_time"]     = 15;
    vars["default_attempts_load"] = 10;
    vars["test_mode"]             = testMode;
    vars["group_id"]              = groupId;
    vars["current_level"]         = currentLevel;

    return vars;
}

/**
 * Колбек загрузки флешки.
 *
 * @param e
 */
function embedCallback(e)
{
    if (!e.success) {
        $('#updateFlashBlock img').attr('src', '/img/update-flash-btn.png');
        $('#updateFlashBlock').css('backgroundImage', 'url(/img/update-flash.jpg)').show();
        console.log("ошибка загрузки swf!");
    } else {
        console.log("полет нормальный");
    }
}

/**
 * Функция скрытия/отображения контейнера с игрой.
 *
 * @param visible
 */
function gameVisible(visible)
{
    visible = !visible * 1000;
    $("#gameContent").css("left", visible + "px");
}

/**
 * функция записи текста в лог браузера.
 *
 * @param text
 */
function showText(text)
{
    //console.log(text);
}

/**
 * Отправка статистики по реферальной ссылке.
 *
 * @param serverTime        серверное время
 * @param statisticsServer  адрес сервера статистики
 * @param session           идентификатор текущей сессии
 * @param isRegister        флаг регистрации
 * @param referrerId
 * @param viewerId
 * @param referrer
 * @param requestKey
 *
 * @return void
 */
function sendStatistics(serverTime, statisticsServer, session, isRegister, userId, viewerId,
                        referrer, requestKey, splitId)
{
    var data;
    userId     = userId || 0;
    referrer   = referrer || "unknown";
    requestKey = requestKey || "";
    serverTime *= 1000;

    if (isRegister) {
        data = [{
            event:       "registerUser",
            referrerId:  userId,
            referrer:    referrer,
            request_key: requestKey,
            timestamp:   serverTime,
            splitId:     splitId
        }];
        $.post(
            '//' + statisticsServer + '/statistics/index',
            {
                userId:  viewerId,
                data:    JSON.stringify(data),
                session: session
            },
            function(data){}
        );
    }

    if (referrer == 'unknown') {
        return;
    }

    data = [{
        event:       'referalGame',
        referrerId:  userId,
        referrer:    referrer,
        request_key: requestKey,
        timestamp:   serverTime
    }];

    $.post(
        '//' + statisticsServer + '/statistics/index',
        {
            userId:  viewerId,
            data:    JSON.stringify(data),
            session: session
        },
        function(data){}
    );
}
