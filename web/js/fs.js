/**
 * Служебные функции работы с ФС.
 *
 * file: fs.js
 * category: utils
 * author: Azamat
 */

/**
 * Объект для работы с апи ФС.
 */
var FS = null;

/**
 * Инициализация апи.
 *
 * @param flashVars гет-параметры, полученные от социальной сети.
 * @param callback  функция обратного вызова при успешной инициализации API.
 * @return void
 */
function initApi(flashVars, callback)
{
    $.ajaxSetup({
        cache: true
    });

    $.getScript(flashVars['fsapi'], function () {
        FS = new fsapi(flashVars['apiId'], flashVars['clientKey']);
        FS.init(function() {
            console.error('Api error');
        });

        if (typeof callback == 'function') {
            callback();
        }
    });
}

/**
 * Вызывает событие социальной сети.
 *
 * @param eventName имя события
 * @param params    параметры
 * @return void
 */
function fsEvent(eventName, params)
{
    if (FS == null) {
        console.error('fs сlient not initialized');
        onOrderFail({error: 1});
    }

    FS.event(eventName, onOrderFail, params);
}

/**
 * Слушатель событий.
 *
 * @param data данные ответа
 * @return void
 */
function onOrderFail(data)
{
    document.getElementById('game').fsEventHandler(data);
}
