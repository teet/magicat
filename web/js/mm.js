/**
 * Служебные функции работы с MM.
 *
 * file: mm.js
 * category: utils
 * author: Amir Kumalov
 * created: 15/07/14.
 */

/**
 * Инициализация API MM.
 *
 * @param flashVars GET параметры, полученные от соцсети
 * @param callback  Функция обратного вызова при успешной инициализации API.
 * @see http://api.mail.ru/docs/guides/jsapi/
 */
function initApi(flashVars, callback)
{
    mailru.loader.require("api", function() {
        mailru.app.init(flashVars["clientKey"]);

        if (typeof callback === "function") {
            callback();
        }
    });
}
