/**
 * Редактор игровых квестов.
 *
 * file: qeditor.js
 * category: utils
 * author: Azamat
 */

/** Номер последнего квеста. */
var gLastQuestId = 0;

/**
 * Загружает данные квестов.
 *
 * @param callback Функция обратного вызова по завершению загрузки
 * @return void
 */
function loadQuests(callback)
{
    $.getJSON('/q/g', function(questsInfo) {
        gLastQuestId = parseInt(questsInfo.lastId, 10);

        fillQuestTable('activeQuests', questsInfo.active);
        fillQuestTable('inactiveQuests', questsInfo.inactive);

        if (typeof(callback) == 'function') {
            callback();
        }
    });
}

/**
 * Проверяет объект на пустоту.
 *
 * @param obj Проверяемый объект
 * @return bool Возвращает true, если объект не содержит элементов, иначе false
 */
function isEmpty(obj)
{
    for (var key in obj) {
        return false;
    }
    return true;
}

/**
 * Устанавливает обработчики событий для ссылок.
 *
 * Добавление/удаление/перемещение квеста, добавление/удаление наград.
 *
 * @param item Строка таблицы(квест), содержащая поля с параметрами квеста
 * @return void
 */
function setActionLinkHandlers(item)
{
    $(item).find('#awards A').click(function() {
        var link = $(this);
        var list = $(link).parent().find('#awardList');

        if ($(list).find('#awardItem').length >= 3) {
            $(this).hide();
            return false;
        }

        $(list).append($('#questAwardItemTpl').html());

        $(list).find('#awardItem:last a').click(function() {
            $(this).parent().remove();
            $(link).show();
            return false;
        }).mouseover(function(){$(this).addClass('link-hovered-hover');})
          .mouseout(function(){$(this).removeClass('link-hovered-hover');});

        if ($(list).find('#awardItem').length >= 3) {
            $(this).hide();
        }

        return false;
    });

    $(item).find('#move A').click(function() {
        moveQuest(item);
        return false;
    });

    $(item).find('#remove A').click(function() {
        if (confirm('Вы уверены?')) {
            $(item).remove();
        }

        return false;
    });
}

/**
 * Заполняет таблицу квестов данными.
 *
 * @param id    Идентификатор таблицы
 * @param items Объект, содержащий данные квестов
 * @return void
 */
function fillQuestTable(id, items)
{
    if (isEmpty(items)) {
        return;
    }

    var itemTpl   = $('#questItemTpl table tbody').html();
    var questList = $('#' + id + ' tbody');

    for (var k in items) {
        $(questList).append(itemTpl);
        var item = $(questList).find('tr:last');

        $(item).find('#questId').html(k);
        $(item).find('#levelFrom input').val(items[k][0]);
        $(item).find('#levelTo input').val(items[k][1]);

        setActionLinkHandlers(item);
        fillAwardList(items[k][2], $(item).find('#awards'));
    }
}

/**
 * Заполняет список наград за выполнение квеста.
 *
 * @param awards     Массив параметров награды
 * @param awardsRoot Родительский объект(поле таблицы), содержащий контейнер для списка наград
 * @return void
 */
function fillAwardList(awards, awardsRoot)
{
    if (awards.length <= 0) {
        return;
    }

    var awardTpl  = $('#questAwardItemTpl').html();
    var awardList = $(awardsRoot).find('#awardList');
    var link      = $(awardsRoot).find('a:last');

    for (var i in awards) {
        $(awardList).append(awardTpl);
        var item = $(awardList).find('#awardItem:last');

        $(item).find('#type')[0].selectedIndex = parseInt(awards[i][0], 10);
        $(item).find('#count').val(awards[i][1]);
        $(item).find('a:last').click(function() {
            $(this).parent().remove();
            $(link).show();
            return false;
        });
    }

    if (awards.length >= 3) {
        $(link).hide();
    }
}

/**
 * Добавляет новый квест в список активных.
 *
 * @return void
 */
function addNewQuest()
{
    var questList = $('#activeQuests tbody');

    if (questList.length <= 0) {
        alert('Ошибка! Не удается найти таблицу активных квестов.');
        return false;
    }

    $(questList).append($('#questItemTpl table tbody').html());
    gLastQuestId++;

    var item = $(questList).find('tr:last');
    $(item).find('#questId').html('i' + gLastQuestId);
    setActionLinkHandlers(item);
}

/**
 * Перемещает квест в другой список.
 *
 * @param item Строка таблицы(квест), содержащая поля с параметрами квеста
 * @return void
 */
function moveQuest(item)
{
    if ($(item).parent().parent().attr('id') == 'activeQuests') {
        $(item).appendTo($('#inactiveQuests tbody'));
    } else {
        var levelFrom = parseInt($(item).find('#levelFrom input').val(), 10);
        var levelTo   = parseInt($(item).find('#levelTo input').val(), 10);

        if (checkQuestIntersect(levelFrom, levelTo)) {
            alert('Невозможно переместить квест, т.к. имеются пересечения с существующими.');
            return false;
        }

        $(item).appendTo($('#activeQuests tbody'));
    }
}

/**
 * Проверяет пересечение параметров квеста с набором существующий активных квестов.
 *
 * @param levelFrom Уровень доступности квеста
 * @param levelTo   Уровень, до которого нужно дойти для завершения квеста
 * @return bool Возвращает true, если квест пересекается с другими, иначе false
 */
function checkQuestIntersect(levelFrom, levelTo)
{
    if (levelFrom <= 1) {
        alert('Квест может быть доступен только со 2-го уровня');
        return true;
    }

    if (levelFrom == levelTo) {
        alert('Уровень завершения квеста должен быть хотя бы на 1 больше уровня открытия квеста');
        return true;
    }

    var res = false;

    $('#activeQuests .quest-item').each(function() {
        var f = parseInt($(this).find('#levelFrom input').val(), 10);
        var t = parseInt($(this).find('#levelTo input').val(), 10);

        if ((levelFrom >= f && levelFrom < t) ||
            (levelTo > f && levelTo <= t) ||
            (levelFrom <= f && levelTo >= t)
        ) {
            res = true;
            return;
        }
    });

    return res;
}

/**
 * Проверяет пересечение параметров квеста с существующими активными квестами.
 *
 * Проверка идет по списку квестов, находящиеся внутри объекта.
 *
 * @param levelFrom Уровень доступности квеста
 * @param levelTo   Уровень, до которого нужно дойти для завершения квеста
 * @param quests    Объект, содержащий данные квеста
 *
 * @return string Возвращает id квеста, если пар-ры переданного квеста пересекаются с ним, иначе пустую строку
 */
function getQuestIntersect(levelFrom, levelTo, quests)
{
    for (var k in quests) {
        if ((levelFrom >= quests[k][0] && levelFrom < quests[k][1]) ||
            (levelTo > quests[k][0] && levelTo <= quests[k][1]) ||
            (levelFrom <= quests[k][0] && levelTo >= quests[k][1])
        ) {
            return k;
        }
    }

    return '';
}

/**
 * Возвращает объект с данными квестов.
 *
 * @param list           Список строк(квестов), содержащие параметры квестов
 * @param checkIntersect Флаг проверки квестов на пересечение
 * @return object Возвращает объект с данными квестов или null, в случае неверных параметров одного из квестов.
 */
function getQuestItems(list, checkIntersect)
{
    var items = {};

    $(list).each(function() {
        var levelFrom = parseInt($(this).find('#levelFrom input').val(), 10);
        var levelTo   = parseInt($(this).find('#levelTo input').val(), 10);

        if (isNaN(levelFrom) || isNaN(levelTo) || levelFrom <= 1 || levelTo <= levelFrom) {
            alert('Неверные параметры уровней: ' + levelFrom + ':' + levelTo);
            items = null;
            return false;
        }

        if (checkIntersect) {
            var questId = getQuestIntersect(levelFrom, levelTo, items);

            if (questId != '') {
                alert('Квесты "' + $(this).find('#questId').text() + '" и "' + questId + '" пересекаются');
                items = null;
                return false;
            }
        }

        var awardItems = $(this).find('#awards #awardList #awardItem');
        var awards     = [];

        if (awardItems.length <= 0) {
            alert('Для квеста не указан список наград за его выполенение');
            items = null;
            return false;
        }

        awardItems.each(function() {
            var aType  = parseInt($(this).find('#type').val(), 10);
            var aCount = parseInt($(this).find('#count').val(), 10);

            if (isNaN(aType) || isNaN(aCount) || aCount <= 0) {
                if (isNaN(aType)) {
                    alert('Неверный тип награды');
                } else {
                    alert('Количество награды за предмет не может быть <= 0');
                }

                items = null;
                return false;
            }

            awards.push([aType, aCount]);
        });

        if (items != null) {
            items[$(this).find('#questId').text()] = [levelFrom, levelTo, awards];
        }
    });

    return getSortedItems(items);
}

/**
 * Возвращает отсортированный список по значению уровня открытия квеста.
 *
 * @param items Объект с параметрами квестов
 * @return отсортированный объект с параметрами квестов
 */
function getSortedItems(items)
{
    if (items == null || isEmpty(items)) {
        return null;
    }

    var list = [];
    var i, j, k, tmp;

    for (var k in items) {
        list.push([k, items[k]]);
    }

    for (i = 0; i < list.length - 1; i++) {
        for (j = i + 1; j < list.length; j++) {
            if (list[i][1][0] > list[j][1][0]) {
                tmp     = list[i];
                list[i] = list[j];
                list[j] = tmp;
            }
        }
    }

    items = {};

    for (i = 0; i < list.length; i++) {
        items[list[i][0]] = list[i][1];
    }

    return items;
}

/**
 * Сохраняет весь список активных и неактивных квестов.
 *
 * @param btn Объект кнопки, который инициировал событие
 * @return void
 */
function saveQuests(btn)
{
    if (gLastQuestId <= 0) {
        alert('Сначала надо добавить хотя бы один квест.');
        return;
    }

    $(btn).attr({'disabled': true});
    var data = {
        lastId: gLastQuestId,
        active: {},
        inactive: {}
    };

    data.active = getQuestItems($('#activeQuests .quest-item'), true);

    if (data.active == null) {
        $(btn).attr({'disabled': false});
        return;
    }

    data.inactive = getQuestItems($('#inactiveQuests .quest-item'), false);

    if (data.inactive == null) {
        $(btn).attr({'disabled': false});
        return;
    }

    $.post('/q/s', 'data=' + JSON.stringify(data), function(res) {
        $(btn).attr({'disabled': false});

        if (res.hasOwnProperty('status') && (res.status == 1)) {
            alert('Данные успешно сохранены.');
        } else {
            var msg = 'Не удалось сохранить изменения.';

            if (res.hasOwnProperty('err')) {
                switch (res.err) {
                    case 1:
                        msg = 'Неверные данные';
                        break;
                    case 2:
                        msg = 'Не удалось создать папку для резервной копии';
                        break;
                    case 3:
                        msg = 'Не удалось создать файл резервной копии';
                        break;
                }
            }

            alert(msg);
        }
    });
}

/**
 * Фукнция, вызывающаяся по завершению загрузки страницы.
 *
 * @return void
 */
$(function() {
    loadQuests(function() {
        $('.link-hovered').mouseover(function(){$(this).addClass('link-hovered-hover');}).mouseout(function(){$(this).removeClass('link-hovered-hover');});
        $('.link-inhovered').mouseover(function(){$(this).addClass('link-inhovered-hover');}).mouseout(function(){$(this).removeClass('link-inhovered-hover');});
    });
});
