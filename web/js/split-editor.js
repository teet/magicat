/**
 * Редактор сплитов.
 *
 * file: splitEditor.js
 * category: utils
 * author: Azamat
 */

var gGroupTpl  = null;
var gSplitTpl  = null;
var gGroupList = null;

/**
 * Проверяет объект на пустоту.
 *
 * @param obj Проверяемый объект
 * @return bool Возвращает true, если объект не содержит элементов, иначе false
 */
function isEmpty(obj)
{
    for (var key in obj) {
        return false;
    }
    return true;
}

/**
 * Отправка данных на сервер.
 *
 * @param {string} url  url запроса
 * @param {object} data данные
 * @returns void
 */
function sendRequest(url, data, callback, dataType)
{
    if (typeof dataType !== 'string') {
        dataType = 'json';
    }

    $("#ajax-loader").dialog("open");
    $.post(url, data, function(response) {
        $("#ajax-loader").dialog("close");

        if (typeof response === 'object'
            && response.hasOwnProperty("response")
            && response["response"].hasOwnProperty("errorCode")
        ) {
            alert("Произошла ошибка на сервере. Код ошибки: " + response["response"]["errorCode"]);
            return;
        }

        if (typeof callback === 'function') {
            callback(response);
        }
    }, dataType);
}

/**
 * Возвращает форматированную строку даты и времени.
 *
 * @param format    формат строки
 * @param timestamp unix-time, кол-во секунд с 1970 года.
 *
 * @return string
 */
function getFormattedDate(format, timestamp)
{
    var d = new Date();
    d.setTime(timestamp * 1000);
    return d.toLocaleFormat(format);
}

/**
 * Загружает данные сплитов.
 *
 * @param callback Функция обратного вызова по завершению загрузки
 * @return void
 */
function loadSplits(callback)
{
    sendRequest('/se/g', {} , function(response) {
        fillGroupList(response.items);

        if (typeof callback == 'function') {
            callback();
        }
    });
}

/**
 * Заполняет таблицу квестов данными.
 *
 * @param items Объект, содержащий данные квестов
 * @return void
 */
function fillGroupList(items)
{
    if (isEmpty(items)) {
        return;
    }

    for (var k in items) {
        var t = parseInt(items[k]['removeTime'], 10);
        if (t > 0) {
            continue;
        }

        $(gGroupList).append(gGroupTpl);
        var item      = $(gGroupList).find('.split-group:last');
        var startTime = '-';
        var stopTime  = '-';

        var startTimeStamp = parseInt(items[k]['startTime'], 10);
        if (startTimeStamp > 0) {
            startTime = getFormattedDate("%d.%m.%Y<br/>%H:%M", startTimeStamp);
        }

        var stopTimeStamp = parseInt(items[k]['stopTime'], 10);
        if (stopTimeStamp > 0) {
            stopTime = getFormattedDate("%d.%m.%Y<br/>%H:%M", stopTimeStamp);
        }

        $(item).find('#groupId').html(k);
        $(item).find('#groupName input').val(items[k]['name']);
        $(item).find('#startTime').html(startTime);
        $(item).find('#stopTime').html(stopTime);

        addSplitActionButton($(item).find('#splits table thead #splitActions'), 'add');
        var actionButtons = $(item).find('#groupActions')[0];

        if (startTimeStamp <= 0) {
            addGroupActionButton(actionButtons, 'start');
            addGroupActionButton(actionButtons, 'remove');
        } else if (stopTimeStamp <= 0) {
            addGroupActionButton(actionButtons, 'stop');
        } else {
            addGroupActionButton(actionButtons, 'remove');
        }

        fillSplitList(items[k]['splits'], $(item).find('#splits table tbody'));
    }

    makeLinksHovered(gGroupList);
}

function fillSplitList(items, container)
{
    if (isEmpty(items)) {
        return;
    }

    var mainSplit  = true;
    var splitCount = 0;

    for (var k in items) {
        splitCount++;
    }

    for (var k in items) {
        $(container).append(gSplitTpl);
        var item = $(container).find('.split-item:last');

        $(item).find('#splitId').html(k);
        $(item).find('#splitName input').val(items[k].name);
        $(item).find('#percent input').html(items[k].percent);
        $(item).find('#copacity input').html(items[k].copacity);

        if (mainSplit) {
            if (splitCount == 1) {
                addSplitActionButton($(item).find('#splitActions')[0], 'remove');
            }
            mainSplit = false;
        } else {
            addSplitActionButton($(item).find('#settings')[0], 'edit');
            addSplitActionButton($(item).find('#splitActions')[0], 'remove');
        }
    }
}

function addGroup()
{
    sendRequest('/se/ag', {}, function(response) {
        $(gGroupList).append(gGroupTpl);
        var item = $(gGroupList).find('.split-group:last');
        item.find('#groupId').html('g' + response.id);
        item.find('#groupName INPUT').val(response.name);

        addSplitActionButton($(item).find('#splits table thead #splitActions'), 'add');

        var actionButtons = $(item).find('#groupActions');
        addGroupActionButton(actionButtons, 'start');
        addGroupActionButton(actionButtons, 'remove');

        makeLinksHovered(actionButtons);
    });
}

function addGroupActionButton(actionButtons, type)
{
    switch (type) {
        case 'start':
            $('<a href="#" id="startBtn" class="link-inhovered" title="Запустить группу сплитов">Старт</a><br/><br/>')
            .appendTo(actionButtons)
            .click(function() {
                var group   = $(this).parent().parent();
                var groupId = $(group).find('#groupId').html();

                startGroup(groupId, group);
                return false;
            });

            break;
        case 'stop':
            $('<a href="#" id="stopBtn" class="link-inhovered" title="Остановить группу сплитов">Стоп</a><br/>')
            .appendTo(actionButtons)
            .click(function() {
                var group   = $(this).parent().parent();
                var groupId = $(group).find('#groupId').html();

                stopGroup(groupId, group);
                return false;
            });

            break;
        case 'remove':
            $('<a href="#" id="removeBtn" class="link-inhovered" title="Удалить группу сплитов">Удалить</a><br/>')
            .appendTo(actionButtons)
            .click(function() {
                var group   = $(this).parent().parent();
                var groupId = $(group).find('#groupId').html();

                removeGroup(groupId, group);
                return false;
            });

            break;
    }
}

function addSplitActionButton(container, type)
{
    switch (type) {
        case 'add':
            $('<a href="#" id="addBtn" class="link-inhovered" title="Добавить сплит">Добавить</a>')
            .appendTo(container)
            .click(function() {
                var group   = $(this).parent().parent().parent().parent().parent().parent();
                var groupId = group.find('#groupId').html();

                addSplit(groupId, group.find('#splits table tbody'));
                return false;
            });

            break;
        case 'edit':
            $('<a href="#" id="editBtn" class="link-inhovered" title="Редактировать параметры сплита">Изменить</a>')
            .appendTo(container)
            .click(function() {
                var split   = $(this).parent().parent();
                var splitId = $(split).find('#splitId').html();

                editSplit(splitId, split);
                return false;
            });

            break;
        case 'remove':
            $('<a href="#" id="removeBtn" class="link-inhovered" title="Удалить сплит">Удалить</a>')
            .appendTo(container)
            .click(function() {
                var split   = $(this).parent().parent();
                var splitId = $(split).find('#splitId').html();

                removeSplit(splitId, split);
                return false;
            });

            break;
    }
}

function startGroup(groupId, group)
{
    if (!confirm('Запуск группы сплитов, продолжить?')) {
        return false;
    }

    sendRequest('/se/stg', {'groupId': groupId}, function(response) {
        //
    });
}

function stopGroup(groupId, group)
{
    if (!confirm('Остановка группы сплитов, продолжить?')) {
        return false;
    }

    sendRequest('/se/spg', {'groupId': groupId}, function(response) {
        //
    });
}

function removeGroup(groupId, group)
{
    if (!confirm('Группа сплитов будет удалена безвозвратно, продожить?')) {
        return false;
    }

    sendRequest('/se/rg', {'groupId': groupId}, function(response) {
        $(group).remove();
    });
}

/**
 *
 */
function addSplit(groupId, splitList)
{
    sendRequest('/se/as', {'groupId': groupId}, function(response) {
        var splitCount = $(splitList).find('.split-item').length;

        for (var k = 0; k < response.length; k++) {
            $(splitList).append(gSplitTpl);
            var item = $(splitList).find('.split-item:last');

            $(item).find('#splitId').html(response[k].id);
            $(item).find('#splitName input').val(response[k].name);

            //if (splitCount > 0) {
                addSplitActionButton($(item).find('#settings')[0], 'edit');
                addSplitActionButton($(item).find('#splitActions')[0], 'remove');
            //}

            makeLinksHovered($(item).find('#settings'));
            makeLinksHovered($(item).find('#splitActions'));
        }
    });
}

function makeLinksHovered(parent)
{
    var hoveredItems   = null;
    var inhoveredItems = null;

    if (typeof parent === 'string') {
        hoveredItems   = $(parent + ' .link-hovered');
        inhoveredItems = $(parent + ' .link-inhovered');
    } else if (typeof parent === 'object' && parent !== null) {
        hoveredItems   = $(parent).find('.link-hovered');
        inhoveredItems = $(parent).find('.link-inhovered');
    }

    if (hoveredItems != null) {
        hoveredItems.mouseover(function(){$(this).addClass('link-hovered-hover');}).mouseout(function(){$(this).removeClass('link-hovered-hover');});
        inhoveredItems.mouseover(function(){$(this).addClass('link-inhovered-hover');}).mouseout(function(){$(this).removeClass('link-inhovered-hover');});
    }
}

/**
 * Фукнция, вызывающаяся по завершению загрузки страницы.
 *
 * @return void
 */
$(function() {
    gGroupTpl  = $('#groupItemTpl table tbody').html();
    gSplitTpl  = $('#splitItemTpl table tbody').html();
    gGroupList = $('#splitsTable tbody');

    $("#ajaxLoader").dialog({
        title: 'Загрузка...',
        draggable: false,
        autoOpen: false,
        closeOnEscape: false,
        modal: true,
        width: "auto",
        open: function(event, ui) {
            $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
        }
    });

    loadSplits(function() {
        $('#addGroupBtn').click(function() {
            addGroup();
        });
    });
});
