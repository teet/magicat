/**
 * Служебные функции работы с OD.
 *
 * file: od.js
 * category: utils
 * author: Amir
 * author: Azamat
 * created: 07/05/14.
 */

/**
 * Инициализация API OD.
 *
 * @param flashVars GET параметры, полученные от соцсети
 * @param callback  Функция обратного вызова при успешной инициализации API.
 * @see http://apiok.ru/wiki/display/api/FAPI.init+ru
 */
function initApi(flashVars, callback)
{
    FAPI.init(
        flashVars["api_server"],
        flashVars["apiconnection"],
        function() {
            console.log("Инициализация API прошла успешно");
            if (typeof callback === "function") {
                callback();
                FAPI.UI.setWindowSize(1, 620);
            }
        },
        function(error) {
            console.log("Ошибка инициализации API");
        }
    );
}

/**
 * Загружает скрипт для работы с виджетами в ОД.
 *
 * @param url      Адрес скрипта
 * @param callback Функция обратного вызова
 */
function loadWidgetsScript(url, callback)
{
    var js = document.createElement('script');
    js.src = url;
    js.onload = js.onreadystatechange = function() {
        if (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete') {
            if (!this.executed) {
                this.executed = true;
                if (typeof callback == 'function') {
                    setTimeout(callback, 0);
                }
            }
        }
    };

    document.documentElement.appendChild(js);
}

/**
 * Инициализация виджетов ОД.
 *
 * Загружает скрипт для работы с виджетами ОД и инициализирует виджеты.
 * @see http://apiok.ru/wiki/pages/viewpage.action?pageId=42476656
 *
 * @param appId     Айди приложения
 * @param subscribe Флаг: 0-не ставить слушателей на события изменения состояния виджетов("Класс"), 1-ставить.
 * @param groupId   Айди группы приложения
 */
function initWidgets(appId, subscribe, groupId)
{
    loadWidgetsScript('http://connect.ok.ru/connect.js', function() {
        OK.CONNECT.insertShareWidget(
            'okShareWidget',
            'http://' + window.location.hostname + '/sw/' + appId,
            '{width:165,height:35,st:"oval",sz:30,ck:1}'
        );

        if (subscribe == 1) {
            if (window.addEventListener) {
                window.addEventListener('message', onShare, false);
            } else {
                window.attachEvent('onmessage', onShare);
            }
        }
    });
}

/**
 * Слушатель на событие изменения статуса кнопки "Класс".
 *
 * @param event Объект с данными о событии
 * @see http://apiok.ru/wiki/pages/viewpage.action?pageId=42476656
 */
function onShare(event)
{
    var args = event.data.split('$');
    if (args[0] == 'ok_shared') {
        document.getElementById('game').externalLikeClickCallback(1);
    }
}

/**
 * Показывает окно приглашения друзей.
 * @param text
 * @param params
 * @see http://apiok.ru/wiki/display/api/ActionScript+API+-+showInvite
 */
function showInvite(text, params)
{
    FAPI.UI.showInvite(text, params);
}

/**
 * Показывает окно уведомления друзей.
 * @param text
 * @param params
 * @param userIds
 * @see http://apiok.ru/wiki/display/api/ActionScript+API+-+showNotification
 */
function showNotification(text, params, userIds)
{
    FAPI.UI.showNotification(text, params, userIds);
}

/**
 * Обработчик событий от соц. сети.
 *
 * @param method Имя AS-метода, который вызвал обратную связь. Например, showPayment
 * @param result В случае диалога содержит результат: «ОК», «Отмена», ...
 * @param data   Некоторые данные, передаваемые приложению. Например: значение «resignature», полученное от showConfirmation
 * @return void
 */
function API_callback(method, result, data)
{
    //пока не используется, но метод должен присутствовать
}
