/**
 * Nodejs-клиент.
 *
 * file: nodeClient.js
 * category: utils
 * author: Azamat
 */

/**
 * Создает сокетное соединения с сервером nodejs.
 *
 * @param serverHost адрес сервера, на котором запещен nodejs
 * @param serverPort порт веб-сервера
 * @param userId     идентификатор пользователя в соц. сети
 * @param authKey    ключ авторизации в социальной сети
 * @param snParams   объект с дополнительными параметрами для соц. сети
 */
function initNodeClient(serverHost, serverPort, userId, authKey, snParams)
{
    if (typeof io !== 'object' || io == null) {
        loadSocketIo(serverHost + ':' + serverPort + '/socket.io/socket.io.js', function(res) {
            initNodeClient(serverHost, serverPort, userId, authKey, snParams);
        });

        return;
    }

    var params = {port: serverPort};

    if (serverHost.indexOf('https://') == 0) {
        params.secure = true;
    }

    var socket = io.connect(serverHost, params);

    socket.on('connect', function () {
        socket.emit('doAuth', userId, authKey, snParams);
    });

    socket.on('onAuth', function (isAuthorized) {
    });
}

function loadSocketIo(url, callback)
{
	$.ajax({
		type     : 'GET',
		url      : url,
		dataType : 'script',
		cache    : false,
		timeout  : 5000,
		complete : function(data, res) {
            if (callback) {
                callback(res);
            }
		}
	});
}
