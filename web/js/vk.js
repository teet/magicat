/**
 * служебные функции работы с ВК
 *
 * file: vk.js
 * category: utils
 * author: Artur
 * author: Azamat
 * created: 05/03/14.
 */

/**
 *
 */
function initObserver() {
    if (!VK.Observer) {
        VK.Observer = {
            _subscribers: function() {
                if (!this._subscribersMap) {
                    this._subscribersMap = {};
                }
                return this._subscribersMap;
            },
            publish: function(eventName) {
                var
                    args = Array.prototype.slice.call(arguments),
                    eventName = args.shift(),
                    subscribers = this._subscribers()[eventName],
                    i, j;

                if (!subscribers) return;

                for (i = 0, j = subscribers.length; i < j; i++) {
                    if (subscribers[i] != null) {
                        subscribers[i].apply(this, args);
                    }
                }
            },
            subscribe: function(eventName, handler) {
                var
                    subscribers = this._subscribers();

                if (typeof handler != 'function') return false;

                if (!subscribers[eventName]) {
                    subscribers[eventName] = [handler];
                } else {
                    subscribers[eventName].push(handler);
                }
            },
            unsubscribe: function(eventName, handler) {
                var
                    subscribers = this._subscribers()[eventName],
                    i, j;

                if (!subscribers) return false;
                if (typeof handler == 'function') {
                    for (i = 0, j = subscribers.length; i < j; i++) {
                        if (subscribers[i] == handler) {
                            subscribers[i] = null;
                        }
                    }
                } else {
                    delete this._subscribers()[eventName];
                }
            }
        };
    }
}

/**
 * Инициализация апи ВК.
 *
 * @param callback функция обратного вызова при успешной инициализации API.
 * @see http://vk.com/dev/openapi_init
 */
function initApi(callback)
{
    initObserver();
    VK.init( function() {
        console.log( 'АПИ инициализированно успешно' );

        VK.addCallback( 'onOrderSuccess', onOrderSuccess );
        VK.addCallback( 'onOrderCancel', onOrderCancel );
        VK.addCallback( 'onOrderFail', onOrderFail );
        VK.addCallback( 'onWindowBlur', onWindowBlur );
        VK.addCallback( 'onWindowFocus', onWindowFocus );

        if (callback) {
            callback();
        }
    }, function() {
        console.log( 'Ошибка инициализации АПИ' );
    }, '5.12' );
}

/**
 * Инициализация виджетов ВК
 *
 * @param appId Айди приложения
 * @see http://vk.com/dev/sites
 */
function initWidgets( appId, subscribe, groupId )
{
    VK.Widgets.Like(
        'vk_like',
        {
            type: 'button',
            pageUrl: 'http://vk.com/app' + appId,
            pageTitle: 'Помоги Анне',
            pageDescription: 'Я играю с котёнком! Присоединяйся!',
            pageImage: 'http://cs616528.vk.me/v616528463/10f72/ZQ5kNb4sPsk.jpg'
        }
    );

    $('#vk_share').html(
        VK.Share.button({
            url: 'http://vk.com/app' + appId,
            description: 'Помоги Анне спасти своих котят от злого мага! Взрывай кубики, проходи увлекательные уровни, открывай новые острова и побей рекорды своих друзей! Все это в игре "Три котенка!"',
            image: 'http://cs616528.vk.me/v616528463/10f72/ZQ5kNb4sPsk.jpg',
            noparse: true
        }, {
            type: 'round',
            text: 'Поделиться'
        })
    );

    VK.Widgets.Subscribe( 'vk_subscribe', { soft: 1 }, -groupId );

    if( subscribe == 1 ) {
        VK.Observer.subscribe( 'widgets.like.liked', function() {
            document.getElementById( 'game' ).externalLikeClickCallback(1);
        });

        VK.Observer.subscribe( 'widgets.like.unliked', function() {
            document.getElementById( 'game' ).externalLikeClickCallback(0);
        });
    }
}

/**
 * Событие происходит, когда покупка закончилась успешно.
 * @param orderId
 * @see http://vk.com/dev/Javascript_SDK
 */
function onOrderSuccess( orderId )
{
    document.getElementById( 'game' ).externalOnOrderSuccess({
        transaction : orderId
    });
}

/**
 * Событие происходит, когда пользователь отменяет покупку.
 * @see http://vk.com/dev/Javascript_SDK
 */
function onOrderCancel()
{
    document.getElementById( 'game' ).externalOnOrderSuccess({
        error : 'onOrderCancel'
    });
}

/**
 * Событие происходит, когда покупка закончилась неуспешно.
 * @param errorCode
 * @see http://vk.com/dev/Javascript_SDK
 */
function onOrderFail( errorCode )
{
    document.getElementById( 'game' ).externalOnOrderSuccess({
        error : errorCode
    });
}

/**
 * Событие происходит, когда окно с приложением теряет фокус.
 * @see http://vk.com/dev/Javascript_SDK
 */
function onWindowBlur()
{
    gameVisible(0);
}

/**
 * Событие происходит, когда окно с приложением получает фокус.
 * @see http://vk.com/dev/Javascript_SDK
 */
function onWindowFocus()
{
    gameVisible(1);
}

/**
 * Открывает окно для покупки товара в приложении или ввода голоса на счёт приложения.
 * @param itemType
 * @see http://vk.com/dev/Javascript_API
 */
function showOrderBox( itemType ) {
    var params = {
        type    : 'item',
        item    : itemType
    };

    VK.callMethod( 'showOrderBox', params );
}

/**
 * Отправляет статистику перехода по партнерской ссылке.
 *
 * @param integer serverTime        серверное время
 * @param integer userId            id пользователя
 * @param string  statisticsServer  адрес сервера статистики
 * @param integer session           текущая игровая сессия
 * @param string  vkSid             параметр vk_sid
 * @param string  vkLeadId          параметр vk_lead_id
 * @param string  vkHash            параметр vk_hasg
 *
 * @return void
 */
function sendStatisticsOffer(serverTime, userId, statisticsServer, session, vkSid, vkLeadId, vkHash)
{
    var data = [{
        event:     'partnerOffer',
        timestamp: serverTime * 1000,
        status:    0,
        vkSid:     vkSid,
        vkLeadId:  vkLeadId,
        vkHash:    vkHash
    }];
    $.post(
        '//' + statisticsServer + '/statistics/index',
        {
            userId: userId,
            data: JSON.stringify(data),
            session: session
        },
        function(data){}
    );
}
