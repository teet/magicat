<?php
define('TIME', time());

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpFoundation\Request;
use App\GameBundle\Core\Response\GameResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use App\GameBundle\Core\Config\Config;
use App\GameBundle\Core\SocialNetworks\Authorize\AuthorizeSocial;
use App\GameBundle\Core\Exception\GameException;
use App\GameBundle\Core\Authorize\AuthorizeSignature;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new App\GameBundle\AppGameBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        try {
            $developerList = Config::get('developerList');
            if (Config::get('gameState') == 1
                && $request->getPathInfo() != '/i'
                && !isset($developerList[$request->request->get('userId')])
            ) {
                throw new GameException(
                    'Request can not be treat in technical works state',
                    GameException::GAME_NOT_AVAIBLE
                );
            }

            if ($request->getMethod() == "POST") {
                // Post-данные записываем в список аттрибутов,
                // чтобы они были доступны контроллерам в качестве входных параметров.
                $request->attributes = $request->request;

                if (Config::get("testMode") === 0) {
                    // При каждом запросе проверяем авториацию пользователя.
                    AuthorizeSocial::create()->process($request);

                    // Проверка подписи для входных данных.
                    $authorizeSignature = new AuthorizeSignature();
                    $authorizeSignature->process();
                }
            }

            // Вызываем хендл так, чтобы symfony не перехватывал исключения ($catch = false).
            $response = parent::handle($request, $type, false);
        } catch (GameException $e) {
            if (Config::get('gameState') != 1) {
                $this->_writeErrorToLog($request, $e);
            }
            $response = new GameResponse(array('errorCode' => $e->getCode()));
        } catch (Exception $e) {
            $this->_writeErrorToLog($request, $e);
            $response = new GameResponse(array('errorCode' => GameException::INTERNAL_ERROR));
        }

        return $response;
    }

    private function _writeErrorToLog(Request &$request, Exception &$e)
    {
        $this->boot();

        $logger      = $this->getContainer()->get('logger');
        $requestData = GameException::getRequestLog($request->attributes->all());

        $logger->error("ip[" . $request->getClientIp() . "]: " . $requestData);
        $logger->error($e->getCode() . ' | ' . $e->getMessage() . ' | ' . $request->getRequestUri());
    }
}
