<?php
define('TIME', time());

use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocialFS;

/**
 * @file
 * Платежный скрипт Фотостраны.
 */

require_once './../vendor/autoload.php';

$dispatcher = new ProxySocialFS();
$dispatcher->process();
