<?php

/**
 * @file
 * Служит для экспорта данных о старте, завершении офферки пользователями на сервер статистики.
 *
 * @author Azamat Bogotov <azamat@e-magic.org>
 */

use App\GameBundle\Core\Config\Config;

require_once './../../vendor/autoload.php';

if (Config::get('socialNetwork') != 'VK') {
    exit();
}

if (!isset($_POST['t']) || !isset($_POST['st']) || !isset($_POST['et'])) {
    // отсутствуют параметры
    exit('{"error":1}');
}

$type      = intval($_POST['t']);  // тип: 1-начавшие офферку, 2-завершившие офферку
$startTime = intval($_POST['st']); // дата начала
$endTime   = intval($_POST['et']); // дата завершения

if ($startTime <= 0 || $endTime <= 0) {
    // неверные параметры
    exit('{"error":2}');
}

$logDir = './../../../logs/';

switch ($type) {
    case 1:
        $fileName = $logDir . 'Partner_Success_Start.csv';
        break;
    case 2:
        $fileName = $logDir . 'Partner_Success_Complete.csv';
        break;
    default:
        exit('{"error":3}');
}

if (!file_exists($fileName)) {
    // нет данных
    exit('[]');
}

$fp = fopen($fileName, 'r');

if ($fp === false) {
    exit('{"error":4}');
}

$result = array();

while (($data = fgetcsv($fp, 256, ';')) !== false) {
    $t = strtotime($data[0]);

    if ($t >= $startTime && $t <= $endTime) {
        $result[] = $data;
    } elseif ($t > $endTime) {
        break;
    }
}

fclose($fp);

echo json_encode($result);
