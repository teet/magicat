<?php
define('TIME', time());

use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocialMM;

/**
 * @file
 * Платежный скрипт Мой Мир.
 */

require_once './../vendor/autoload.php';

$dispatcher = new ProxySocialMM();
$dispatcher->process();
