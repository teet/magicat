<?php
define('TIME', time());

use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocialOD;

/**
 * @file
 * Платежный скрипт Odnoklassniki.
 */

require_once './../vendor/autoload.php';

$dispatcher = new ProxySocialOD();
$dispatcher->process();
