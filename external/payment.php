<?php
define('TIME', time());

use App\GameBundle\Core\SocialNetworks\Proxy\ProxySocialVK;

/**
 * @file
 * Платежный скрипт Вконтакте.
 */

require_once './../vendor/autoload.php';

$dispatcher = new ProxySocialVK();
$dispatcher->process();
